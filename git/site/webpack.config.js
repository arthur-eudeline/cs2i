const path = require('path');

const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

// Défini dans package.json scrips.dev
const env = (process.env.NODE_ENV === 'dev');

let config = {
  entry: './js/dev/app.js',
  output: {
    path: path.resolve(__dirname, 'js/'),
    filename: 'bundle.js'
  },
  watch: true,
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: ['babel-loader']
      }
    ]
  },
  plugins: [],

  devtool: !env ? 'cheap-module-eval-source-map' : false // active les sources
};

if (!env) {
  config.optimization = {
    minimizer: [new UglifyJsPlugin()]
  };
}

module.exports = config;
