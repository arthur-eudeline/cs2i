const Path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const Glob = require('glob');

// var test = "salut";

// export de la configuration WebPack
var WebPackConfig = {
  mode: "development",

  // Point d'entrée de webpack
  entry: './src/index.js',

  // Emplacement du fichier généré
  output: {
    path: Path.resolve(__dirname),
    filename: './dist/bundle.js'
  },

  // Watch automatiquement les changements
  watch: true,

  module: {
    rules: [

      // Babel : conversion de ES6 en JS
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      },

      // EJS
      {
        test: /\.ejs$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'ejs-loader',
      },

      // SCSS
      {
        test: /\.scss$/,
        exclude: /(node_modules|bower_components)/,
        // l'ordre est important ! on va de bas en haut
        use : [
          // injecte le css dans une balise <style> dans le <head>
          // { loader : "style-loader" },

          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              //   // you can specify a publicPath here
              //   // by default it use publicPath in webpackOptions.output
              // publicPath: './dist/'
            }
          },

          // permet de faire un 'import css from "style.css"'
          {
            loader : "css-loader",
            options : {
              importLoaders: 1
            }
          },

          // Auto prefixer css
          {
            loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: [
                require('autoprefixer')({
                  browsers : ['last 2 versions', 'ie > 8']
                }),
              ]
            }
          },

          // convertit du SCSS en CSS
          { loader : "sass-loader" },
        ]
      },

      // CSS
      {
        test: /\.css$/,
        exclude: /(node_modules|bower_components)/,
        // l'ordre est important ! on va de bas en haut
        use : [
          // injecte le css dans une balise <style> dans le <head>
          // { loader : "style-loader" },

          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              //   // you can specify a publicPath here
              //   // by default it use publicPath in webpackOptions.output
              // publicPath: './dist/'
            }
          },

          // permet de faire un 'import css from "style.css"'
          {
            loader : "css-loader",
            options : {
              importLoaders: 1
            }
          }
        ]
      }

    ]
  },

  plugins: [
    new MiniCssExtractPlugin({
      filename: "./dist/main.css"
    }),
    new HtmlWebpackPlugin({
      filename : './dist/index.html',
      template : './src/index.html.ejs',
      // minify : true
    })
  ],

  devServer: {
    contentBase: Path.join(__dirname, 'dist'),
    compress: true,
    port: 9000,
    // watch: './src/index.js'
    // entry: './src/index.js',
    // hot: true,
  }
  // open: 'google chrome',

};

// Pour chaque dossier saisi, on récupère la liste des fichiers en .ejs
// let files = Glob.sync('./src/**/*.ejs');
// files.forEach( function( file ) {
//   let fileName = file.split("/").pop().replace('.ejs', '');
//
//
//   // Pour chaque fichier on rajoute un compileur HTML
//   WebPackConfig.plugins.push(new HtmlWebpackPlugin({
//
//     // Le fichier sera enregistré dans ./templates/__directory_name __/__nom_du_fichier__.html
//     filename: './' + fileName,
//
//     // file contient déjà l'emplacement absolu du fichier à convertir
//     template: file,
//
//     // Empêche d'ajouter les balises script WordPress.js
//     inject : false,
//
//     // Minification
//     minify : true
//
//   }));
//
// });

module.exports = WebPackConfig;
