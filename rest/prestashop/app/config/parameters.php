<?php return array (
  'parameters' => 
  array (
    'database_host' => '127.0.0.1',
    'database_port' => '8889',
    'database_name' => 'cs2i_rest_prestashop',
    'database_user' => 'root',
    'database_password' => 'root',
    'database_prefix' => 'ps_',
    'database_engine' => 'InnoDB',
    'mailer_transport' => 'smtp',
    'mailer_host' => '127.0.0.1',
    'mailer_user' => NULL,
    'mailer_password' => NULL,
    'secret' => 'k2ggQ0osYDsxWosbc156RehH64W7Mx3gw3bKqbIT3mNF4JHtzDufcayZ',
    'ps_caching' => 'CacheMemcache',
    'ps_cache_enable' => false,
    'ps_creation_date' => '2018-11-21',
    'locale' => 'fr-FR',
    'cookie_key' => 'DNh1jqsREXKwwwZMZhmNVigm8fMMtFyGwoAyFmrqaNkAFN1HpA5Zdm1C',
    'cookie_iv' => '761TVdjm',
    'new_cookie_key' => 'def00000aaaa47e0aa247cdb1c085333206b5d452e70442ea8d9604f93010001b3b539811a9424fd4ba4a81b80413fb4d9c63ed81611127d90c9cebd1c14bb18972b4fe8',
  ),
);