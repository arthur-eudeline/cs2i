<?php return array (
  'parameters' => 
  array (
    'database_host' => '127.0.0.1',
    'database_port' => '8889',
    'database_name' => 'cs2i_rest_eval_prestashop',
    'database_user' => 'root',
    'database_password' => 'root',
    'database_prefix' => 'ps_',
    'database_engine' => 'InnoDB',
    'mailer_transport' => 'smtp',
    'mailer_host' => '127.0.0.1',
    'mailer_user' => NULL,
    'mailer_password' => NULL,
    'secret' => 'pF1g7iQr4HeAGJTBn4TDy9jKZ0LBz3rwVti5PsKMUVDRNcpfyTDcg1Cg',
    'ps_caching' => 'CacheMemcache',
    'ps_cache_enable' => false,
    'ps_creation_date' => '2018-12-11',
    'locale' => 'fr-FR',
    'cookie_key' => 'XaTADNCWa5fp684uiBMUZSweMJhRqSqV4x3xs18uAvnqdRwyMD3cZeqM',
    'cookie_iv' => 'VMATpeZJ',
    'new_cookie_key' => 'def0000092867e46373964ef6a8666fbab7b449c4cbf2e5902174239067f172db646b49f224080093d66d2b81528faed34cb7586c84b1b1086dc7d9f545dbca185e31d4f',
  ),
);
