<?php
/**
 * @author    Arthur Eudeline - CS2i B3ca 2018
 */
?>

<!-- Template -->
<div class="template-container" id="list-eleve">

  <!-- Template ligne liste -->
  <div id="row-template" class="hidden">

    <div class="template-eleve-list-row">
      <!-- Photo -->
      <div class="template-image image-location"></div>

      <!-- Contenu -->
      <div class="template-content-wrapper">
        <p>
          <b>Nom :</b> <span class="nom-location"></span>
        </p>
        <p>
          <b>Prénom :</b> <span class="prenom-location"></span>
        </p>
        <p>
          <b>Âge :</b> <span class="age-location"></span>
        </p>

        <p>
          <input type="hidden" class="json-location" />
          <button class="remove-button">Supprimer l'élève</button>
        </p>
      </div>
    </div>

  </div>
  <!-- /template ligne liste -->

  <h2>Liste des élèves :</h2>


</div>
<!-- /Template -->
