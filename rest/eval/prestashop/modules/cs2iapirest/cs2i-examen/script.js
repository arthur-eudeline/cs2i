jQuery(document).ready( function($){

  // Génération du template
  if ( typeof listEleves !== "undefined" ) {
    var rowTemplate = $("#row-template").html();
    $("#row-template").remove();

    listEleves.forEach( function( eleve, index ){
      var row = $(rowTemplate).clone();

      $(row).find(".nom-location").text( eleve.field_nom );
      $(row).find(".prenom-location").text( eleve.field_prenom );
      $(row).find(".age-location").text( eleve.field_age );
      $(row).find(".image-location").attr( "style", "background-image: url(" + eleve.field_photo + ")" );
      $(row).find(".json-location").val( JSON.stringify(eleve) );

      $("#list-eleve").append( row );
    });

  } else {
    console.error( "listEleves n'est pas défini !" );
  }

  // Action de suppression
  $(document).on('touch click', '.remove-button', function(){
    if ( window.confirm("Êtes-vous sûr ?") ){
      var eleveData = JSON.parse( $(this).parent().find('.json-location').val() );

      // Récupération du token
      var token = "";
      $.get( "http://localhost:8888/cs2i/rest/eval/drupal/rest/session/token" ).then(
        function (response){
          token = response;

          $.ajax({
            "url": "http://localhost:8888/cs2i/rest/eval/drupal/node/"+ eleveData.nid,
            "method": "DELETE",
            "headers": {
              // "authorization": "Basic YXJ0aHVyOmFydGh1cjE4",
              "Authorization": "Basic " + btoa("arthur:arthur18"),
              "Accept" : "*/*",
              'X-CSRF-Token' : token

            },
            "data"  : {
              "_format" : "json",
              // "_token"  : xCSRtoken
            }
          }).done( function( response ){
            console.log( response );
          });
        },

        function (error){
          console.error(error);
        }
      );
    }
  })

} );
