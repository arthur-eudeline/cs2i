<?php

if ( !function_exists('get_list_eleves') ){

  /**
  * Retourne la liste des élèves, et au besoin, l'injecte dans l'objet window en JS
  *
  * @method get_list_eleve
  *
  * @param  boolean        $return       Si on doit retourner ou non le JSON
  * @param  boolean        $write_script Si on doit ou non ecrire une balise script et y injecter la variable listEleves
  *
  * @return array         Un tableau contenant la liste des élèves au format tableau dans l'index "data", et au format JSON dans l'index "json"
  */
  function get_list_eleves( $return = true, $write_script = false){
    $curl = curl_init("http://localhost:8888/cs2i/rest/eval/drupal/api/eleves");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Afin que curl_exec() return un résultat plutôt de que de l'écrire
    $curl_result = curl_exec($curl);
    curl_close($curl);

    $output = json_decode( $curl_result );

    // Formatage de l'URL des images qui est en relative, vers un format absolu
    if ( is_array($output) && !empty($output) ){
      foreach($output as $key => $eleve){

        if ( isset($eleve->field_photo) && is_string($eleve->field_photo) ){
          $photo_url_end = $eleve->field_photo;
          $output[$key]->field_photo = 'http://' . $_SERVER['SERVER_NAME'];
          $output[$key]->field_photo .= ($_SERVER['SERVER_PORT'] != 80) ? ':' . $_SERVER['SERVER_PORT'] : "";
          $output[$key]->field_photo .= $photo_url_end;
        }

      }
    }
    // /formatage

    // injection de la variable JS
    if( $write_script ){
      echo '<script type="text/javascript"> var listEleves = '. json_encode( $output ) .' </script>';
    }

    if ( $return ){
      return array(
        "data"    => $output,
        "json"    => json_encode($output)
      );
    }
  }
  
}


 ?>
