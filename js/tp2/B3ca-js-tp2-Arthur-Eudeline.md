### Arthur Eudeline - B3ca

# JavaScript - TP2



# 1 : Gestionnaire d’événements



## 1.1 : Exemples

![ALT](../tp1/assets/image-20181018125443811.png "titre")

![image-20181018125454799](../tp1/assets/image-20181018125454799.png)



## 1.2 : Mot clé `return`

En JavaScript, et comme d'autres langages tels que le PHP, le mot clé `return` utilisé au sein d'une fonction permet de **retourner un résultat**. Ainsi les valeurs peuvent être stockées ou traitées ultérieurement :

```js
var uppercase = function( text ){
    return text.toUpperCase();
}

var monTexte = uppercase("minuscule"); // la valeur de monTexte sera MINUSCULE et non pas minuscule
```

En dehors d'une fonction, `return function()` va simplement retourner le résultat de la fonction. 



## 1.3 : L'évènement `onfocus`

L'évènement `onfocus` se déclanche lors de la sélection d'une balise `<input>` ou de tout autre élément séléctionnable. À partir de là, nous pouvons ordonner la modifications des attributs du champs séléctionné par le biais du mot clé `this` :

```html
<input type="text" onfocus="this.value='Votre Email ici !'" />
```

Ce qui affiche bien :

![image-20181018130535004](../tp1/assets/image-20181018130535004.png)



## 1.4 : L'évènement `onreset`

```html
<form method="post" action="#" onreset="window.confirm('Cela effacera les données saisies')">
    <input type="text" onfocus="this.value='Votre Email ici !'" />

    <button type="reset">Annuler</button>
</form>
```



![image-20181018131044304](../tp1/assets/image-20181018131044304.png)



## 1.5 : La méthode `getElementById()`

La méthode `document.getElementById()`, comme son nom l'indique, permet de récupérer un objet du DOM en passant par l'attribut `id` attaché à sa balise HTML. 

Il existe également la méthode `document.querySelector()` qui s'avère moins restrictive et permet de cibler des élements du DOM avec des sélécteurs également utilisés en CSS, à la manière de jQuery :

```html
<!-- Exemple de bouton -->
<button onmouseover="changeText('Visitez mon site professionnel')" onmouseour="changeText()">Mon site Pro</button>

<!-- La zone de texte à changer -->
<h3 id="text" style="text-align: center">Vous êtes sur la page d'accueil</h3>
```

```js
var zoneText = document.getElementById('text');

var changeText = function ( text ) {

	if(!text){
		text = "Vous êtes sur la page d'accueil";
	}

	zoneText.innerHTML = text;
};
```

Ce qui produit par exemple :

![image-20181018132552351](../tp1/assets/image-20181018132552351.png)



# 2 : Ajouter des traitements locaux à un formulaire



## 2.1 : Exemples

### Exemple 1

```html
<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Exemples</title>
  <meta name="description" content=""/>

  <link rel="stylesheet" type="text/css" href="#" />

  <script type="text/javascript">
  function controler(formulaire) {
    var test = document.formulaire.input.value;
    alert("Vous avez saisi : " + test);
  }
  </script>
</head>

<body>

  <form name="formulaire">
    <input type="text" name="input" value=""><br>
    <input type="button" name="bouton" value="Contr&ocirc;ler"
    onclick="controler(formulaire)">
  </form>

</body>
</html>
```

Ce qui affiche :

![image-20181024143333222](../../../../OneDrive/Cours/Développement/Symfony/assets/image-20181024143333222.png)

### Exemple 2

```html
<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Exemples</title>
  <meta name="description" content=""/>

  <link rel="stylesheet" type="text/css" href="#" />

  <script type="text/javascript">
  var nombreclic=0;
  function compteclic(form) {
    nombreclic++;
    if (nombreclic > 1)
    alert("Vous avez d\351j\340 cliqu\351 ce bouton.\nLe formulaire est en cours de traitement...");
  }
  </script>
</head>

<body>

  <form name="formulaire">
    <input type="button" value="Cliquez-moi !"
    onclick="compteclic(document.formulaire)">
  </form>

</body>
</html>
```

Ce qui affiche :

![image-20181024143554162](../../../../OneDrive/Cours/Développement/Symfony/assets/image-20181024143554162.png)



## 2.2 : Formulaire “contrôle de connaissances”

![image-20181024165915405](../../../../OneDrive/Cours/Développement/Symfony/assets/image-20181024165915405.png)

### Code HTML du formulaire

```html
<h1>Contrôle de connaissances</h1>

<form id="main-form" onsubmit="return formSubmitAction(event)" action="">

<!-- Angle Droit -->
<div id="angle-droit">
  <p><b>Un angle droit forme un angle de :</b></p>

  <p>
    <label>
      <input type="radio" name="angle-droit" data-question-id="angleDroit" value="45" />
      45°
    </label>
  </p>

  <p>
    <label>
      <input type="radio" name="angle-droit" data-question-id="angleDroit" value="90" />
      90°
    </label>
  </p>
</div>
<!-- /Angle Droit -->

<!-- Triangle Isocèle -->
<div id="triangle-isoscele">
  <p><b>Un triangle isocèle possède :</b></p>

  <p>
    <label>
      <input type="checkbox" value="2" data-question-id="triangleIsoscele" />
      Deux côtés égaux
    </label>
  </p>

  <p>
    <label>
      <input type="checkbox" value="3" data-question-id="triangleIsoscele" />
      Trois côtés égaux
    </label>
  </p>
</div>
<!-- /Triangle Isocèle -->

<!-- Triangle Équilatéral -->
<div id="triangle-equilateral">
  <label>
    <p><b>Un triangle Équilatéral possède :</b></p>
    <select data-question-id="triangleEquilateral">

      <option disabled selected>
        Réponse
      </option>

      <option value="2">
        Deux côtés égaux
      </option>

      <option value="3">
        Trois côtés égaux
      </option>

    </select>
  <label>
</div>
<!-- /Triangle Équilatéral -->

<p>
  <button type="submit">Vérifier</button>
  <button type="reset">Recommencer</button>
</p>
</form>

<h3 id="display-zone"></h3>
```

### Script JS

#### Objet Questions

```js
var questions = {
    "angleDroit" : {
      "submitedAnswer" : "",
      "correctAnswer" : "90",
      "isCorrect" : function(){
        if( this.submitedAnswer === this.correctAnswer ){
          return true;
        } else {
          return false;
        }
      }
    },

    "triangleIsoscele" : {
      "submitedAnswer" : "",
      "correctAnswer" : "2",
      "isCorrect" : function(){
        if( this.submitedAnswer === this.correctAnswer ){
          return true;
        } else {
          return false;
        }
      }
    },

    "triangleEquilateral" : {
      "submitedAnswer" : "",
      "correctAnswer" : "3",
      "isCorrect" : function(){
        if( this.submitedAnswer === this.correctAnswer ){
          return true;
        } else {
          return false;
        }
      }
    }
};
```

#### Action à la soumission du formulaire

```js
function formSubmitAction(event){
    // empêche la page de refresh
    event.preventDefault();

    // On stocke l'objet DOM form et on évite de reparcourir le DOM si on l'a déjà stocké
    if($form === null){
      $form = document.getElementById("main-form");
    }

    // ------------------- Mise à jour de l'objet questions -------------------
    // On parcours les éléments qui composent le formulaire (ne retourne que les inputs, selects, buttons, etc...)
    for(var i = 0; i < $form.elements.length; i++){
      // Si la question existe bien dans l'objet
      if( questions[$form.elements[i].dataset.questionId] !== undefined ){

        // S'il s'agit d'une checkbox ou d'un radio
        if( $form.elements[i].tagName === "INPUT" && ($form.elements[i].type === "radio" || $form.elements[i].type === "checkbox") ){
          // Et si ce radio ou cette checkbox est checkée
          if($form.elements[i].checked){
            // La propriété "submitedAnswer" pour la question prend la valeur de la case cochée
            questions[$form.elements[i].dataset.questionId].submitedAnswer = $form.elements[i].value;
          }
        }

        // S'il ne s'agit pas d'une checkbox ou d'un radio
        else {
          questions[$form.elements[i].dataset.questionId].submitedAnswer = $form.elements[i].value;
        }
      }
    }// /for

    // ------------------- Comptage du nombre de bonnes réponses -------------------
    var questionsCount = 0;
    var correctAnswersCount = 0;
    for(question in questions){
      if( questions[question].isCorrect() ){
        correctAnswersCount++;
      }
      questionsCount++;
    }

    // ------------------- Affichage du nombre de bonnes réponses -------------------
    if($displayZone === null){
      $displayZone = document.getElementById("display-zone");
    }

    $displayZone.innerHTML = "Vous avez " + correctAnswersCount + " réponses correcte(s) sur un total de " + questionsCount + " question(s)";
  }
```

