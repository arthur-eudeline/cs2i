### Arthur Eudeline B3ca

# jQuery TP5



## Exercice 1 : Apprendre à utiliser jQuery UI



### 1.1 : DatePicker

```html
<!doctype html>
<html lang="fr">
<head>
	...
</head>
<body>

<h1>Date Picker</h1>
<input id="datePicker">

<script src="../jquery-ui/external/jquery/jquery.js"></script>
<script src="../jquery-ui/jquery-ui.js"></script>
  
<!-- traductions françaises -->
<script src="../jquery-ui/ui/i18n/datepicker-fr.js"></script>

<script>
  jQuery(document).ready(function($){
    $('#datePicker').datepicker( $.datepicker.regional[ "fr" ] );
  });
</script>

</body>
</html>
```

Ce qui permet simplement d'obtenir :

![image-20181231151923865](assets/image-20181231151923865-6265963.png)



### 1.2 : Progress bar

```html
<!doctype html>
<html lang="fr">
<head>
  ...
</head>
<body>

<h1>Progressbar</h1>
<div id="progressbar"></div>

<script src="../jquery-ui/external/jquery/jquery.js"></script>
<script src="../jquery-ui/jquery-ui.js"></script>
<script>
  jQuery(document).ready(function ($) {
    $('#progressbar').progressbar({
      value: 0
    });

    // Progression de la barre
    var interval = window.setInterval(
      function () {
        $('#progressbar').progressbar("value", $('#progressbar').progressbar("value") + 1);
      },
      50
    );

    // Détruit l'interval à la fin de la barre
    $('#progressbar').on('progressbarcomplete', function(){
      window.clearInterval( interval );
    });

  });
</script>

</body>
</html>
```

![image-20181231153258281](assets/image-20181231153258281-6266778.png)



## Exercice 2 : les plugins jQuery



### 2.2 : Diaporama

![image-20181231160239756](assets/image-20181231160239756-6268559.png)

```html
<!doctype html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css">
  
  <title>Swiper</title>
</head>
<body>

<h1>Swiper</h1>

<!-- Slider main container -->
<div class="swiper-container">
  <div class="swiper-wrapper">
    <!-- Slides -->
    <div class="swiper-slide">
      <div style="margin:15px; background-color:#aaa">
        <h1>Slide textuelle</h1>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad architecto consectetur in ipsum iste? At atque
          blanditiis eius laborum quo sit, velit voluptate! Accusamus error labore odit, quaerat temporibus voluptates.
        </p>
      </div>
    </div>
    <div class="swiper-slide">
      <img src="https://www.w3schools.com/w3css/img_lights.jpg" style="margin: 0 auto; display:block;">
    </div>
  </div>
  
  <!-- Pagination (facultative) -->
  <div class="swiper-pagination"></div>
  
  <!-- bouton de navigation (facultatifs) -->
  <div class="swiper-button-prev"></div>
  <div class="swiper-button-next"></div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>

<script>
  var mySwiper = new Swiper('.swiper-container', {
    speed: 400,
    spaceBetween: 100,
    loop: true,

    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
    },

    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    }
  });
</script>

</body>
</html>
```

