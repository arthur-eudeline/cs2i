### Arthur Eudeline - B3ca

# JavaScript - TP4



## 1.1 : Tester les exemples du cours

### Page 9 :

```html
<html>
<script type="text/javascript"
src="http://code.jquery.com/jquery-latest.min.js">
</script>
<body>
<div id="monDiv">Bonjour</div>
<a href="#" onClick="$('#monDiv').hide();">
disparition</a>
</body>
</html>
```

Ce qui permet de cacher la `<div>` possédant l'`id` *monDiv* grâce à l'emploi de la méthode `jQueryObject.hide()`



### Page 12 :

```html
<html>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function($){
        // Seléctionne tous les li impaires du document et injecte l'HTML contenu dans .prepend() après la balise ouvrante
        $("li:odd").prepend('<span>Changed</span>').css({background:"red"});
    });
</script>
<body>
<ul>
    <li>
        First item
    </li>
    <li>
      	<!-- emplacement d'injection du code passé à .prepend() -->
        Second item
    </li>
    <li>
        Third item
    </li>
</ul>
</body>
</html>
```



### Page 13 :

```html
<html>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function($){
        // Séléctionne une div qui est cachée, cherche un élément avec la classe .foo, vide son contenu, le remplace par le texte "changed" et affiche la div initialement selectionnée
        $("div:hidden").find(".foo").empty().text("Changed").end().show();
    });
</script>
<body>
	<div>
		<span class="foo">
			Some text
		</span>
	</div>
	<div style="display:none">
		<span>
			More text
		</span>
    <span class="foo">
      <!-- le texte qui va être remplacé par "changed" -->
			Goodbye cruel world.
		</span>
	</div>
</body>
</html>
```



### Page 14 :

```html
<html>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function($){
        // Se déclanche quand la checkbox change d'état ( changement de :checked)
        $('#checkbox').change( function(){
            if ($('#total').prop('checked')) {
                console.log("Case cochée");
            } else {
                console.log("Case décochée");
            }
        } );
    });
</script>
<body>

    <label>
        <input id="checkbox">
        Ma super checkbox
    </label>

</body>
</html>

```



### Page 15 :

```html
<html>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function($){
        // Se déclanche à la soumission du form avec l'id #ok
        $('#ok').submit(function() {
            // On regarde si le champ avec l'id "login" a bien une valeur, s'il est vide on affiche une popup et on empêche la soumission du form
            if ($('#login').val() =='') {
                alert ('Entrer un login');
                return false;
            }
        })
    });
</script>
<body>

    <form id="ok">
        <label>
            <p>Ma super checkbox</p>
            <input type="text" id="login" placeholder="Login...">
        </label>
    </form>

</body>
</html>
```



### Page 16 :

```html
<html>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function($){
        // Se déclanche quand on sélectionne le champ avec l'id "nom"
        $('#nom').focus(function() {
            $(this).val(''); // vide sa valeur (donc son contenu étant donné que c'est un input)
        } );
    });
</script>
<body>

<label>
    <p>Mon super champ :</p>
    <input id="nom" value="Mon contenu qui va disparaître">
</label>

</body>
</html>
```





### Page 17 :

```html
<html>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function($){
        // détecte le click sur n'importe quel input de type radio, on peut également utiliser .change() pour plus de précision
        $(':radio').click(function() {
            console.log("Un bouton radio à été clické !");
            console.log( $(this) ); // On console.log l'objet jQuery du radio sur lequel on a cliqué
        });
    });
</script>
<body>

<div>
    <label>
        <input type="checkbox" value="input_1">
        Mon super champ n°1
    </label>
</div>

<div>
    <label>
        <input type="checkbox" value="input_2">
        Mon super champ n°2
    </label>
</div>


</body>
</html>
```



### Page 18 :

```html
<html>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function($){
        // Quand on clique sur un el avec la classe "none", sélectionne tous les élements dans la même balise qui sont des checkbox (grâce au selecteur passé à .siblings())
        $(".none").click(function() {
            // Décoche toutes les checkboxs
            $(this).siblings(":checkbox").prop("checked", false);
        } );
        
        $(".all").click(function() {
            $(this).siblings(":checkbox").prop("checked", true);
        } );
    });
</script>
<body>

<div>
    <button class="all">Select All</button>
    <button class="none">Select None</button>
    <input name="chk1" type="checkbox"/>
    <input name="chk2" type="checkbox"/>
    <input name="chk3" type="checkbox"/>
</div>
<div>
    <button class="all">Select All</button>
    <button class="none">Select None</button>
    <input name="chk4" type="checkbox"/>
    <input name="chk5" type="checkbox"/>
    <input name="chk6" type="checkbox"/>
</div>

</body>
</html>
```

##  Pratique 2 : La librairie jQuery : Simplification d'AJAX avec jQuery

### 2.1 : exemple de la page 21

#### Création de `courses.xml` :

```xml
<?xml version = "1.0" encoding="UTF-8" standalone="yes" ?>
<root>
  <product id="produit-1">
    <name>Benco</name>
    <price>3.50</price>
  </product>

  <product id="produit-2">
    <name>DVD : Camping</name>
    <price>9.99</price>
  </product>
</root>
```

#### Création du fichier HTML

```html
<!doctype html>
<html lang="fr">
<head>
	...
</head>

<body>

<h1>Liste de courses : </h1>
<div id="coursesList"></div>

<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>
  jQuery(document).ready(function($){
    $.ajax({ type: "GET", url: "courses.xml",
      dataType: "xml",
      contentType: "application/xml; charset=UTF-8",
      complete : function(data, status) {
        var products = data.responseXML;
        var html = "";
        $(products).find('product').each(function(){
          var id = $(this).attr('id');
          var name = $(this).find('name').text();
          var price = $(this).find('price').text();
          html += "<li>#"+id
            + " - <strong>"+name+"</strong> : "
            + price + "</li>";
        });
        $("#coursesList").html(html);
        console.log(data);
      }
    });
  });
</script>
</body>
</html>
```

On rajoute à la méthode `jQuery.ajax()` le paramètre `contentType: "application/xml; charset=UTF-8"` pour maximiser la compatibilité.



### 2.2 : Reprise de l'exemple du TP3

Afin de se débarasser de la contrainte de passer par des scripts PHP et un serveur, on stoque directement le XML final dans un fichier `authors.xml` :

```xml
<?xml version = "1.0" encoding="UTF-8" standalone="yes" ?>
<root>
  <author id="1">
    <name>Lewis Carrol</name>
    <books>
      <book>
        <title>
          Alice au pays des merveilles
        </title>
      </book>
      <book>
        <title>
          La Chasse au Snark
        </title>
      </book>
      <book>
        <title>
          Sylvie et Bruno
        </title>
      </book>
    </books>
  </author>

  ...
</root>
```

On reprend ensuite le fichier HTML précédemment créé lors du TP3 :

```html
<div class="container">
  <div class="row">
    <h1 class="col-xs-12">Liste des livres</h1>
    
    <div class="col-xs-12 col-sm-8 padding">
      <div class="row">
        
        <!-- Selection auteur -->
        <label class="control-label col-xs-12 padding">
          <p>Auteurs :</p>
          
          <select id="auteurs-select" class="form-control">
            <!-- Insertion de la liste des auteurs -->
          </select>
        </label>
        <!-- /Selection auteur -->
        
        <!-- Affichage des livres -->
        <label class="control-label col-xs-12 padding">
          <p>Livres de l'auteur :</p>
          <select id="select-livres-auteur" class="form-control" disabled>
            <!-- Insertion de la liste des livres -->
          </select>
        </label>
      </div>
    </div>
    
    <div class="col-xs-12 col-sm-4 padding">
      <div class="row">
        <div class="col-xs-12 padding">
          
          <p><b>Livres de l'auteur :</b></p>
          
          <table id="table-livres-auteur" class="table table-striped table-bordered col-xs-12">
            <thead>
            <th>Nom du livre</th>
            </thead>
            
            <tbody>
              <!-- Insertion de la liste des livres -->
            </tbody>
          </table>
        
        </div>
      </div>
    </div>
  
  </div>
</div>
```

Puis enfin, nous ajoutons le jQuery :

```javascript
  jQuery(document).ready(function ($) {
    var $authors = "";
    var $selectAuteurs = $("#auteurs-select");
    var $selectLivres  = $("#select-livres-auteur");
    var $tableLivre    = $("#table-livres-auteur tbody");
    
    // On effectue l'appel Ajax pour récupérer toutes les données :
    $.get(
      'authors.xml',
      function( data ){
        $authors = $(data);
        
        // On rempli la liste d'auteur
        fillAuthorList();
        
        // On charge les livres du premier auteur, qui est sélectionné par défaut
        refreshBookList(1);
      }
    );

    /**
     * Remplissage de la liste déroulante de séléction des auteurs
     */
    function fillAuthorList(){
      if ($authors !== ""){
        $authors.find('author').each(function () {
          var option = "";

          option += "<option value='" + $(this).attr("id") + "'>";
          option += $(this).find("name").html();
          option += "</option>";

          $selectAuteurs.append(option);
        });
      } else {
        console.error("Liste des auteurs vide !");
      }
    }
    
    /**
     * Change la liste des livres
     */
    function refreshBookList( authorID ){
      var bookList = $authors.find('author#' + authorID + ' books book');
      
      // Select
      $selectLivres.empty(); // on vide les livres existants
      $(bookList).each(function(){
        var option = "";
        
        option += "<option>";
        option += $(this).find("title").html();
        option += "</option>";
        
        $selectLivres.append(option);
      });
      $selectLivres.removeAttr("disabled");
      
      // Table
      $tableLivre.empty();
      $(bookList).each(function(){
        var row = "";
        
        row += "<tr>";
        row += "<th>" + $(this).find('title').html() + "</th>";
        row += "</tr>";
        
        $tableLivre.append(row);
      });
    }

    /**
     * À chaque fois qu'on change d'auteur
     */
    $selectAuteurs.on("change", function() {
      refreshBookList( $(this).val() );
    });
  });
```

![image-20181228005945577](assets/image-20181228005945577-5955185.png)



### 2.3 : Modification de la `<table>` pour afficher plus d'informations

#### Modification de la structure XML

Dorénavant, les livres auront une structure basée sur ce modèle :

```xml
<book id="3">
  <title>
    Sylvie et Bruno
  </title>
  <author>
    Lewis Carrol
  </author>
</book>
```



#### Modification de la structure HTML

```html
<p><b>Livres :</b></p>
          
<table id="table-livres-auteur" class="table table-striped table-bordered col-xs-12">
  <thead>
  <th>ID</th>
  <th>Titre</th>
  <th>Auteur</th>
  </thead>
            
  <tbody>
    <!-- Insertion de la liste des livres -->
  </tbody>
</table>
```



#### Modification du JavaScript

```javascript
/**
 * Change la liste des livres
 */
function refreshBookList( authorID ){
  var bookList = $authors.find('author#' + authorID + ' books book');
      
  // Select
  // ...
      
  // Table
  $tableLivre.empty();
  $(bookList).each(function(){
    var row = "";
        
    row += "<tr>";
    row += "<th>" + $(this).attr('id') + "</th>";
    row += "<th>" + $(this).find('title').html() + "</th>";
    row += "<th>" + $(this).find('author').html() + "</th>";
    row += "</tr>";
        
    $tableLivre.append(row);
  });
}
```



#### Résultat

![image-20181228010854255](assets/image-20181228010854255-5955734.png)



### 2.4 : Forçage de l'utilisation de la méthode `POST`

Pour cela, nous avons deux options :

#### Utilisation de la méthode `jQuery.ajax()` 

Nous pouvons simplement changer la méthode utilisée avec l'option `type` :

```javascript
$.ajax({
  type: "POST",
  url: "authors.xml",
  dataType: "xml",
  complete: function (data, status) {
    $authors = $(data.responseXML);

    // On rempli la liste d'auteur
    fillAuthorList();

    // On charge les livres du premier auteur, qui est sélectionné par défaut
    refreshBookList(1);
  }
});
```

### Utilisation de la méthode `jQuery.post()` 

Au même titre que la méthode `jQuery.get()`, la méthode `jQuery.post()` simplifie l'envoie de requêtes Ajax avec la méthode `POST` :

```javascript
$.post(
  'authors.xml',
  function( data ){
    $authors = $(data);
        
    // On rempli la liste d'auteur
    fillAuthorList();
        
    // On charge les livres du premier auteur, qui est sélectionné par défaut
    refreshBookList(1);
  }
);
```



#### Vérification

On remarque bien que la ligne `Request Method` indique `POST` :

![image-20181228011707547](assets/image-20181228011707547-5956227.png)



## 3 : La librairie jQuery

Voilà les notes prises durant la lecture des différentes sections :



### jQuery Tutorial

----

```js
jQuery(document).ready(function($){ 
	// code à exécuter quand le document est prêt
});
```

Attend que le document soit chargé (scripts, links, images), avant d'éffectuer le code situé dans la fonction. Fonctionne même si une autre librairie utilise `$`, comme c'est le cas lorsque l'on travaille sur WordPress.

---

```js
$('__SELECTEUR_CSS__')
```

Retourne un objet jQuery ciblant tous les élements du DOM ciblés par le selecteur CSS passé en argument.

> Note : [30 selecteurs CSS utiles](https://code.tutsplus.com/fr/tutorials/the-30-css-selectors-you-must-memorize--net-16048)

------

```js
$('element').on('click touch', function(){
  // appelé à chaque click ou touch sur l'élément
});

// Appel spécifique d'une fonction en fonction de l'évènement déclanché
$("element").on({
  mouseenter: function(){
    $(this).css("background-color", "lightgray");
  },
  mouseleave: function(){
    $(this).css("background-color", "lightblue");
  }
}); 

// Version raccourcie spécifique à un événement particulié
$("element").click(function(){ ... });
```

Appelle la fonction à chacun qu'un des éléments spécifiés est déclanché sur l'élément.

> Note : [évènements jQuery](https://www.w3schools.com/jquery/jquery_ref_events.asp)



### jQuery Effects :

----

```js
$('element').hide( __DURATION_MS__ );
$('element').show( __DURATION_MS__ );

// Alterne automatiquement entre hide et show en fonction de si l'élément est déjà caché ou non
$('element').toggle( __DURATION_MS__ );
```

Anime une dissimulation d'un élément en réduisant sa taille 

----

```js
$("selecteur").fadeIn( __DURATION_MS__ );
$("selecteur").fadeOut( __DURATION_MS__ );

// Alterne automatiquement
$("selecteur").fadeToggle( __DURATION_MS__ ); 
```

Anime la dissimulation d'un élément en jouant sur son opacité avant de le passer en `display:none`

---

```js
// Cache
$("selecteur").slideUp( __DURATION_MS__ );
// Montre
$("selecteur").slideDown( __DURATION_MS__ );

// Alterne automatiquement
$("selecteur").slideToggle( __DURATION_MS__ ); 
```

Anime la dissimulation d'un élément en animant sa hauteur de 0 à 100%

---

```js
$("selecteur").animate(
  {
  	left: '250px', 				// déplace de la position actuelle de left jusqu'à 250px
  	height: '+=150px', 		// ajoute 150px à la hauteur, -= pour retirer
  	opacity: '0.5',				// anime la transition de l'opactité actuelle à 0.5
  	width: 'toggle'				// anime la largeur de l'élément de 0 à 100% en fonction de son état initial (show|hide|toggle)  
	},
  __DUREE_MS__,
  __ANIMATION_CURVE__,		// défini la courbe de vitesse de l'animation (swing|linear)
  function(){
    // action a effectuer à la fin de l'animation
  }
);
```

Permet d'animer précisément les propriété CSS d'un élément avec une durée spécifique. Ces animations se basent sur la modification de l'attribut `style` de l'élément qui est alors animé pour chaque propriété en faisant augmenter ou décroître la valeur de la propriété de façon à ce quelle passe de son point initial au point spécifié par l'utilisateur, le tout suivant une courbe d'accélération ou étant linéaire suivant le paramètre `__ANIMATION_CURVE__` pouvant prendre la valeur `swing` pour une accélération courbée, ou `linear` pour une accélération constante.

---

```js
$('selecteur').stop()
```

Permet de stopper une animation en cours sur un élément à un instant T.

---

```
$('selecteur').slideDown().slideUp();
```

Permet d'enchaîner les animations.



### jQuery HTML

---

```js
$('selecteur').text();		// retourne le contenu textuel d'un élément
$('selecteur').html();		// retourne le contenu textuel ET le contenu HTML
$('selecteur').val();			// retourne le contenu d'un input

$('selecteur').text( __CONTENT__ );		// remplace le contenu textuel d'un élément
$('selecteur').html( __CONTENT__ );		// remplace le contenu textuel ET le contenu HTML
$('selecteur').val( __CONTENT__ );		// remplace le contenu d'un input
```

Les méthodes détaillées ci-dessus jouent à la fois le rôle de getter et de setter.

---

```js
$('selecteur').append( __CONTENT__ );
$('selecteur').prepend( __CONTENT__ );
$('selecteur').after( __CONTENT__ );
$('selecteur').before( __CONTENT__ );
```

Permet d'insérer du contenu HTML à un endroit spécifique par rapport aux éléments séléctionnés à la manière décrite ci-dessous :

```html
<!-- insertion de before 1er -->
<!-- insertion de before 2e -->
<div id="ma-div-ciblée">
  <!-- insertion de prépend 2e -->
	<!-- insertion de prépend 1er -->
  <p>Mon contenu de base</p>
  <!-- insertion de append 1er -->
  <!-- insertion de append 2e -->
</div>
<!-- insertion de after 1er -->
<!-- insertion de after 2e -->
```

---

```js
$('selecteur').remove();		// supprime la balise du sélécteur et son contenu
$('selecteur').empty(); 		// supprime tout le contenu de la balise du sélécteur (y compris l'HTML)
```

---

```js
$('selecteur').addClass('ma-classe-css');			// ajoute la classe css si elle n'y est pas déjà (ne pas mettre le .) 
$('selecteur').removeClass('ma-classe-css');  // supprime la classe si l'élément l'a
$('selecteur').toggleClass('ma-classe-css');	// ajoute ou supprime la classe
```

Ces méthodes est son extrêments utiles pour animer avec du CSS, car on peut accèder à plus de parmètres que `jQuery.animate()` le permet. On accède ainsi à toutes les propriétés d'animations CSS 3, mais en perd en compatibilité inter-navigateur, contrairement à ce que l'utilisation de `jQuery.animate()` permet.

---

```js
$('selecteur').css( __NOM_PROPRIETE_CSS__ );		// récupère la valeur de la propriété css appliquée à l'élément
$('selecteur').css( __NOM_PROPRIETE_CSS__, __VALEUR__);		// attribue une nouvelle valeur pour la propriété indiquée

// Permet de réatribuer la valeur de plusieurs propriétés en même temps
$('selecteur').css({
  'background-color' : 'yellow',
  __NOM_PROPRIETE_CSS__ : __VALEUR__
});
```

----

```js
// récupère la largeur ou la hauteur de l'élément, sans prendre en compte padding, margin et border
$('selecteur').width(); 			
$('selecteur').height();

// récupère la largeur ou la hauteur de l'élément, en prenant en compte son padding
$('selecteur').innerWidth();	
$('selecteur').innerHeight();

// récupère la largeur ou la hauteur de l'élément, en prenant en compte son margin et ses borders
$('selecteur').innerWidth();	
$('selecteur').innerHeight();
```



### jQuery Traversing

Fait référence à la manière dont il est possible de naviguer à travers le DOM du document HTML avec jQuery.

---

#### Remonter le DOM

```js
$('selecteur').parent();		// retourne l'instance jQuery de l'élément parent direct du selecteur
$('selecteur').parents();		// retourne un tableau de tous les parents de l'élément jusqu'au root (<html>)

// retourne un tableau de tous les parents de l'élément jusqu'à ce qu'un élément correspondant aux critères du second selecteur soit rencontré.
$('selecteur').parentsUntil('selecteur2'); 
```

---

#### Descendre le DOM

```js
$('selecteur').children(); 					// retourne un tableau de tous les enfants DIRECT de l'élément

// retourne les éléments enfants correspondant au critères du selecteur (* selectionne tous les enfants)
$('selecteur').find('selecteur2');
```

---

### Parcourir le DOM horizontalement

Les `siblings` ou frères et soeur en français, désignent tous les éléments situés au même niveau du DOM. Cela fait donc référence aux éléments partageant le même parent :

```html
<div id="parent">
  
  <div id="enfant1">
    <!-- p n'est pas considéré comme un enfant direct de #parent, ne peut donc être DIRECTEMENT attein pour les méthodes de ciblages de frère sur un élément #enfant1, #enfant2 ou #enfant3 -->
    <p></p>
  </div>
  
  <h2 id="enfant2"></h2>
  
  <img id="enfant3">
</div>
```

> Nous nous baserons sur cette structure pour les exemples suivants

---

```js
$("#enfant1").siblings(); 			// retourne un tableau contenant #enfant2 et #enfant3
$('#enfant2').siblings('div'); 	// retourne #enfant1 puisque c'est le seul frère de type <div>

$('#enfant2').next();						// retourne #enfant3
$('#enfant2').before();					// retourne #enfant1

$('#enfant1').nextAll();				// retourne #enfant2 et #enfant3
$('#enfant3').beforeAll();			// retourne #enfant1 et #enfant2

$('#enfant1').nextUntil('h2');	// retourne tous les frères jusqu'à ce qu'on rencontre un élément correspondant au selecteur
$('#enfant1').prevUntil('selecteur');
```

---

```js
$('selecteur').first();					// retourne la première occurence rencontrée
$('selecteur').last();					// retourne la dernière occurence rencontrée
$('selecteur').eq( n );					// retourne la n-ème occurence rencontrée

$('p').filter('.intro');				// = $('p.intro');
$('p').not('.intro');						// retourne tous les p qui n'ont pas la classe 'intro' ou tout autre selecteur passé dans not()
```

