### Arthur EUDELINE B3A

# JavaScript TP1



## 1.1 : Deux mises en pratique d'exemples

![Capture d'écran 2018-10-09 à 14.06.53](./assets/screen01.png)

```html
<html>
<head> <title>Invite</title> </head>
<body>
	<script type="text/javascript">
	var prenom = prompt("Saisir votre prenom", "Votre prenom ici");
	document.write("<h2>Bonjour " + prenom + "</h2>");
</script>
</body>
</html>

```



------



![Capture d'écran 2018-10-09 à 14.06.53](assets/screen02.png)



```html
<html>
<head> <title>Confirmation</title> </head>
<body>
	<script type="text/javascript">
	var a = confirm("Cliquer sur OK ou Annuler");
	if (a == true) {
		alert("Vous avez cliqué sur OK");
	} else {
		alert("Vous avez cliqué sur Annuler");
	}
</script>
</body>
</html>

```



## 1.2 : Nombres impaires

```html
<html>
	<head>
		<title>Nombres Impaires</title>
	</head>

	<body>

	</body>

	<script type="text/javascript">

		var n = window.prompt("Saisissez un nombre :", 4);

		while (!Number.isInteger(parseInt(n))){
			n = window.prompt("Saisissez un nombre ENTIER :", 4);
		}

		document.write("<h2>Liste des nombres impaires entre 0 et " + n + " :</h2>");
		document.write("<ul>");
		for(var i=0; i <= n; i++){
			if(i%2 === 1){
				document.write("<li>"+ i +"</li>");
			}
		}
		document.write("</ul>");
	</script>
</html>
```

Ce qui donne l’affichage suivant par exemple si on passe 4 au programme :

![Capture d'écran 2018-10-09 à 14.42.09](assets/screen03.png)

## 1.3 : Table de multiplication

```js
var max = window.prompt("Afficher la table de multiplication jusqu'au nombre :", 10);

while (!Number.isInteger(parseInt(max))){
	max = window.prompt("Afficher la table de multiplication jusqu'au nombre :", 10);
}

renderMultiplicationTable(max);

function renderMultiplicationTable (n){

	document.write("<h1>Tables de multiplication de 0 à " + n + " :</h1>");
	document.write("<table>");

	for( var x = 0; x <= n; x++ ){
		document.write("<tr>");
		if( x === 0){
			document.write("<th>X</th>");
		} else {
			document.write("<th>"+ x +"</th>");
		}

		for( var y = 1; y <= n; y++ ){
			if(x === 0){
				document.write("<th>"+ y +"</th>");
			} else{
				document.write("<td>"+ x*y +"</td>");
			}



		}
		document.write("</tr>");
	}

	document.write("</table>");
}
```

![Capture d'écran 2018-10-09 à 15.06.53](assets/screen04.png)



## 1.4 : Affichage d’un sapin

```js
var n = window.prompt("Nombre de branches du sapin :", 5);

while (!Number.isInteger(parseInt(n))){
	n = window.prompt("Nombre de branches du sapin :", 5);
}

renderSapin(n);

function renderSapin (lignes){
	for(var x=0; x <= lignes; x++){
		document.write("<div>");
		for(var etoile = 0; etoile < (x * 2 - 1); etoile++){
			document.write("*");
		}
		document.write("</div>");
	}
}
```

![Capture d'écran 2018-10-09 à 15.43.09](assets/screen05.png)

## 2.1 : Programme démontrant l'utilisation de l'objet `Math`

![screen06](assets/screen06.png)

## 2.2 : 10 chiffres aléatoires

```js
function bigRandom(){
	document.write('<h1>10 chiffres random : </h1>');
	document.write('<ul>');
	for(var i=0; i < 10; i++){
		document.write("<li>"+Math.round(Math.random() * 1000) +"</li>");
	}
	document.write('</ul>');
}

bigRandom();
```

![Capture d'écran 2018-10-09 à 16.25.38](assets/screen07.png)



## 2.3 : Deviner un chiffre entre 1 et 1000

![Capture d'écran 2018-10-09 à 16.39.22](assets/screen08.png)

```js
document.write('<h1>Devinez le chiffre entre 1 et 1000 </h1>');
var chiffre = Math.round(Math.random() * 1000);
var reponse = window.prompt("Votre proposition du chiffre à deviner ?");
document.write('<ol>');
while(parseInt(reponse) !== chiffre ){
	document.write("<li>Réponse : "+ reponse +"</li>");

	if(reponse < chiffre){
		window.alert("Plus grand");
	} else {
		window.alert("Plus petit");
	}
	reponse = window.prompt("Votre proposition du chiffre à deviner ?");
}
document.write('</ol>');

window.alert("Gagné !!!!");
```



## 3.1 : Exemples

```js
var chaine = "informatique";
document.write("J'épelle : ");
for (i=0; i<chaine.length; i++) document.write (chaine.charAt(i), "-");

var chaine = "informatique";
var s_ch = "ma";
var car = "i";
var position = 2;
document.write ("<br /><br />1ère position de ", s_ch, " dans ", chaine," est : ", chaine.indexOf( s_ch),"<br>");
document.write ("position de ", car ," dans ", chaine," à partir de la position ", position," est : ", chaine.indexOf(car, position), "<br>");
```
Ce qui affiche :

![image-20181017141329449](../../java/tp2/assets/image-20181017141329449.png)



## 3.2 : Calcul du nombre de lettres dans un message donné 

```js
var text = window.prompt("Saisissez un mot pour connaître le nombre de lettres qui le composent.");
document.write("<h1>Nombre de lettres :</h1>");
document.write("<p>Vous avez saisi le mot \"<b>"+ text +"</b>\"</p>");
document.write("<p>Ce mot comporte <b>"+ text.length +" caractères</b></p>");
```



Ce qui affiche :

![image-20181017141915832](../../java/tp2/assets/image-20181017141915832.png)



## 3.3 : Mot à l'envers 

```js
var text = window.prompt("Saisissez un mot pour l'inverser.");

var textReverse = text.split('').reverse().join("");

document.write("<h1>Mot inversé :</h1>");
document.write("<p>Vous avez saisi le mot \"<b>"+ text +"</b>\"</p>");
document.write("<p>inversion : <b>"+ textReverse +"</b></p>");
```

Ce qui affiche :

![image-20181017142606884](../../java/tp2/assets/image-20181017142606884.png)



## 3.4 : détection palindrome

```js
var text = window.prompt("Saisissez un mot pour l'inverser.");

var textReverse = text.split('').reverse().join("");

var isPalindrome = false;
if(text.toLowerCase() === textReverse.toLowerCase()){
    isPalindrome = true;
}

document.write("<h1>Détection Palindrome :</h1>");
document.write("<p>Vous avez saisi le mot \"<b>"+ text +"</b>\"</p>");
document.write("<p>inversion : <b>"+ textReverse +"</b></p>");
document.write("<p>Le mot est un palindrome : <b>"+ isPalindrome +"</b></p>");
```

Ce qui affiche :

![image-20181017143332590](../../java/tp2/assets/image-20181017143332590.png)



## 4.1 : L'objet `Array`, exemples :

```js
	var semaine = new Array(7);
	semaine[0] = "Lundi" ;
	semaine[1] = "Mardi" ;
	semaine[2] = "Mercredi" ;
	semaine[3] = "Jeudi" ;
	semaine[4] = "Vendredi" ;
	semaine[5] = "Samedi" ;
	semaine[6] = "Dimanche" ;
	semaine.sort();
	for (i=0; i<7; i++)  document.write(semaine[i] + "<br>");

	var mois= new Array(12);
	document.write("Il y a " + mois.length + "mois dans l'année");
	var NbTrim = mois.length / 3;
	document.write(" partagés en " + NbTrim + " trimestres");
```

Ce qui affiche :

![image-20181017144311756](../../java/tp2/assets/image-20181017144311756.png)



## 4.2 : Affichage et opérations sur un tableau

```js
document.write("<h1>Tableaux : Affichage et moyenne </h1>");
var array = [1,2,3,4,5];

var somme = 0;

document.write("<table style='width:80%'><tr>");
for (var i = 0; i < array.length; i++){
    document.write("<td>"+ array[i] +"</td>");
    somme = somme + array[i];
}
document.write("</tr></table>");

document.write("<h4>Moyenne des éléments contenus dans le tableau : "+ (somme/array.length) +"</h4>");

document.write("<h4>Plus petite valeur contenue dans le tableau : "+ (Math.min(...array)) +"</h4>");
document.write("<h4>Plus grande valeur contenue dans le tableau : "+ (Math.max(...array)) +"</h4>");
```

Ce qui affiche :

![image-20181017145704714](../../java/tp2/assets/image-20181017145704714.png)



## 5.1 : Exemples 

```JS
document.write("<h1>Date : exemple </h1>");
var aujourdhui= new Date();
var maDate = new Date ("November 24, 1981");
var jour = maDate.getDate();         // jour vaut  24.
document.write("Nous étions le  " + jour + "/" +
(maDate.getMonth()+1) + "/" + (maDate.getYear()+1900) + "<br>");
document.write("Nous sommes le " + aujourdhui.getDate() + "/" +
(aujourdhui.getMonth()+1) + "/" + (aujourdhui.getYear()+1900) );
```

Ce qui affiche

![image-20181017153754813](../../java/tp2/assets/image-20181017153754813.png)



## 5.2 : Manipulation d'objet Date

![image-20181017155559125](../../java/tp2/assets/image-20181017155559125.png)

```js
var getDate = function(date, localize){

    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

    var output;

    if( !date || date === "now" ){
        output = new Date()
    } else {
        output = new Date(date);
    }

    if( localize ){
        return output.toLocaleDateString('fr-FR', options );
    } else {
        return output;
    }
};

var date70 = getDate("0");
console.log("*** Date Obj 01 Janvier 1970 ***");
console.log(date70);

var currentDate = getDate();
console.log("*** Date d'aujourd'hui ***");
console.log(currentDate);

var secondsSince70 = currentDate.getTime();
console.log("*** Nombre de secondes depuis le 01 Janvier 1970 ***");
console.log(secondsSince70);

var secondsScince2000 = currentDate.getTime() - getDate("2000-01-01").getTime();
console.log("*** Nombre de secondes depuis le 01 Janvier 2000 ***");
console.log(secondsScince2000);
```



## 6.1 : Timeout, exemple

![image-20181017160509482](../../java/tp2/assets/image-20181017160509482.png)



## 6.2 : Horloge

```html
<h1 id="hour" style="text-align:center; font-family: sans-serif">00:00:00</h1>
<h3 id="day" style="text-align:center; font-family: sans-serif">Lundi 01 Janvier</h3>

<script type="text/javascript">
var date = new Date();
var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
var hourElem = document.querySelector("#hour");
var dayElem = document.querySelector("#day");

var timeRefresh = function(){
    date = new Date();
    hourElem.innerText = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    dayElem.innerText = date.toLocaleDateString('fr-FR', options );
};
timeRefresh();

var timeInterval = window.setInterval( function(){ timeRefresh(); }, 1000);
</script>
```

![image-20181017162543052](../../java/tp2/assets/image-20181017162543052.png)