### Arthur Eudeline - B3ca

# JavaScript TP3 :



## Pratique 1 :

![image-20181115155557923](assets/image-20181115155557923.png)

Pour obtenir ce résultat on créer le fichier `functions.php` qui contiendra les fonctions qui s'occupent de récupérer des données :

```php
<?php

// Config PDO
$config = array(
  'host'      => 'localhost',
  'database'  => 'cs2i_js_tp3',
  'creds'     => array(
    'login'   => 'root',
    'password'=> 'root'
  )
);

try{

  $PDO = new PDO(
    'mysql:host='. $config["host"] . ';dbname=' . $config["database"],
    $config["creds"]["login"],
    $config["creds"]["password"]
  );

  $PDO->setAttribute(
    PDO::ATTR_ERRMODE,
    PDO::ERRMODE_EXCEPTION
  );

} catch(Exception $e) {

  echo "<h1>Erreur :</h1>";
  echo "<pre>". $e->getMessage() ."</pre>";
  die();

}
// /Config PDO

/**
* Retourne la liste des auteurs
*
* @method get_auteurs
*
* @return Array      La liste des auteurs
*/
function get_auteurs(){
  global $PDO;

  $query = "SELECT * FROM auteur";
  return $PDO->query($query)->fetchAll();

}

function get_livres(){
  global $PDO;

  $values = array(
    "id_auteur" => $_REQUEST["id_auteur"]
  );

  $db_query  = $PDO->prepare("SELECT * FROM livre WHERE idAuteur = :id_auteur;");
  $db_query->execute($values);
  return $db_query->fetchAll(PDO::FETCH_OBJ);
}


?>
```

Ensuite, notre `index.php` vérifie si on demande ou non l'affichage des livres d'un auteur et affiche appelle ou non les livres de l'auteur :

```php
<?php
  require_once("functions.php");

  if ( isset($_REQUEST["id_auteur"]) ){
    $livres = get_livres( $_REQUEST["id_auteur"] );
  }

  $auteurs = get_auteurs();
  require_once( __DIR__. "/home.template.php" );
?>
```

Puis nous n'avons plus qu'à afficher les données contenues dans les variables `$livres` et `$auteurs` en se basant sur des boucles `foreach()` dans le template.



## Pratiques 2, 3, 4 :

### Résultat :

####Vue :

![image-20181115161231780](assets/image-20181115161231780.png)

#### Communication avec les fichiers XML

![image-20181115161326955](assets/image-20181115161326955.png)

### Code :

Au niveau de notre fichier `functions.php`, on vient remplacer la fonction `get_livres()` par la fonction `render_xml_livres_liste()` :

```php
<?php 
	// functions.php 
  // [...]

  function render_xml_livres_liste(){
    global $PDO;

    $values = array(
      "id_auteur" => $_REQUEST["id_auteur"]
    );

    $db_query  = $PDO->prepare("SELECT * FROM livre WHERE idAuteur = :id_auteur;");
    $db_query->execute($values);
    $livres = $db_query->fetchAll(PDO::FETCH_OBJ);

    header("Content-Type: application/xml");
    echo '<root>';
    foreach( $livres as $livre ) {
      echo '<livre>' . $livre->titre . '</livre>';
    }
    echo '</root>';
  }
  
?> 
```

Et lorsque nous effectuons une requête sur le fichier `index.php`, on appelle l'XML et on affiche pas le HTML :

```php
<?php
	// index.php
  require_once("functions.php");

  if ( isset($_REQUEST["id_auteur"]) ){
    render_xml_livres_liste();
  } else {
    $auteurs = get_auteurs();
    require_once( __DIR__. "/home.template.php" );
  }
?>
```

Ainsi, notre `script.js` execute la requête et injecte le résultat dans le HTML :

```js
function getXMLHttpRequest() {
  var request = 'nuselectContent';

  try {
    request = new XMLHttpRequest();
  } catch ( requestError ) {

    try {
      var request = new ActiveXObject("Microsoft.XMLHTTP");
    } catch ( error ) {

      try {
        var request = new ActiveXObject("Msxml2.XMLHTTP");
      } catch ( error ) {
        request = 'nuselectContent';
      }

    }

  }

  return request;
}

function changeAuteurs() {

  var select = document.getElementById("auteurs-select").selectedIndex;
  var idAuteur = document.getElementById("auteurs-select")[select].value;

  var httpRequest = getXMLHttpRequest();

  httpRequest.onreadystatechange = function(){
    // Quand la requête a fini de charger
    if (httpRequest.readyState == 4) {

      // Si la requête s'est correctement exécutée
      if (httpRequest.status == 200) {

        var xmlDoc = httpRequest.responseXML
        var livres = xmlDoc.getElementsByTagName("livre");

        var selectContent = "";
        var tableContent = "";

        for (var i =0; i< livres.length; i++) {

          selectContent += "<option>" + livres[i].childNodes[0].nodeValue + "</option>";
          tableContent+= "<tr><td>" + livres[i].childNodes[0].nodeValue + "</td></tr>";

        }

        document.getElementById("select-livres-auteur").innerHTML    = selectContent;
        document.getElementById("select-livres-auteur").disabled     = false;

        document.querySelector("#table-livres-auteur tbody").innerHTML  = tableContent;

      }

      // Si la requête a rencontré un problème
      else {
        console.error('Request error. Current request status : ' + httpRequest.status);
      }

    }
  }

  httpRequest.open('GET', 'index.php?id_auteur=' + idAuteur, true);
  httpRequest.send('nuselectContent');
}
```

Enfin, notre HTML ressemble à cela :

```php+HTML
<!-- home.template.html -->
<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>AJAX + XML</title>
  <meta name="description" content=""/>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <style>
    .padding {
      padding : 15px;
    }
  </style>
</head>

<body>

  <div class="container">
    <div class="row">
        <h1 class="col-xs-12">Liste des livres</h1>

        <div class="col-xs-12 col-sm-8 padding">
          <div class="row">

            <!-- Selection auteur -->
            <label class="control-label col-xs-12 padding">
              <p>Auteurs :</p>

              <select id="auteurs-select" class="form-control" onchange="changeAuteurs()" >
                <?php if( isset($auteurs) && is_array($auteurs) && !empty($auteurs) ){ ?>
                  <?php foreach($auteurs as $auteur) { ?>

                    <option value="<?php echo $auteur["id"]?>">
                      <?php echo $auteur["nom"];?>
                    </option>

                  <?php } ?>
                <?php } ?>
              </select>
            </label>
            <!-- /Selection auteur -->

            <!-- Affichage des livres -->
            <label class="control-label col-xs-12 padding">
              <p>Livres de l'auteur :</p>
              <select id="select-livres-auteur" class="form-control" disabled>
              </select>
            </label>
          </div>
        </div>

        <div class="col-xs-12 col-sm-4 padding">
          <div class="row">
            <div class="col-xs-12 padding">

              <p><b>Livres de l'auteur :</b></p>

              <table id="table-livres-auteur" class="table table-striped table-bordered col-xs-12">
                <thead>
                  <th>Nom du livre</th>
                </thead>

                <tbody></tbody>
              </table>

            </div>
          </div>
        </div>

    </div>
  </div>
  
  <script type="text/javascript" src="script.js"></script>
</body>
</html>
```

