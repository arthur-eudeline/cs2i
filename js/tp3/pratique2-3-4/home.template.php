<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>AJAX + XML</title>
  <meta name="description" content=""/>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <style>
    .padding {
      padding : 15px;
    }
  </style>
</head>

<body>

  <div class="container">
    <div class="row">
        <h1 class="col-xs-12">Liste des livres</h1>

        <div class="col-xs-12 col-sm-8 padding">
          <div class="row">

            <!-- Selection auteur -->
            <label class="control-label col-xs-12 padding">
              <p>Auteurs :</p>

              <select id="auteurs-select" class="form-control" onchange="changeAuteurs()" >
                <?php if( isset($auteurs) && is_array($auteurs) && !empty($auteurs) ){ ?>
                  <?php foreach($auteurs as $auteur) { ?>

                    <option value="<?php echo $auteur["id"]?>">
                      <?php echo $auteur["nom"];?>
                    </option>

                  <?php } ?>
                <?php } ?>
              </select>
            </label>
            <!-- /Selection auteur -->

            <!-- Affichage des livres -->
            <label class="control-label col-xs-12 padding">
              <p>Livres de l'auteur :</p>
              <select id="select-livres-auteur" class="form-control" disabled>
              </select>
            </label>
          </div>
        </div>

        <div class="col-xs-12 col-sm-4 padding">
          <div class="row">
            <div class="col-xs-12 padding">

              <p><b>Livres de l'auteur :</b></p>

              <table id="table-livres-auteur" class="table table-striped table-bordered col-xs-12">
                <thead>
                  <th>Nom du livre</th>
                </thead>

                <tbody></tbody>
              </table>

            </div>
          </div>
        </div>

    </div>
  </div>
  
  <script type="text/javascript" src="script.js"></script>
</body>
</html>
