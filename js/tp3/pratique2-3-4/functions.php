<?php

// Config PDO
$config = array(
  'host'      => 'localhost',
  'database'  => 'cs2i_js_tp3',
  'creds'     => array(
    'login'   => 'root',
    'password'=> 'root'
  )
);

try{

  $PDO = new PDO(
    'mysql:host='. $config["host"] . ';dbname=' . $config["database"],
    $config["creds"]["login"],
    $config["creds"]["password"]
  );

  $PDO->setAttribute(
    PDO::ATTR_ERRMODE,
    PDO::ERRMODE_EXCEPTION
  );

} catch(Exception $e) {

  echo "<h1>Erreur :</h1>";
  echo "<pre>". $e->getMessage() ."</pre>";
  die();

}
// /Config PDO

/**
* Retourne la liste des auteurs
*
* @method get_auteurs
*
* @return Array      La liste des auteurs
*/
function get_auteurs(){
  global $PDO;

  $query = "SELECT * FROM auteur";
  return $PDO->query($query)->fetchAll();

}

function render_xml_livres_liste(){
  global $PDO;

  $values = array(
    "id_auteur" => $_REQUEST["id_auteur"]
  );

  $db_query  = $PDO->prepare("SELECT * FROM livre WHERE idAuteur = :id_auteur;");
  $db_query->execute($values);
  $livres = $db_query->fetchAll(PDO::FETCH_OBJ);

  header("Content-Type: application/xml");
  echo '<root>';
  foreach( $livres as $livre ) {
    echo '<livre>' . $livre->titre . '</livre>';
  }
  echo '</root>';
}


?>
