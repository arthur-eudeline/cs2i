function getXMLHttpRequest() {
  var request = 'nuselectContent';

  try {
    request = new XMLHttpRequest();
  } catch ( requestError ) {

    try {
      var request = new ActiveXObject("Microsoft.XMLHTTP");
    } catch ( error ) {

      try {
        var request = new ActiveXObject("Msxml2.XMLHTTP");
      } catch ( error ) {
        request = 'nuselectContent';
      }

    }

  }

  return request;
}

function changeAuteurs() {

  var select = document.getElementById("auteurs-select").selectedIndex;
  var idAuteur = document.getElementById("auteurs-select")[select].value;

  var httpRequest = getXMLHttpRequest();

  httpRequest.onreadystatechange = function(){
    // Quand la requête a fini de charger
    if (httpRequest.readyState == 4) {

      // Si la requête s'est correctement exécutée
      if (httpRequest.status == 200) {

        var xmlDoc = httpRequest.responseXML
        var livres = xmlDoc.getElementsByTagName("livre");

        var selectContent = "";
        var tableContent = "";

        for (var i =0; i< livres.length; i++) {

          selectContent += "<option>" + livres[i].childNodes[0].nodeValue + "</option>";
          tableContent+= "<tr><td>" + livres[i].childNodes[0].nodeValue + "</td></tr>";

        }

        document.getElementById("select-livres-auteur").innerHTML    = selectContent;
        document.getElementById("select-livres-auteur").disabled     = false;

        document.querySelector("#table-livres-auteur tbody").innerHTML  = tableContent;

      }

      // Si la requête a rencontré un problème
      else {
        console.error('Request error. Current request status : ' + httpRequest.status);
      }

    }
  }

  httpRequest.open('GET', 'index.php?id_auteur=' + idAuteur, true);
  httpRequest.send('nuselectContent');
}
