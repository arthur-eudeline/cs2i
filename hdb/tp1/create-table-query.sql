-- Employé
CREATE TABLE employe
(
    id INT PRIMARY KEY NOT NULL,
    nom VARCHAR(100),
    prenom VARCHAR(100)
);

-- Client
CREATE TABLE client
(
	id INT PRIMARY KEY NOT NULL,
	nom VARCHAR(100),
	prenom VARCHAR(100)
);

-- Chantier
CREATE TABLE chantier
(
	id INT PRIMARY KEY NOT NULL,
	adresse VARCHAR(255),
	date_debut DATE,
	date_fin DATE,
	-- client_id INT NOT NULL FOREIGN KEY REFERENCES client(id)
	client_id INT NOT NULL,
	CONSTRAINT FK_Client FOREIGN KEY (client_id) REFERENCES client(id)
);

-- Outil
CREATE TABLE outil
(
	id INT PRIMARY KEY NOT NULL,
	description TINYTEXT,
	prix FLOAT,
	etat INT
);

-- [employé] Possède [un ou plusieurs outils]
CREATE TABLE possede
(
	id INT PRIMARY KEY NOT NULL,
	employe_id INT NOT NULL,
	outil_id INT NOT NULL,
	date_emprunt DATETIME NOT NULL,
	date_retour DATETIME NOT NULL
);

-- planning employé chantier
CREATE TABLE planning_employe_chantier
(
	id INT PRIMARY KEY NOT NULL,
	-- employe_id INT NOT NULL FOREIGN KEY REFERENCES employe(id),
	employe_id INT NOT NULL,
	CONSTRAINT FK_Employe FOREIGN KEY (employe_id) REFERENCES employe(id),
	-- chantier_id INT NOT NULL FOREIGN KEY REFERENCES chantier(id),
	chantier_id INT NOT NULL,
	CONSTRAINT FK_Chantier FOREIGN KEY (chantier_id) REFERENCES chantier(id),
	date_arrivee DATETIME,
	date_depart DATETIME
);
