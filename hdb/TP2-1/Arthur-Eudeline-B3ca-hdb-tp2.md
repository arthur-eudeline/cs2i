### Arthur Eudeline

# HDB : TP2



1. Sélection du numéro véhicule, de l’EVN, du numéro de rame commercial de tous les véhicules :

   ```sql
   SELECT * FROM `vehicule`
   ```


2. Sélection de tous les véhicules avec le nomtechnique complet.

   ```sql
   SELECT 
   `vehicule`.Num_Vehicule, 
   `vehicule`.Evn, 
   `vehicule`.NumRameCommercial, 
   `vehicule`.CodeSerieMateriel, 
   `serie_materiel`.NomTechniqueComplet 
   
   FROM `vehicule` INNER JOIN `serie_materiel` ON `vehicule`.CodeSerieMateriel = `serie_materiel`.CodeSerieMateriel
   ```


3. Afficher les compositions de plus de 1 véhicule (EvnEngin, Nombre de véhicule, numéro de

   rame commercial).

   ```sql
   SELECT 
   composition.EvnEngin, 
   COUNT(composition.EvnVehicule) AS nbres,
   vehicule.NumRameCommercial
   
   FROM composition
   INNER JOIN vehicule ON composition.EvnVehicule = vehicule.Evn
   
   GROUP BY composition.EvnEngin
   HAVING nbres > 1
   ```

4. Pour une série, afficher toutes les sous-séries.

5. ```sql
   SELECT 
   AbreviationSerie,
   GROUP_CONCAT(CodeSerieMateriel) AS sous_serie_materiel
   
   FROM serie_materiel
   
   WHERE sous_serie_materiel != ""
   
   GROUP BY AbreviationSerie
   ```



5. Pour une série / sous-série, affiche toutes les variantes.

   ```sql
   SELECT 
   `serie_materiel`.AbreviationSerie,
   `serie_materiel`.AbreviationSousSerie, 
   GROUP_CONCAT(CodeSerieMateriel SEPARATOR ',')
   
   FROM serie_materiel
   
   WHERE AbreviationSousSerie != '' AND AbreviationVariante != ''
   
   GROUP BY serie_materiel.AbreviationSerie
   ```



6. Liste des véhicules (EVN + Numéro de véhicule) qui ont des tags véhicules.

   ```sql
   SELECT 
   `vehicule`.Evn, 
   `vehicule`.Num_Vehicule, 
   GROUP_CONCAT( `tagvehicule`.UidTag )
   
   FROM `vehicule`
   INNER JOIN `tagvehicule` ON `vehicule`.Evn = `tagvehicule`.Evn
   
   GROUP BY `vehicule`.Evn
   ```

7. Liste des vehicules (Evn + Numéro de véhicule) qui ont des tags réservoirs

   ```sql
   SELECT 
   `vehicule`.Evn, 
   `vehicule`.Num_Vehicule,
   GROUP_CONCAT( `tagreservoir`.UidTag )
   
   FROM `vehicule`
   INNER JOIN `tagreservoir` ON `vehicule`.Evn = `tagreservoir`.Evn
   
   GROUP BY `vehicule`.Evn
   ```



8. Liste des véhicules (EVN + numéro de véhicule) qui ont au moins un tag véhicule et un tag
   réservoir.

   ```sql
   SELECT 
   `vehicule`.Evn, 
   `vehicule`.Num_Vehicule, 
   GROUP_CONCAT( `tagvehicule`.UidTag SEPARATOR ',' ), 
   GROUP_CONCAT( `tagreservoir`.UidTag SEPARATOR ',' )
   
   FROM `vehicule`
   INNER JOIN `tagreservoir` ON `vehicul`.Evn = `tagreservoir`.Evn
   INNER JOIN `tagvehicule` ON `vehicule`.Evn = `tagvehicule`.Evn
   
   GROUP BY `vehicule`.Evn
   ```

9. Liste des véhicules (EVN + numéro de véhicule) qui ont au moins un tag véhicule et/ou au
   moins un tag réservoir.

   ```sql
   SELECT 
   `vehicule`.Evn,
   `vehicule`.Num_Vehicule,
   GROUP_CONCAT( `tagvehicule`.UidTag SEPARATOR ','),
   GROUP_CONCAT( `tagreservoir`.UidTag SEPARATOR ',')
   
   FROM `vehicule`
   INNER JOIN `tagreservoir` ON `vehicule`.Evn = `tagreservoir`.Evn
   LEFT JOIN `tagvehicule` ON `vehicule`.Evn = `tagvehicule`.Evn
   
   GROUP BY `vehicule`.Evn
   ```

10. Afficher la composition complète trier par position pour le véhicule dont l’EVN est : 938702473571

    ```sql
    SELECT 
    `composition`.EvnVehicule,
    `composition`.Position
    
    FROM `composition`
    
    WHERE `EvnEngin` IN ( SELECT EvnEngin FROM `composition` WHERE EvnVehicule = 938702473571)
    
    ORDER BY Position
    ```



11. Afficher le nombre de véhicule par série.

    ```sql
    SELECT 
    `serie_materiel`.CodeSerieMateriel, 
    `AbreviationSerie`,
    COUNT( `vehicule`.Evn )
    
    FROM `serie_materiel`
    INNER JOIN `vehicule` ON `serie_materiel`.CodeSerieMateriel = `vehicule`.CodeSerieMateriel
    
    GROUP BY `CodeSerieMateriel`
    ```

12. A partir de votre requête précédente, rajouter le nombre de véhicule ayant un tag véhicule par abréviation série.

    ```sql
    SELECT 
    `serie_materiel`.CodeSerieMateriel,
    `AbreviationSerie`,
    COUNT( `vehicule`.Evn ),
    COUNT( `tagvehicule`.Evn) AS nbres
    
    FROM `serie_materie`
    INNER JOIN `vehicule` ON `serie_materiel`.CodeSerieMateriel = `vehicule`.CodeSerieMateriel
    LEFT JOIN `tagvehicule` ON `vehicule`.Evn = `tagvehicule`.Evn
    
    GROUP BY `serie_materiel`.AbreviationSerie
    ORDER BY `nbres`  DES`
    ```



13. A partir de votre requête précédente, veuillez afficher le pourcentage de pose par série de tag véhicule arrondi à 0.01 près.

    ```sql
    SELECT 
    `serie_materiel`.CodeSerieMateriel, 
    `AbreviationSerie`, 
    ROUND(( COUNT(`tagvehicule`.Evn) / COUNT(`vehicule`.Evn)) * 100, 2) as pourcentage
    
    FROM `serie_materiel`
    INNER JOIN `vehicule` ON `serie_materiel`.CodeSerieMateriel = `vehicule`.CodeSerieMateriel
    LEFT JOIN `tagvehicule` ON `vehicule`.Evn = `tagvehicule`.Evn
    
    GROUP BY `serie_materiel`.AbreviationSerie
    
    ORDER BY `pourcentage`  DESC
    ```



14. En repartant de votre requête précédente, enlever les séries dont le pourcentage de pose est = 0 %.

    ```sql
    SELECT 
    `serie_materiel`.CodeSerieMateriel, 
    `AbreviationSerie`, 
    ROUND(( COUNT(`tagvehicule`.Evn) / COUNT(`vehicule`.Evn)) * 100, 2) as pourcentage
    
    FROM `serie_materiel`
    INNER JOIN `vehicule` ON `serie_materiel`.CodeSerieMateriel = `vehicule`.CodeSerieMateriel
    LEFT JOIN `tagvehicule` ON `vehicule`.Evn = `tagvehicule`.Evn
    
    GROUP BY `serie_materiel`.AbreviationSerie
    
    HAVING pourcentage != 0
    ORDER BY pourcentage  DESC
    ```



15. En repartant de la requête 14, rajouter la même chose pour les tags réservoirs

    ```sql
    
    ```
