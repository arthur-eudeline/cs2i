### Arthur Eudeline - B3ca

# HDB - TP2 : Requêtes SQL



### 1 : Nom et prénom de tous les clients

```sql
Select nomClient, prenomClient 
from client;
```



### 2 : Tous les clients triés par ordre alphabétique sur le champ nom

```sql
Select * 
from client 
order by nomClient ASC;
```



### 3 : Tous les clients dont le nom commence par “D”

```sql
Select * 
from client 
where nomClient like 'D%';
```



###4 : Tous les clients qui ont commandés des chantiers

```sql
SELECT client.nomClient, client.prenomClient 
FROM chantier INNER JOIN client ON client.idClient = chantier.idClients 
group by client.idClient;
```



### 5 : Tous les clients qui ont des chantiers non terminés

```sql
SELECT client.nomClient, client.prenomClient 
FROM chantier INNER JOIN client ON client.idClient = chantier.idClients 
where chantier.DateFinChantier IS NULL group by client.idClient ;
```



### 6 : Le clilent dont le chantier n'est pas terminé et qui a commencé le plus tôt

```sql
Select client.nomClient, client.prenomClient 
from client, chantier 
where client.idClient = chantier.idClients and chantier.`DateDebChantier` 
in (Select min(DateDebChantier) from chantier);
```



### 7 : Le nombre de chantier non terminé par ville 

```sql
Select chantier.`villeChantier`, count(chantier.`idChantier`) as 'nombreChantier' 
from chantier 
where chantier.`DateFinChantier` is NULL
group by chantier.`villeChantier`;
```



### 8 : Liste de tous les employés (nom, prénom) triée par nom dans l'ordre alphabétique

```sql
Select employe.`nomEmploye`, employe.`prenomEmploye` 
from employe 
order by employe.`nomEmploye` ASC
```



### 9 : Tous les employés qui ont des outils non rendus avec leur nombre 

```sql
Select employe.`nomEmploye`, employe.`prenomEmploye`, count(employeoutil.`idOutil`) as 'NombreOutilsNonRendus'
from employe, employeoutil 

where 
employe.`idEmploye` = employeoutil.`idEmploye`
and
employeoutil.`dateFinEmpruntOutil` is NULL 

group by 
employeoutil.`idEmploye`

order by 
employe.`nomEmploye` ASC
```



### 10 : Liste de tous les outils disponibles

Il nous serait possible de faire cette requête dans la mesure où l'`etat` avec l'id 1 correspond à un état **disponible**

```sql
select 
*

from
outil

where
outil.`idEtat` = 1
```

Cependant, l'état n'étant pas changé on doit croiser les résultats avec ceux de la table `employeoutil` et de la colonne `dateFinEmpruntOutil` :

```sql
select 
outil.descriptionOutil

from
outil, employeoutil

where
outil.idOutil = employeoutil.`idOutil`
and
employeoutil.`dateFinEmpruntOutil` is NULL
```



### 11 : Somme du prix des outils par employés 

```sql
Select 
employe.`nomEmploye`,
employe.`prenomEmploye`,
count(employeoutil.`idOutil`) as 'NombreOutilsNonRendus',
sum(outil.`prixOutil`)

from 
employe, employeoutil, outil 

where 
employe.`idEmploye` = employeoutil.`idEmploye`
and
employeoutil.`dateFinEmpruntOutil` is NULL 

group by 
employe.`idEmploye`, employeoutil.`idOutil`

order by 
employe.`nomEmploye` ASC
```



### 12 : Liste des employés qui n'ont jamais été sur un chantier

```sql
Select
employe.`nomEmploye`,
employe.`prenomEmploye`

from
employe

where
employe.`idEmploye` not in (select employechantier.`idEmploye` from employechantier);
```



### 13 : Nom des clients dont les chantiers se sont terminés entre le 01/01/2017 et le 31/12/2017

```sql
Select
client.`nomClient`

from
client

where
client.`idclient` 
in 
(
    select 
    chantier.idClients 
    
    from 
    chantier 
    
    where
    chantier.`DateDebChantier` >= '01/01/2017' 
    and 
    chantier.`DateDebChantier` <='31/12/2017'
);
```



### 14 : Moyenne de chantier par employés ayant effectué un chantier

```sql
select
avg(nombreChantier) as "moyenneChantierEffectueParEmploye"

from
(
    select 
    count(*) as 'nombreChantier'

    from 
    employechantier, employe

    where
    employe.`idEmploye` = employechantier.`idEmploye`

    group by
    employe.`idEmploye`
) as `table`;
```



### 15 : Cumul d'heure passé par les employés par chantier

```sql
select
employechantier.`idChantier`,
sum(TempsPasse.tempsPasse) as "NombreHeureParChantier"

from
employechantier,
(
    Select
    employechantier.`idChantier`,
    TIMESTAMPDIFF(HOUR,employechantier.`dateHeurDeb`, employechantier.`dateHeureFin`) as "tempsPasse"


    from
    employechantier
) as `TempsPasse`


group by
employechantier.`idChantier`
```

