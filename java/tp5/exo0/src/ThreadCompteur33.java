class ThreadCompteur33 extends Thread {
  int no_fin;

  // Constructeur
  ThreadCompteur33(int fin) {
    no_fin = fin;
  }

  // On redéfinit la méthode run()
  public void run () {
    for (int i=1; i<=no_fin ; i++) {
      System.out.println(this.getName()+":"+i);
    }
  }

  public static void main (String args[]) {
    // On instancie les threads
    ThreadCompteur33 cp1 = new ThreadCompteur33 (100);
    ThreadCompteur33 cp2 = new ThreadCompteur33 (50);

    cp1.start();
    cp2.start();
    try {
      cp1.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    try {
      cp2.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}