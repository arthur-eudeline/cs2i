class Impression {
  public void imprime(String t) {
    for (int i = 1; i < 30; i++) {
      for (int j = 0; j < t.length(); j++) {
        System.out.print(t.charAt(j));
      }
    }
  }
}

class TPrint extends Thread {
  static Impression mImp = new Impression();
  String txt;

  public TPrint(String t) {
    txt = t;
  }

  public void run() {
    for (int j = 0; j < 3; j++) {
      mImp.imprime(txt);
    }
  }

  static public void main(String args[]) throws InterruptedException {
    TPrint a = new TPrint("bonjour ");
    TPrint b = new TPrint("au revoir ");
    a.start();
    b.start();
    a.join();
    b.join();
  }
}