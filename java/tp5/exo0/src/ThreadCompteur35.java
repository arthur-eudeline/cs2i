class ThreadCompteur35 extends Thread {
  int no_fin;
  int attente;

  ThreadCompteur35(int fin, int att) {
    no_fin = fin;
    attente = att;
  }

  // On redéfinit la méthode run()
  public void run() {
    for (int i = 1; i <= no_fin; i++) {
      System.out.println(this.getName() + ":" + i);

      try {
        sleep(attente);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public static void main(String args[]) {
    // On instancie les threads
    ThreadCompteur35 cp1 = new ThreadCompteur35(100, 100);
    ThreadCompteur35 cp2 = new ThreadCompteur35(50, 200);
    cp1.start();
    cp2.start();
    try {
      cp1.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    try {
      cp2.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}