class ImpressionRunnable {
  synchronized public void imprime(String t) {
    for (int i = 1; i < 30; i++) {
      for (int j = 0; j < t.length(); j++) {
        System.out.print(t.charAt(j));
      }
    }
  }
}

class TPrintRunnable implements Runnable {
  static ImpressionRunnable mImp = new ImpressionRunnable();
  String txt;

  public TPrintRunnable(String t) {
    txt = t;
  }

  public void run() {
    for (int j = 0; j < 3; j++) {
      mImp.imprime(txt);
    }
  }

  static public void main(String args[]) {
    TPrintRunnable a = new TPrintRunnable("bonjour ");
    Thread aThread = new Thread(a);

    TPrintRunnable b = new TPrintRunnable("au revoir ");
    Thread bThread  = new Thread(b);

    aThread.start();
    bThread.start();

    try {
      aThread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    try {
      bThread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}