public class ThreadCompteur extends Thread {

  private String name;
  private int fin;

  // Constructeur
  ThreadCompteur(String name, int fin) {
    this.name = name;
    this.fin = fin;
  }

  public void run() {
    long attente = (long) Math.random() * 1000;
    try {
      for (int i = 0; i < this.fin; i++) {
        sleep(attente);
        System.out.println(this.name + " \t: " + i);
      }

      System.out.println("FIN : " + this.name);

    } catch (InterruptedException e) {
      System.out.println("ERREUR : " + e.getMessage());
    }
  }

  public static void main(String [] args){

    ThreadCompteur quentin = new ThreadCompteur("Quentin", 10);
    ThreadCompteur corentin = new ThreadCompteur("Corentin", 10);

    System.out.println("\nThreads : début");

    quentin.start();
    corentin.start();


    try {
      quentin.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    try {
      corentin.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    System.out.println("\nThreads : fin");

  }
}
