public class ThreadCompteurRunnable implements Runnable {

  private String name;
  private int fin;

  // Constructeur
  ThreadCompteurRunnable(String name, int fin) {
    this.name = name;
    this.fin = fin;
  }

  public void run() {
    long attente = (long) Math.random() * 1000;
    try {
      for (int i = 0; i < this.fin; i++) {
        Thread.sleep(attente);
        System.out.println(this.name + " \t: " + i);
      }

      System.out.println("FIN : " + this.name);

    } catch (InterruptedException e) {
      System.out.println("ERREUR : " + e.getMessage());
    }
  }

  public static void main(String[] args) {

    ThreadCompteurRunnable quentin = new ThreadCompteurRunnable("Quentin Runnable", 10);
    Thread quentinThread = new Thread(quentin);

    ThreadCompteurRunnable corentin = new ThreadCompteurRunnable("Corentin Runnable", 10);
    Thread corentinThread = new Thread(corentin);

    System.out.println("\nThreads : début");

    quentinThread.start();
    corentinThread.start();


    try {
      quentinThread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    try {
      corentinThread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    System.out.println("\nThreads : fin");

  }
}
