### Arthur Eudeline - B3ca

# Java TP5 



## Exercice 0

###Page 33 :

```java
class ThreadCompteur33 extends Thread {
  int no_fin;

  // Constructeur
  ThreadCompteur33(int fin) {
    no_fin = fin;
  }

  // On redéfinit la méthode run()
  public void run () {
    for (int i=1; i<=no_fin ; i++) {
      System.out.println(this.getName()+":"+i);
    }
  }

  public static void main (String args[]) {
    // On instancie les threads
    ThreadCompteur33 cp1 = new ThreadCompteur33 (100);
    ThreadCompteur33 cp2 = new ThreadCompteur33 (50);

    cp1.start();
    cp2.start();
    try {
      cp1.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    try {
      cp2.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
```

Ce qui affiche :

```
Thread-0:1
Thread-0:2
Thread-1:1

[...]

Thread-0:97
Thread-0:98
Thread-0:99
Thread-0:100

Process finished with exit code 0
```



### Page 35 :

```java
class ThreadCompteur35 extends Thread {
  int no_fin;
  int attente;

  ThreadCompteur35(int fin, int att) {
    no_fin = fin;
    attente = att;
  }

  // On redéfinit la méthode run()
  public void run() {
    for (int i = 1; i <= no_fin; i++) {
      System.out.println(this.getName() + ":" + i);

      try {
        // On stope le thread
        sleep(attente);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public static void main(String args[]) {
    // On instancie les threads
    ThreadCompteur35 cp1 = new ThreadCompteur35(100, 100);
    ThreadCompteur35 cp2 = new ThreadCompteur35(50, 200);
    cp1.start();
    cp2.start();
    try {
      cp1.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    try {
      cp2.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
```

On obtient le même résultat qu'au dessus, à l'exception près que les messages n'apparaissent pas instantannément, il y a un délais d'affichage entre chaque message conditionné par la valeur passée à la méthode `sleep()`.



### Page 42 :

#### Par héritage

```java
class Impression {
  synchronized public void imprime(String t) {
    for (int i=1; i<30; i++) {
      for (int j=0; j<t.length(); j++) {
        System.out.print(t.charAt(j));
      }
    }
  }
}
class TPrint extends Thread {
  static Impression mImp = new Impression();
  String txt;
  public TPrint(String t) {txt = t;}
  public void run() {
    for (int j=0; j<3; j++) { mImp.imprime(txt); }
  }
  static public void main(String args[]) throws InterruptedException {
    TPrint a = new TPrint("bonjour ");
    TPrint b = new TPrint("au revoir ");
    a.start();
    b.start();
    a.join();
    b.join();
  }
}
```

Ce qui affiche :

![image-20181213084823722](assets/image-20181213084823722-4687303.png)

#### Par implémentation de l'interface `Runnable`

```java
class ImpressionRunnable {
  synchronized public void imprime(String t) {
    for (int i = 1; i < 30; i++) {
      for (int j = 0; j < t.length(); j++) {
        System.out.print(t.charAt(j));
      }
    }
  }
}

class TPrintRunnable implements Runnable {
  static ImpressionRunnable mImp = new ImpressionRunnable();
  String txt;

  public TPrintRunnable(String t) {
    txt = t;
  }

  public void run() {
    for (int j = 0; j < 3; j++) {
      mImp.imprime(txt);
    }
  }

  static public void main(String args[]) {
    TPrintRunnable a = new TPrintRunnable("bonjour ");
    Thread aThread = new Thread(a);

    TPrintRunnable b = new TPrintRunnable("au revoir ");
    Thread bThread  = new Thread(b);

    aThread.start();
    bThread.start();

    try {
      aThread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    try {
      bThread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
```

#### Exécution du code sans l'utilisation de `synchronised` sur `Impression.imprime()`

Étant donné que `Impression.imprime()` est une ressource critique, on remarque que sans l'utilisation du mot clé `synchronized`, on assiste à une utilisation simultanée de la ressource par les deux Threads, ce qui cause ici une altération des calculs, et donc une modification du comportement attendu par notre programme.

![image-20181229122349720](assets/image-20181229122349720-6082629.png) 

## Exercice 1 : Des threads indépendants

### 1.1 : création de `ThreadCompteur` héritant de la classe native `Thread`

```java
public class ThreadCompteur extends Thread {

  private String name;
  private int fin;

  // Constructeur
  ThreadCompteur(String name, int fin) {
    this.name = name;
    this.fin = fin;
  }

  public void run() {
    long attente = (long) Math.random() * 1000;
    try {
      for (int i = 0; i < this.fin; i++) {
        sleep(attente);
        System.out.println(this.name + " \t: " + i);
      }

      System.out.println("FIN : " + this.name);

    } catch (InterruptedException e) {
      System.out.println("ERREUR : " + e.getMessage());
    }
  }

  public static void main(String [] args){

    ThreadCompteur quentin = new ThreadCompteur("Quentin", 10);
    ThreadCompteur corentin = new ThreadCompteur("Corentin", 10);

    System.out.println("\nThreads : début");

    quentin.start();
    corentin.start();


    try {
      quentin.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    try {
      corentin.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    System.out.println("\nThreads : fin");

  }
}
```

Voici le résultat lors de l'exécution :

![image-20181229122950386](assets/image-20181229122950386-6082990.png)



### 1.2 : Création de `ThreadCompteur` par l'implémentation de l'interface `Runnable`



Nous avons juste à changer l'instanciation des `Thread` et, étant donné que notre classe n'hérite pas de `Thread`, nous devons utiliser `Thread.sleep()` au lieu de `sleep()` :

```java
public class ThreadCompteurRunnable implements Runnable {

  private String name;
  private int fin;

  // Constructeur
  ThreadCompteurRunnable(String name, int fin) {
    this.name = name;
    this.fin = fin;
  }

  public void run() {
    long attente = (long) Math.random() * 1000;
    try {
      for (int i = 0; i < this.fin; i++) {
        Thread.sleep(attente);
        System.out.println(this.name + " \t: " + i);
      }

      System.out.println("FIN : " + this.name);

    } catch (InterruptedException e) {
      System.out.println("ERREUR : " + e.getMessage());
    }
  }

  public static void main(String[] args) {

    ThreadCompteurRunnable quentin = new ThreadCompteurRunnable("Quentin Runnable", 10);
    Thread quentinThread = new Thread(quentin);

    ThreadCompteurRunnable corentin = new ThreadCompteurRunnable("Corentin Runnable", 10);
    Thread corentinThread = new Thread(corentin);

    System.out.println("\nThreads : début");

    quentinThread.start();
    corentinThread.start();


    try {
      quentinThread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    try {
      corentinThread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    System.out.println("\nThreads : fin");

  }
}
```

Ce qui affiche :

![image-20181229123720670](assets/image-20181229123720670-6083440.png)



## Exercice 2 : Thread faiblement liés

Pour obtenir la position, je rajoute un attribut et une méthode statique à ma classe :

```java
public class SynCompteur extends Thread {

  private String name;
  private int fin;
  // la position du Thread
  public int position;

  // Le compteur de position des Threads
  static int compteurFinThread = 0;
  synchronized static int enregistrerPosition(){
    compteurFinThread++;
    return compteurFinThread;
  }
  
	...
    
  public void run() {
    long attente = (long) Math.random() * 1000;
    try {
      for (int i = 1; i <= this.fin; i++) {
        sleep(attente);
        System.out.println(this.name + " \t: " + i);
      }

      // Enregistrement de la position à laquelle fini le Thread
      this.position = enregistrerPosition();

    } catch (InterruptedException e) {
      System.out.println("ERREUR : " + e.getMessage());
    }
  }
  
  public static void main(String [] args){

    SynCompteur quentin = new SynCompteur("Quentin", 10);
    SynCompteur corentin = new SynCompteur("Corentin", 10);

    System.out.println("\nThreads : début");

    quentin.start();
    corentin.start();


    try {
      quentin.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    try {
      corentin.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    System.out.println("\nThreads : fin");

    System.out.println("\n*** Classement ***");
    System.out.println("Quentin a terminé à la position : " + quentin.position);
    System.out.println("Corentin a terminé à la position : " + corentin.position);

  }
}
```

Ainsi, nous pouvons savoir quel Thread a terminé en premier :

![image-20181229132836358](assets/image-20181229132836358-6086516.png)



## Exercice 3 : Problème d'accès concurrents 

### 3.1 : Création des classes :

#### Compte :

```java
public class Compte {

  private int solde = 0;

  public Compte() {
  }

  public void operationNulle (int somme) {
    this.ajouter(somme);
    this.retirer(somme);
  }

  public int getSolde() {
    return solde;
  }

  public void ajouter(int somme){
    this.solde += somme;
  }

  public void retirer(int somme){
    this.solde -= somme;
  }

  public void setSolde(int solde) {
    this.solde = solde;
  }
}
```

#### Opération :

```java
public class Operation extends Thread {

  private Compte compte;
  private String id;

  public Operation(String id, Compte compte){
    this.compte = compte;
    this.id = id;
  }

  public void run(){
    for (int i = 0; i < 1000; i++){
      int somme = (int) Math.round( Math.random() * 1000 );
      compte.operationNulle(somme);
    }
  }

}
```

#### Main :

```java
public class Main {

  static public void main(String args[]){

    Compte compte = new Compte();

    Operation op1 = new Operation("Opération 1", compte);
    Operation op2 = new Operation("Opération 2", compte);
    Operation op3 = new Operation("Opération 3", compte);

    op1.start();
    op2.start();
    op3.start();

    try {
      op1.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    try {
      op2.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    try {
      op3.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    System.out.println("Solde final du compte : " + compte.getSolde() );

  }

}
```

### 3.2 : Exécution du programme

Si on lance le programme en tant que tel, on obtient un solde final différent de celui de départ, ce qui n'est pas sensé arriver. Cela est dû au fait que nous n'avons pas protégé notre ressource critique, le `Compte` avec le mot clé `synchronized`. Ainsi, tous nos Threads accède en même temps au compte et y éffectue des opérations qui se basent sur des informations érronées :

![image-20181229135418463](assets/image-20181229135418463-6088058.png)



### 3.2.1 : Utilisation du mot clé `synchronized` sur une méthode 

Étant donné que dans notre exemple précis, les Threads ne manipulent que la méthode `Compte.operationNulle()`, nous pouvons ne protéger que cette méthode :

```java
public class Compte {

  private int solde = 0;

  public Compte() {
  }

  synchronized void operationNulle (int somme) {
    this.ajouter(somme);
    this.retirer(somme);
  }
  
  ...
}
```

![image-20181229135654284](assets/image-20181229135654284-6088214.png)



### 3.2.2 : Utilisation du mot clé `synchronized` sur un objet :

En réalité, notre ressource critique n'est pas la méthode `Compte.operationNulle()`, c'est **toute** l'instance `Compte compte` que nous manipulons dans nos Threads. Il est donc plus simple et surtout plus sûr de la protéger dans sa globalité :

```java
public class Operation extends Thread {

  private Compte compte;
  private String id;

  public Operation(String id, Compte compte){
    this.compte = compte;
    this.id = id;
  }

  public void run(){
    for (int i = 0; i < 1000; i++){
      int somme = (int) Math.round( Math.random() * 1000 );
      
      // Utilisation de synchronized sur l'objet compte
      synchronized (compte) {
        compte.operationNulle(somme);
      }
    }
    
  }

}
```



### 3.3 : Remplacement des actions du Thread

Étant donné que l'instance `Compte compte` constitue toujours notre ressource critique, il faut maintenir l'ensemble des opérations effectuées sur cette instance dans un bloc `synchronized`. En prenant soin de laisser les opérations sur `Compte compte` dans notre bloc, nous évitons ainsi tout problème d'accès concurrent :

```java
public class Operation extends Thread {

  private Compte compte;
  private String id;

  public Operation(String id, Compte compte){
    this.compte = compte;
    this.id = id;
  }

  public void run(){
    for (int i = 0; i < 1000; i++){
      int somme = (int) Math.round( Math.random() * 1000 );
      synchronized (compte) {
        // compte.operationNulle(somme);
        compte.ajouter(somme);
        compte.retirer(somme);
      }
    }
  }

}
```

![image-20181229140607315](assets/image-20181229140607315-6088767.png)