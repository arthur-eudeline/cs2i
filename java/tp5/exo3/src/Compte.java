public class Compte {

  private int solde = 0;

  public Compte() {
  }

  void operationNulle (int somme) {
    this.ajouter(somme);
    this.retirer(somme);
  }

  public int getSolde() {
    return solde;
  }

  public void ajouter(int somme){
    this.solde += somme;
  }

  public void retirer(int somme){
    this.solde -= somme;
  }

  public void setSolde(int solde) {
    this.solde = solde;
  }
}
