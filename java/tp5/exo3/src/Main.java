public class Main {

  static public void main(String args[]){

    Compte compte = new Compte();

    Operation op1 = new Operation("Opération 1", compte);
    Operation op2 = new Operation("Opération 2", compte);
    Operation op3 = new Operation("Opération 3", compte);

    op1.start();
    op2.start();
    op3.start();

    try {
      op1.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    try {
      op2.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    try {
      op3.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    System.out.println("Solde final du compte : " + compte.getSolde() );

  }

}
