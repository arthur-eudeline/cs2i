public class Operation extends Thread {

  private Compte compte;
  private String id;

  public Operation(String id, Compte compte){
    this.compte = compte;
    this.id = id;
  }

  public void run(){
    for (int i = 0; i < 1000; i++){
      int somme = (int) Math.round( Math.random() * 1000 );
      synchronized (compte) {
        // compte.operationNulle(somme);
        compte.ajouter(somme);
        compte.retirer(somme);
      }
    }
  }

}
