public class SynCompteur extends Thread {

  private String name;
  private int fin;
  public int position;

  static int compteurFinThread = 0;
  synchronized static int enregistrerPosition(){
    compteurFinThread++;
    return compteurFinThread;
  }


  // Constructeur
  SynCompteur(String name, int fin) {
    this.name = name;
    this.fin = fin;
  }

  public void run() {
    long attente = (long) Math.random() * 1000;
    try {
      for (int i = 1; i <= this.fin; i++) {
        sleep(attente);
        System.out.println(this.name + " \t: " + i);
      }

      this.position = enregistrerPosition();

    } catch (InterruptedException e) {
      System.out.println("ERREUR : " + e.getMessage());
    }
  }

  public static void main(String [] args){

    SynCompteur quentin = new SynCompteur("Quentin", 10);
    SynCompteur corentin = new SynCompteur("Corentin", 10);

    System.out.println("\nThreads : début");

    quentin.start();
    corentin.start();


    try {
      quentin.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    try {
      corentin.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    System.out.println("\nThreads : fin");

    System.out.println("\n*** Classement ***");
    System.out.println("Quentin a terminé à la position : " + quentin.position);
    System.out.println("Corentin a terminé à la position : " + corentin.position);

  }
}