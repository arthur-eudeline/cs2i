import java.util.LinkedList;

public class Main {
    public static void main(String[] args)
    {
        // Création de la liste
        LinkedList<Float> li = new LinkedList<Float>();

        // Ajout de la valeur 3.2 à la position 0
        li.add(0, 3.2f);
        li.add(1, 4.2f);
        li.add(2, 3.6f);
        li.add(3, 7.2f);

        // Affichage de la taille de la liste
        System.out.println("La taille de la liste est de : " + li.size() );

        // Affichage de tous les éléments
        for (int i = 0; i < li.size(); i++){
            System.out.println( "li["+ i +"] : "+ li.get(i) );
        }

        // Suppression du dernier élément de la liste
        li.remove( (li.size() - 1) );

        System.out.println( "Nouvelle taille de la liste après suppression du dernier élément : "+ li.size() );

    }
}
