public class ElementPile {

    private int elem;

    public ElementPile(){
        this.elem = 0;
    }

    public ElementPile(int el){
        this.elem = el;
    }

    public int getElem(){
        return this.elem;
    }

    public void setElem(int elem){
        this.elem = elem;
    }

}