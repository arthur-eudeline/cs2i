public class Main {
    public static void main(String[] args){

        Pile pile = new Pile();
        System.out.println( "[Avant ajout] La pile est vide : " + pile.estVide() );
        pile.empile(1);
        pile.empile(42);
        pile.empile(33);
        System.out.println( "[Après ajout] La pile est vide : " + pile.estVide() );

        System.out.println( "Taille de la pile avant dépile : " + pile.getSize() );
        System.out.println( "Dépile de la valeur : " + pile.depile() );
        System.out.println( "Taille de la pile après dépile : " + pile.getSize() );

        System.out.println( "Sommet de la pile : " + pile.sommet() );

        pile.affiche();

    }
}
