import java.util.LinkedList;

public class Pile {

    private LinkedList<Integer> content;
    private int max;

    // On initialise une pile vide
    public Pile (){
        this.content = new LinkedList<Integer>();
    }

    // Si la pile a au moins un élément, elle n'est pas vide
    public boolean estVide (){
        if( this.content.size() > 0){
            return false;
        } else {
            return true;
        }
    }

    // Retourne le sommet de la pile
    public int sommet (){
        return this.content.get( (this.content.size() -1) );
    }

    // Ajoute un élément au sommet de la pile
    public void empile (int value){
        this.content.add(value);
    }

    // Retire le dernier element de la pile et le return
    public int depile (){
        int output = this.sommet();
        this.content.remove( (this.content.size() -1) );
        return output;
    }

    // Affiche le contenu d'une pile
    public void affiche(){
        System.out.println("*** Affichage du contenu de la pile ***");

        for(int i = (this.content.size() - 1); i >= 0 ; i--){
            System.out.println( "pile.content["+ i +"] : "+ this.content.get(i) );
        }

        System.out.println("*** Fin du contenu de la pile ***");
    }

    public int getSize(){
        return this.content.size();
    }

    // Fusionne deux pile tout en conservant les piles d'origine
    public static Pile union(Pile A, Pile B ){
        Pile C = new Pile();
        Pile BackUp = new Pile();

        // Dépile de A
        while( !A.estVide() ){
            int empileValue = A.depile();

            BackUp.empile( empileValue );
            C.empile( empileValue );
        }

        // Restitution du BackUp de A
        while( !BackUp.estVide() ){
            A.empile( BackUp.depile() );
        }

        // Dépile de B
        while( !B.estVide() ){
            int empileValue = B.depile();

            BackUp.empile( empileValue );
            C.empile( empileValue );
        }

        // Restitution du BackUp de B
        while( !BackUp.estVide() ){
            B.empile( BackUp.depile() );
        }

        return C;
    }

    public static Pile tri(Pile p){
        Pile res = new Pile();
        Pile aux = new Pile();

        while( !p.estVide() ) {
            if (res.estVide() || (p.sommet() < res.sommet())) {
                res.empile( p.depile() );

                if( !aux.estVide() ){
                    while ( !aux.estVide() ){
                        res.empile( aux.depile() );
                    }
                }
            }
            else {
                aux.empile( res.depile() );
            }
        }

        return res;
    }

}