import java.util.Scanner;

public class TestPile {

    public static void main(String[] args){

        // {4, 3, 2, 5, 8, 2, 6, 9, 3}.
        Pile A = new Pile();

        Scanner entree = new Scanner(System.in);
        System.out.print("Saisissez les nombres à ajouter à la pile A :\n");
        while(true){
            int val = entree.nextInt();
            if(val != -1)
            {
                entree.nextLine();
                A.empile(val);
            }
            else{
                break;
            }
        }
        entree.close();
        System.out.println("Enregistrement terminé");

        System.out.println("\nAffichage avant tri : ");
        A.affiche();

        System.out.println("\nAffichage après tri : ");
        A = Pile.tri(A);
        A.affiche();

    }

}
