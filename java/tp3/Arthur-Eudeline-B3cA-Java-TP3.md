### Arthur Eudeline

# Java TP3



## Exercice 1 : Utilisation de la classe prédéfinie LinkedList

```java
import java.util.LinkedList;

public class Main {
    public static void main(String[] args)
    {
        // Création de la liste
        LinkedList<Float> li = new LinkedList<Float>();

        // Ajout de la valeur 3.2 à la position 0
        li.add(0, 3.2f);
        li.add(1, 4.2f);
        li.add(2, 3.6f);
        li.add(3, 7.2f);

        // Affichage de la taille de la liste
        System.out.println("La taille de la liste est de : " + li.size() );

        // Affichage de tous les éléments
        for (int i = 0; i < li.size(); i++){
            System.out.println( "li["+ i +"] : "+ li.get(i) );
        }

        // Suppression du dernier élément de la liste
        li.remove( (li.size() - 1) );

        System.out.println( "Nouvelle taille de la liste après suppression du dernier élément : "+ li.size() );

    }
}
```

Ce qui affiche :

```bash
La taille de la liste est de : 4
li[0] : 3.2
li[1] : 4.2
li[2] : 3.6
li[3] : 7.2
Nouvelle taille de la liste après suppression du dernier élément : 3

Process finished with exit code 0
```



## Exercice 2 : Définition d'une pile logicielle

### Définir la classe `ElementPile`

```java
public class ElementPile {

    private int elem;

    public ElementPile(){
        this.elem = 0;
    }

    public ElementPile(int el){
        this.elem = el;
    }

    public int getElem(){
        return this.elem;
    }

    public void setElem(int elem){
        this.elem = elem;
    }

}
```

### Définir la classe `Pile`

```java
import java.util.LinkedList;

public class Pile {

    private LinkedList<Integer> content;
    private int max;

    // On initialise une pile vide
    public Pile (){
        this.content = new LinkedList<Integer>();
    }

    // Si la pile a au moins un élément, elle n'est pas vide
    public boolean estVide (){
        if( this.content.size() > 0){
            return false;
        } else {
            return true;
        }
    }

    // Retourne le sommet de la pile
    public int sommet (){
        return this.content.get( (this.content.size() -1) );
    }

    // Ajoute un élément au sommet de la pile
    public void empile (int value){
        this.content.add(value);
    }

    // Retire le dernier element de la pile et le return
    public int depile (){
        int output = this.sommet();
        this.content.remove( (this.content.size() -1) );
        return output;
    }

    // Affiche le contenu d'une pile
    public void affiche(){
        System.out.println("*** Affichage du contenu de la pile ***");

        for(int i = (this.content.size() - 1); i >= 0 ; i--){
            System.out.println( "pile.content["+ i +"] : "+ this.content.get(i) );
        }

        System.out.println("*** Fin du contenu de la pile ***");
    }

    public int getSize(){
        return this.content.size();
    }

}
```

### Utilisation de la classe `Pile`

```java
public class Main {
    public static void main(String[] args){

        Pile pile = new Pile();
        System.out.println( "[Avant ajout] La pile est vide : " + pile.estVide() );
        pile.empile(1);
        pile.empile(42);
        pile.empile(33);
        System.out.println( "[Après ajout] La pile est vide : " + pile.estVide() );

        System.out.println( "Taille de la pile avant dépile : " + pile.getSize() );
        System.out.println( "Dépile de la valeur : " + pile.depile() );
        System.out.println( "Taille de la pile après dépile : " + pile.getSize() );

        System.out.println( "Sommet de la pile : " + pile.sommet() );

        pile.affiche();

    }
}
```

Ce qui affiche 

```bash
[Avant ajout] La pile est vide : true
[Après ajout] La pile est vide : false
Taille de la pile avant dépile : 3
Dépile de la valeur : 33
Taille de la pile après dépile : 2
Sommet de la pile : 42
*** Affichage du contenu de la pile ***
pile.content[1] : 42
pile.content[0] : 1
*** Fin du contenu de la pile ***
```



## Exercice 3 : Utilisation d'une pile logicielle

###Création de la mothode d'ajout à la pile via `Scanner`

```java
import java.util.Scanner;

public class TestPile {

    public static void main(String[] args){

        Pile A = new Pile();

        Scanner entree = new Scanner(System.in);
        System.out.print("Saisissez les nombres à ajouter à la pile A :\n");
        while(true){
            int val = entree.nextInt();
            if(val != -1)
            {
                entree.nextLine();
                A.empile(val);
            }
            else{
                break;
            }
        }
        System.out.println("Enregistrement terminé");
        entree.close();
        
    }

}
```



### Création et utilisation de la méthode `Pile.union()`

####Méthode Union

```java
public class Pile {
	// ...

    // Fusionne deux pile tout en conservant les piles d'origine
    public static Pile union(Pile A, Pile B ){
        Pile C = new Pile();
        Pile BackUp = new Pile();

        // Dépile de A
        while( !A.estVide() ){
            int empileValue = A.depile();

            BackUp.empile( empileValue );
            C.empile( empileValue );
        }

        // Restitution du BackUp de A
        while( !BackUp.estVide() ){
            A.empile( BackUp.depile() );
        }

        // Dépile de B
        while( !B.estVide() ){
            int empileValue = B.depile();

            BackUp.empile( empileValue );
            C.empile( empileValue );
        }

        // Restitution du BackUp de B
        while( !BackUp.estVide() ){
            B.empile( BackUp.depile() );
        }

        return C;
    }
	// ...
}
```

####Test de la méthode Union

```java
public class TestPile {

    public static void main(String[] args){

        Pile pile1 = new Pile();
        pile1.empile(24);
        pile1.empile(42);

        Pile pile2 = new Pile();
        pile2.empile(33);
        pile2.empile(55);

        Pile pile3 = new Pile();
        pile3 = Pile.union(pile1, pile2);
        pile3.affiche();

    }

}
```

Ce qui affiche :

```bash
*** Affichage du contenu de la pile ***
pile.content[3] : 33
pile.content[2] : 55
pile.content[1] : 24
pile.content[0] : 42
*** Fin du contenu de la pile ***
```



### Création et utilisation de la méthode `Pile.tri()`

#### Méthode `tri()`

```java
public class Pile {
	// ...
	public static Pile tri(Pile p){
        Pile res = new Pile();
        Pile aux = new Pile();

        while( !p.estVide() ) {
            if (res.estVide() || (p.sommet() < res.sommet())) {
                res.empile( p.depile() );

                if( !aux.estVide() ){
                    while ( !aux.estVide() ){
                        res.empile( aux.depile() );
                    }
                }
            }
            else {
                aux.empile( res.depile() );
            }
        }

        return res;
    }
	// ...
}
```

#### Test du tri

```java
import java.util.Scanner;

public class TestPile {

    public static void main(String[] args){

        // {4, 3, 2, 5, 8, 2, 6, 9, 3}.
        Pile A = new Pile();

        Scanner entree = new Scanner(System.in);
        System.out.print("Saisissez les nombres à ajouter à la pile A :\n");
        while(true){
            int val = entree.nextInt();
            if(val != -1)
            {
                entree.nextLine();
                A.empile(val);
            }
            else{
                break;
            }
        }
        entree.close();
        System.out.println("Enregistrement terminé");

        System.out.println("\nAffichage avant tri : ");
        A.affiche();

        System.out.println("\nAffichage après tri : ");
        A = Pile.tri(A);
        A.affiche();
        
    }

}
```

Ce qui affiche :

```bash
Saisissez les nombres à ajouter à la pile A :
4
3
2
5
8
2
6
9
3
-1
Enregistrement terminé

Affichage avant tri : 
*** Affichage du contenu de la pile ***
pile.content[8] : 3
pile.content[7] : 9
pile.content[6] : 6
pile.content[5] : 2
pile.content[4] : 8
pile.content[3] : 5
pile.content[2] : 2
pile.content[1] : 3
pile.content[0] : 4
*** Fin du contenu de la pile ***

Affichage après tri : 
*** Affichage du contenu de la pile ***
pile.content[8] : 2
pile.content[7] : 2
pile.content[6] : 3
pile.content[5] : 3
pile.content[4] : 4
pile.content[3] : 5
pile.content[2] : 6
pile.content[1] : 8
pile.content[0] : 9
*** Fin du contenu de la pile ***
```



## Exercice 4 : Définition d'une pile logicielle

```java
public class Pile<Type> {

    private LinkedList<Type> content;
    private int max;

    // On initialise une pile vide
    public Pile (){
        this.content = new LinkedList<Type>();
    }

    // Si la pile a au moins un élément, elle n'est pas vide
    public boolean estVide (){
        if( this.content.size() > 0){
            return false;
        } else {
            return true;
        }
    }

    // Retourne le sommet de la pile
    public Type sommet (){
        return this.content.get( (this.content.size() -1) );
    }

    // Ajoute un élément au sommet de la pile
    public void empile (Type value){
        this.content.add(value);
    }

    // Retire le dernier element de la pile et le return
    public Type depile (){
        Type output = this.sommet();
        this.content.remove( (this.content.size() -1) );
        return output;
    }

    // Affiche le contenu d'une pile
    public void affiche(){
        System.out.println("*** Affichage du contenu de la pile ***");

        for(int i = (this.content.size() - 1); i >= 0 ; i--){
            System.out.println( "pile.content["+ i +"] : "+ this.content.get(i) );
        }

        System.out.println("*** Fin du contenu de la pile ***");
    }

    public int getSize(){
        return this.content.size();
    }
}
```

Ainsi on peu tester la pile générique comme ceci :

```java
public class PileTest {

    public static void main(String[] args){
        Pile<String> p = new Pile();

        p.empile("Test1");
        p.empile("Test2");
        p.affiche();
    }

}
```

Ce qui affiche :

```bash
*** Affichage du contenu de la pile ***
pile.content[1] : Test2
pile.content[0] : Test1
*** Fin du contenu de la pile ***
```

