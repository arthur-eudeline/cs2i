### Arthur Eudeline B3ca

# TP6 Java & POO



##Excercice 0

Les interfaces permettent d'émuler une sorte d'héritage multiple en Java, ce qui est en théorie impossible. Les interfaces repertorient un ensemble de méthodes que doivent obligatoirement implémenter les classes qui implémentent l'interface. 

Le code des méthodes n'étant pas défini dans l'interface, il doit être construit dans la classe implémentant l'interface. De cette manière, on obtient avec certitude des classes qui possèdent certaines méthodes, qui peuvent effectuer du code spécifique à leur contexte, mais qui renverront toujours le même type de données.

Ainsi, Java permet de pouvoir créer de variables d'un type spécifique d'interface et qui peuvent recevoir en valeur une instance d'une classe qui implémente la dite interface. Nous sommes ainsi en mesure d'assouplir les contraintes imposée par le typage fort de Java. Exemple : 

```java
public class Main {

  public static void main(String args[]){

    // Ampoule() et Radio() implémentent tout deux l'interface éléctrique
    Ampoule monAmpoule = new Ampoule();
    Radio maRadio = new Radio();

    // On déclare une varible de type Electrique, donc de type INTERFACE Electrique
    Electrique c;

    Boolean sombre = false;

    // Cette variable peut au choix recevoir une instance d'Ampoule() ou de Radio() car ces classes implémentent toutes deux l'interface Electrique
    if ( sombre == true ){
      c = monAmpoule;
    } else {
      c = maRadio;
    }

    // Peut importe la classe stoquée, vu qu'elles implémentent la même interface nous pouvons accèder aux mêmes méthodes
    c.allumer();
    c.eteindre();

  }

}
```



## Exercice 1 : définir un paramètre de type fonction

### 1.1 : Definition des classes `Cosx_x`  et `TestCosx_x`

- Définition de l'interface `Fonction`

```java
public interface Fonction {

  public double appel(double x);

}
```

- Définition de la classe `Cosx_x`

```java
public class Cosx_x implements Fonction {

  public double appel(double x) {
    return Math.cos(x) - x;
  }

}
```

- Définition de la classe `TestCosx_x`

```java
public class TestCosx_x {

  public static void main(String args[]){

    Cosx_x calcul = new Cosx_x();

    for (double i = 0; i <= 10; i ++){
      System.out.println( calcul.appel(i / 10) );
    }

  }

}
```

L'exécution de ce programme affiche :

![image-20181230144032927](assets/image-20181230144032927-6177233.png)



### 1.2 : Réécriture de la fonction `TestDichotomie.zero()` 

```java
public class TestDichotomie {

  static double zero(Fonction fonction, double a, double b, double epsilon) {
    // Nous avons maintenant accès à une instance de Cosx_x() ou de toute autre instance de classe implémentant l'interface Fonction et qui soit passée en argument
  }

  ...
    
  public static void main(String[] args) {

    Fonction fonction = new Cosx_x();

    double y = zero(fonction , 0, 4, 1e-12);
    System.out.println(y);

  }
}
```



### 1.3 : Écrire une fonction résolvant $cos(x) =  x$

Il nous suffit de reprendre la méthode `TestDichotomie.zero()` et d'y remplacer tous les appels à `TestDichotomie.f()` par un appel à notre fonction `Cosx_x.appel()` :

```java
  static double zero(Fonction fonction, double a, double b, double epsilon) {

    System.out.println(fonction.appel(a));

    // Si on n'a pas fonction.appel(a) < 0 on échange a et b
    if (fonction.appel(a) > 0) {
      double w = a;
      a = b;
      b = w;
    }

    // Iterations jusqu’à avoir |a - b| <= epsilon
    while (Math.abs(b - a) > epsilon) {
      double c = (a + b) / 2;
      if (fonction.appel(c) < 0)
        a = c;
      else
        b = c;
    }

    // Lorsque |a - b| <= epsilon, une valeur comprise entre a et b convient
    return (a + b) / 2;
  }
```

Ainsi, lorsque nous relançons le programme avec les paramètres :

```java
	public static void main(String[] args) {

    Fonction fonction = new Cosx_x();

    double y = zero(fonction , 0, 1000, 1e-10);
    System.out.println("Cos(x) = x lorsque x vaut " + y);

  }
```

 nous obtenons ceci :

![image-20181230170803775](assets/image-20181230170803775-6186083.png)

Pour vérifier si notre résultat est correct, nous traçons $f(x) = cos(x) - x$ sur [GeoGebra](https://www.geogebra.org)

![image-20181230171020435](assets/image-20181230171020435-6186220.png)

