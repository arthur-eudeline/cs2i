### Arthur Eudeline - B3ca

#Java TP4



## Exercice 2 : l'héritage



###Pages 50, 51  : utilisation du mot clé `super`

On peut ici déclarer la classe `Personne` :

```java
public class Personne {
    public String nom, prenom;
    public int anneeNaissance;

    // Constructeur par défaut
    public Personne() {
        nom="";
        prenom="";
    }

    // Nouveau constructeur
    public Personne(String nom, String prenom, int anneeNaissance)
    {
        this.nom=nom;
        this.prenom=prenom;
        this.anneeNaissance=anneeNaissance;
    }

    // Permet l'affichage de toutes les infos d'un employé
    public void displayInfos(){
        System.out.println("\n*** Infos ****");
        System.out.println("Prénom : " + this.prenom);
        System.out.println("Nom : " + this.nom);
        System.out.println("Année de naissance : " + this.anneeNaissance);
        System.out.println("*** /Infos ****");
    }
}
```

Et une classe `Employe` qui héritera de la classe `Personne`. Celle-ci héritera également du constructeur et également de la méthode `displayInfos()`, grâce à l'utilisation du mot clé `super` qui fait directement référence à la classe parente :

```java
public class Employe extends Personne {

    // Constructeur par défaut
    public Employe() {}

    // Nouveau constructeur
    public Employe(String nom, String prenom, int anneeNaissance) {

        // Super renvoie à la classe mère, ici Personne, et vu qu'on appelle la méthode "racine" (puisque qu'on ne met rien 		derrière "super"), on fait appel au constructeur
        super (nom, prenom, anneeNaissance);

    }

    // Permet l'affichage des informations
    public void displayInfos(){

        // On appelle directement la méthode définie dans la classe parente
        super.displayInfos();

    }
}
```



Ainsi, on peut tester ces deux classes via le script suivant :

```java
public class TestPersonne {

    public static void main(String[] args){

        Personne personne = new Personne("Doe", "Jane", 1990);
        Employe employee = new Employe("Doe", "John", 1992);

        personne.displayInfos();
        employee.displayInfos();

    }

}
```

Qui affiche : 

```bash
*** Infos ****
Prénom : Jane
Nom : Doe
Année de naissance : 1990
*** /Infos ****


*** Infos ****
Prénom : John
Nom : Doe
Année de naissance : 1992
*** /Infos ****
```



### Page 52 : redéfinition d'une méthode

Reprenons notre classe `Employe` et modifions à présent la méthode `displayInfos()` que nous avions initialement pris à la classe parente. En n'utilisant pas le mot clé `super`  à l'intérieur de cette méthode, nous ne faisons plus appel à la méthode de la classe parente `Personne`, mais redéfinissons directement le contenu de la méthode. Ainsi, si nous venions à créer une classe imaginaire `Interimaire` qui hériterait de la classe `Employe`, si nous faisons appel à `super.displayInfos()` nous ferions appel à la méthode `Employe.displayInfos()`  et non `Personne.displayInfos()` comme ça aurait été le cas si nous avions conservé le mot clé `super` dans la méthode `Employe.displayInfos()`.

```java
public class Employe extends Personne {
	// [...]

    // Permet l'affichage des informations
    public void displayInfos(){

        System.out.println("\n*** Infos de l'employé ****");
        System.out.println("Prénom : " + this.prenom);
        System.out.println("Nom : " + this.nom);
        System.out.println("Année de naissance : " + this.anneeNaissance);
        System.out.println("*** /Infos de l'employé ****");

    }
}
```



### Page 56 : l'opérateur `instanceof`

```java
public class TestPersonne {
    public static void main(String[] args){
        // [...]
        
        // Définition aléatoire du type de Jean
        Personne jean;
        long rand = Math.round( Math.random() * (100 - 1) );
        if ( rand%2 == 1 ) {
            jean = new Personne("Charles", "Jean", 1998);
        } else {
            jean = new Employe("Charles", "Jean", 1998);
        }

        // Affichage d'un message en fonction du type de Jean
        if (jean instanceof Employe){
            System.out.println("Jean est un EMPLOYE");
        } else {
            System.out.println("Jean est une PERSONNE");
        }

    }
}
```



### Page 58 : Transtypage

```java
public class Employe extends Personne {

    public float salaire;
    
    // [...]
    
}
```

```java
public class TestPersonne {
    public static void main(String[] args){
      // [...]
        
			// Affichage du salaire
      Personne aron = new Employe("Darse", "Aron", 1983, (float) 2243.50);

      // float salaire = aron.salaire; --> génère une erreur : Personne n'a pas l'attribut salaire
        
      // en forçant le typage Employe, même si on a déclaré aron en temps que Personne
      float salaire = ((Employe) aron).salaire;

      System.out.println("Salaire d'Aron " + salaire);   
    }
}
```

Ce qui affiche :

```
Salaire d'Aron 2243.5
```



### Page 60 : l'auto-référence avec `this`

En Java, il est possible de déclarer plusieurs constructeurs pour une même classe et qui peuvent être appelés en fonction du nombre de paramètres qui leur sont passés. De cette façon, on peut passer des valeurs par défaut. Cependant, l'utilisation du mot clé `this` doit de faire en premier dans le corps du constructeur. Il est donc **impossible de déclarer des variables provisoires dans le corps du constructeur <u>avant</u> de faire appel au constructeur par l'intermédiaire de `this`**. De ce fait il faut passer par l'utilisation des **constantes** pour émuler la variable :

```java
public class Employe extends Personne {
    // [...]
    
    // Déclaration d'une constante SMIC qui sera réutilisable à l'intérieur du constructeur
    public static final float SMIC = 1188;
    
    // Constructeur le plus "complet" (qui utilise la totalité des paramètres)
    public Employe(String nom, String prenom, int anneeNaissance, float salaire) {
        super (nom, prenom, anneeNaissance);
        this.salaire = salaire;
    }
    
    // Si on ne passe pas le paramètre "salaire" au constructeur, c'est cet autre constructeur qui est appelé
    public Employe(String nom, String prenom, int anneeNaissance){
        // Avec this, on appelle le constructeur défini dans Employe
        this(nom, prenom, anneeNaissance, Employe.SMIC); // Pour appelé notre "variable" SMIC, on appelle en réalité la 		constante SMIC défini plus haut
    }
}
```



## Exercice 3 : Utilisation des exceptions (1/2)

```java
public class TestArithmeticException {
    public static void main(String[] args){

        int a = 20;
        int divider = 0;

        // On essaye volontairement de diviser par Zéro
        try {

            int result = a / divider;

        } catch ( ArithmeticException e){

            // On affiche l'erreur générée
            System.out.println( e.getMessage() );

        }


    }
}
```

Ce qui affiche en console :

```
Connected to the target VM, address: '127.0.0.1:50352', transport: 'socket'

/ by zero
Disconnected from the target VM, address: '127.0.0.1:50352', transport: 'socket'

Process finished with exit code 0
```



## Exercice 4 : Utilisation des exceptions (2/2)

```java
import java.util.Scanner;

public class TestParseIntException {
    public static void main(String[] args){

        int somme = 0;

        System.out.println("Saisissez les entiers dont vous voulez calculer la somme (saisissez -1 pour sortir) :");

        Scanner entree = new Scanner(System.in);
        while(true){

            try {
                if( Integer.parseInt( entree.next() ) != -1 ){
                    somme = somme + Integer.parseInt( entree.next() );
                } else {
                    break;
                }

            } catch (Exception e){
                System.out.println( "Error : " + e.getMessage() );
                break;
            }

        }
        entree.close();

        System.out.println("\nLa somme calculée est de : " + somme);

    }
}
```

Ce qui affiche par exemple :

```bash
Saisissez les entiers dont vous voulez calculer la somme (saisissez -1 pour sortir :
1
2
test
Error : For input string: "test"

La somme calculée est de : 2

Process finished with exit code 0
```



## Exercice 5 : Création des exceptions

### Création de la classe `Entreprise` :

```java
public class Entreprise {

    private int nbEmploye;
    private long capital;
    private String nom;
    private String mission;

    public Entreprise(){}

    public Entreprise (String nom, String mission, int nbEmploye, long capital){

        this.nom        = nom;
        this.mission    = mission;
        this.nbEmploye  = nbEmploye;
        this.capital    = capital;

    }

    public String mission() throws MissionSecreteException{
        return this.mission;
    }

    public long capital() throws SansProfitException{
        return this.capital;
    }

    public void displayInfos(){

        System.out.println("\n**** Informations de l'entreprise ****");
        System.out.println("Nom : " + this.nom );

        // Mission
        try {
            System.out.println( "Mission : " + this.mission() );
        } catch (Exception e){
            System.out.println( "Mission : [ERREUR] " + e.getMessage() );
        }

        System.out.println("Nombre d'employés : " + this.nbEmploye);

        try {
            System.out.println( "Capital : " + this.capital() );
        } catch (Exception e){
            System.out.println( "Capital : [ERREUR] " + e.getMessage() );
        }

    }

    public static void displayInfos(Entreprise[] entreprisesList){

        for( int i = 0; i < entreprisesList.length; i++ ){

            entreprisesList[i].displayInfos();

        }

    }
}
```

### Création de la classe `EntrepriseSecrete`  :

#### Création de la classe :

```java
public class EntrepriseSecrete extends Entreprise {

    public EntrepriseSecrete(String nom, String mission, int nbEmploye, long capital){
        super(nom, mission, nbEmploye, capital);
    }

    public String mission() throws MissionSecreteException{
        throw new MissionSecreteException("L'entreprise est secrète. Sa mission l'est donc aussi.");
    }

}
```

#### Création de l'exception `MissionSecreteException`

```java
public class MissionSecreteException extends Exception {

    public MissionSecreteException(String message){
        super(message);
    }

}
```

### Création de la classe `EntrepriseSansProfit`

#### Création de la classe :

```java
public class EntrepriseSansProfit extends Entreprise {

    public EntrepriseSansProfit(String nom, String mission, int nbEmploye, long capital){
        super(nom, mission, nbEmploye, capital);
    }

    public long capital() throws SansProfitException{
        throw new SansProfitException("Cette entreprise ne génère pas de profit.");
    }
    
}
```

#### Création de l'exception `SansProfitException`

```java
public class SansProfitException extends Exception {

    public SansProfitException(String message){
        super(message);
    }

}
```

### Test de génération d'exception avec les classes :

```java
public class TestEntreprise {
    public static void main(String[] args) {

        Entreprise[] entreprises = new Entreprise[4];

        entreprises[0] = new Entreprise("Renault", "Créer des voitures", 7500, 70000);
        entreprises[1] = new EntrepriseSecrete("CIA", "Récolter des renseignements pour le gouvernement américain", 2255, 8000);
        entreprises[2] = new Entreprise("Peugeot", "Créer des voitures", 8300, 90000);
        entreprises[3] = new EntrepriseSansProfit("Croix Rouge", "Sauver des vies", 2500, 0);

        Entreprise.displayInfos( entreprises );

    }
}

```

Ce qui affiche en console :

```
**** Informations de l'entreprise ****
Nom : Renault
Mission : Créer des voitures
Nombre d'employés : 7500
Capital : 70000

**** Informations de l'entreprise ****
Nom : CIA
Mission : [ERREUR] L'entreprise est secrète. Sa mission l'est donc aussi.
Nombre d'employés : 2255
Capital : 8000

**** Informations de l'entreprise ****
Nom : Peugeot
Mission : Créer des voitures
Nombre d'employés : 8300
Capital : 90000

**** Informations de l'entreprise ****
Nom : Croix Rouge
Mission : Sauver des vies
Nombre d'employés : 2500
Capital : [ERREUR] Cette entreprise ne génère pas de profit.
```



##Exercice 6 : Exceptions et piles

Pour ajouter les exceptions `PilePleineException` et `PileVideException`, nous n'avons besoins que de **modifier que quelques parties du code de notre classe `Pile`** :

```java
import java.util.LinkedList;

public class Pile {
  // [...]
  
  // Taille maximale de la pile
  public static final int MAX_SIZE = 10;
  
  // [...]
  
  // Ajoute un élément au sommet de la pile
  public void empile (int value) throws PilePleineException{
    // Si on tente d'ajouter quelque chose alors que la Pile a atteint sa taille maximum
		if( this.getSize() >= Pile.MAX_SIZE ){
    	throw new PilePleineException("La pile est pleine. Impossible d'y ajouter quoi ce soit d'autre.");
    } else {
      this.content.add(value);
    }
  }
  
  // Retire le dernier element de la pile et le return
  public int depile (){
  	int output = this.sommet();
    this.content.remove( (this.content.size() -1) );
    return output;
  }
  
  // [...] 
}
```

On peut ainsi tester notre code avec :

```java
import java.util.Scanner;

public class TestPile {
    public static void main(String[] args){

        Pile A = new Pile();

        System.out.print("Saisissez les nombres à ajouter à la pile A :\n");

        Scanner entree = new Scanner(System.in);
        while(true){
            int val = entree.nextInt();
            if (val != -1) {
                entree.nextLine();

                try {
                    A.empile(val);
                } catch (PilePleineException e){
                    System.out.println( "Erreur : " + e.getMessage() );
                }
            } else{
                break;
            }
        }
        entree.close();
        System.out.println("Enregistrement terminé");

    }
}
```

Ce qui affichera en console :

```
Saisissez les nombres à ajouter à la pile A :
1
2
3
4
5
6
7
8
9
10
11
Erreur : La pile est pleine. Impossible d'y ajouter quoi ce soit d'autre.
```

