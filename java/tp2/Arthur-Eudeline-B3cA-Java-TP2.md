### Arthur EUDELINE - B3cA

# Java TP2



## 1.0 : Utilisation de la classe `Scanner` 

```java
public static void main(String[] args) {

    float somme = 0;
    int nbNombre = 0;
    Scanner entree = new Scanner(System.in);
    System.out.print("Donner les nombres dont on veux calculer la moyenne (terminer par -1):\n");
    while(true){
        float nombre = entree.nextFloat();
        if(nombre != -1)
        {
            entree.nextLine();
            somme = somme + nombre;
            nbNombre++;
        }
        else{
            break;
        }
    }
    System.out.println("Moyenne calculée : " + somme/nbNombre);
    entree.close();

}
```

![javaTP2_01](../../php/tp-mvc/assets/javaTP2_01.png)



## 2.0 : Nombres aléatoires

```java
public static void main(String[] args) {

    Scanner entree = new Scanner(System.in);
    System.out.println("Combien de nombres à calculer : ");

    int n = Math.round(entree.nextFloat());
    double somme = 0;
    double random;
    double ecartType = 0;

    for( int i = 1; i <= n ; i++ ){

        random = Math.random();

        somme = somme + random;
        ecartType = ecartType + Math.pow(random, 2);

    }

    double moyenne = somme / n;
    System.out.println("\nLa moyenne calculée est de : " + moyenne);

    ecartType = Math.sqrt( (ecartType / n) - Math.pow(moyenne, 2) );
    System.out.println("\nL'écart type est de : " + ecartType);

}
```

Ce qui affiche pour **n = 100 000** :

```
Combien de nombres à calculer : 
100000

La moyenne calculée est de : 0.5017701084298963

L'écart type est de : 0.2886194474369091
BUILD SUCCESSFUL (total time: 6 seconds)
```



## 3 : Manipulation des dates en Java



### 3.1 : la méthode `java.lang.System.currentTimeMillis()`

```java
public static void main(String[] args) throws InterruptedException {

    double ecart;
    double mesure1;
    double mesure2;

    mesure1 = System.currentTimeMillis();
    System.out.println( "Mesure n°1 : " + mesure1 );

    TimeUnit.MINUTES.sleep(1);

    mesure2 = System.currentTimeMillis();
    System.out.println( "Mesure n°2 : " + mesure2 );

    ecart = mesure2 - mesure1;
    System.out.println( "\nÉcart : " + ecart );

    double nbAnnee = mesure2 / ( 3.154E10 );
    System.out.println("\nNombre approximatif d'années écoulées : " + nbAnnee);
}
```

Ce qui affiche : 

```
run:
Mesure n°1 : 1.539162911355E12
Mesure n°2 : 1.539162971368E12

Écart : 60013.0

Nombre approximatif d'années écoulées : 48.80037157675967
BUILD SUCCESSFUL (total time: 1 minute 0 seconds)
```



### 3.2 : L'objet `java.util.Date`

Les modifications sont apportées juste après les mesures :

```java
mesure1 = System.currentTimeMillis();
System.out.println( "Mesure n°1 mls : " + mesure1 );
Date date1 = new Date(mesure1);
System.out.println( "Mesure n°1 date : " + date1 );
```

Ce qui affiche :

```
run:
Mesure n°1 mls : 1539164231190
Mesure n°1 date : Wed Oct 10 11:37:11 CEST 2018
Mesure n°2 mls : 1539164291235
Mesure n°2 date : Wed Oct 10 11:38:11 CEST 2018

Écart : 60045.0

Nombre approximatif d'années écoulées : 48.80038970307546
BUILD SUCCESSFUL (total time: 1 minute 0 seconds)
```



### 3.3 : L'objet `java.util.Calendar`

```java
Calendar calendarObj = Calendar.getInstance();
        
String[] nomsMois = {"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"};
String Cmois = nomsMois[calendarObj.get(Calendar.MONTH)];

String[] nomsJours = {"Samedi", "Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi"};
String CnomJour = nomsJours[calendarObj.get(Calendar.DAY_OF_WEEK)];

int Cjour = calendarObj.get(Calendar.DAY_OF_MONTH);

int Cannee = calendarObj.get(Calendar.YEAR);

System.out.println("\nNous sommes le "+ CnomJour + " " + Cjour + " " + Cmois + " " + Cannee);
```

Ce qui affiche :

```
run:
Nous sommes le Mercredi 10 Octobre 2018
BUILD SUCCESSFUL (total time: 0 seconds)
```



### 3.4 : L'objet `java.text.SimpleDateFormat`

```java
Date d = new Date();
SimpleDateFormat f = new SimpleDateFormat();
String dateFormatee = f.format(d);

System.out.println(dateFormatee);
```

Ce qui affiche :

```
run:
10/10/18 1:49 PM
BUILD SUCCESSFUL (total time: 0 seconds)
```



## 4 : Manipulation des chaînes de caractère :



### 4.1 : Conversion en chaîne :

On peut passer par la méthode `String.valueOf()` afin de couvertire des entiers en une chaîne de caractère :

```java
int nombre = 42;
String texte = String.valueOf(nombre);

System.out.println("Mon nombre est :" + texte);
```

Ce qui affiche :

```
Mon nombre est :42
```



### 4.2 : Conversion d'une chaîne en entier :

Il nous est possible d'utiliser la méthode ``Ìnt.valueOf()` sur le même principe que la question précédente :

```java
String texte2 = "42";
int nombre2 = Integer.valueOf(texte2);
System.out.println("Mon entier est : "+ nombre2);
```

Ce qui affiche :

```
Mon entier est : 42
```



### 4.3 :  Conversion d'une chaîne en réel

```java
String texte3 = "0.12345e4";
Float nombre3 = Float.parseFloat(texte3);

System.out.println("Mon réel est : "+ nombre3);
```

Ce qui affiche : 

```Mon réel est : 1234.5
Mon réel est : 1234.5
```



### 4.4 : Élimination des blancs autour d'une chaîne

On peut utiliser les méthodes `String.trim()` qui retire les espaces et `String.toLowerCase()` qui retourne une chaîne en minuscules :

```java
String nomVille = " Le Puy-en-Velay ";
System.out.println("\nnomVille sans espace autour et en min : [" + nomVille.trim().toLowerCase() +"]");
```

Ce qui affiche :

```
nomVille sans espace autour et en min : [le puy-en-velay]
```



### 4.5 : Utilisation de la méthode `charAt()`

```java
String s1 = "String n°1";
String s2 = "string n°2";

System.out.print("Les deux chaînes commencent par le même caractère : ");
if(s1.charAt(0) == s2.charAt(0)){
    System.out.println("TRUE");
} else {
    System.out.println("FALSE");
}
```

Ce qui affiche : 

```
Les deux chaînes commencent par le même caractère : FALSE
```

Cela résulte du fait que `String.charAt()` est sensible à la casse. Si on change `s2` on obtient `TRUE` :

```java
String s1 = "String n°1";
String s2 = "String n°2";
```

```
Les deux chaînes commencent par le même caractère : TRUE
```



### 4.6 : Comparaison de chaînes

```java
String s21 = "abcd";
String s22 = "abcd";

System.out.println("\ns21 = "+ s21);
System.out.println("s22 = "+ s22);
System.out.println("\n(s21 == s22) = "+ String.valueOf(s21 == s22).toUpperCase() );
System.out.println("(s21.equals(s22)) = "+ String.valueOf(s21.equals(s22)).toUpperCase() );
System.out.println("(s21.compareToIgnoreCase(s22)) = "+ String.valueOf(s21.compareToIgnoreCase(s22)).toUpperCase() );
```

Ce qui affiche :

```
s21 = abcd
s22 = abcd

(s21 == s22) = TRUE
(s21.equals(s22)) = TRUE
(s21.compareToIgnoreCase(s22)) = 0
```

```
s21 = abcd
s22 = ABCD

(s21 == s22) = FALSE
(s21.equals(s22)) = FALSE
(s21.compareToIgnoreCase(s22)) = 0
```



### 4.7 : « Commence par », « fini par »

```java
String s31 = "Lorem ipsum dolor sit amet.";
String s32 = "Lorem";

System.out.println("\n31 = "+ s31);
System.out.println("s32 = "+ s32);

System.out.println("\ns31 commence par s32 = " + String.valueOf( s31.startsWith(s32) ).toUpperCase() );
System.out.println("s31 finit par s32 = " + String.valueOf( s31.endsWith(s32) ).toUpperCase() );
System.out.println("s31 contient s32 = " + String.valueOf( s31.contains(s32) ).toUpperCase() );
```

Ce qui affiche :

```
31 = Lorem ipsum dolor sit amet.
s32 = Lorem

s31 commence par s32 = TRUE
s31 finit par s32 = FALSE
s31 contient s32 = TRUE
```



### 4.8 : Sous-chaînes d'une chaîne

```java
String s41 = "Lorem ipsum dolor sit amet.";
String s42 = "Lorem";

if(s41.contains(s42)){
   s41 = s41.replace(s42, "");
}
System.out.println("s41 = " + s41);
```

Ce qui affiche :

```
s41 =  ipsum dolor sit amet.
```



## 5 : factorielles et objets BigInteger 



### 5.1 : Calcul simple de factorielle

```java
public static long factorielle1(int n){

        long result = 1;

        for(int i = 2; i <= n; i++){
            result = result * i;
        }

        return result;
    }

public static void main(String[] args){

    // 5.1
    int i = 1;
    boolean factoTrue = false;
    while(factorielle1(i) / factorielle1(i - 1) == i ){
        i++;
    }

    System.out.println("La factorielle est vérifiée jusqu'à : " + (i-1));
}
```

Ce qui affiche :

```
La factorielle est vérifiée jusqu'à : 20
```



### 5.2 : Calcul en précision illimité de factorielle

```java
static BigInteger factorielle2(int n){

    BigInteger result = BigInteger.valueOf(1);

    for(int i = 2; i <= n; i++){
        result = result.multiply( BigInteger.valueOf(i) );
    }

    return result;
}

public static void main(String[] args){
    // 5.2
    int i2 = 1;
    while( factorielle2(i2).divide(factorielle2(i2 - 1)).equals( BigInteger.valueOf(i2) ) ){
        i++;
    }
}
```



## 6 : Les arguments de la ligne de commande

```java
public class Calcul {
    public static void main(String[] args){
        double val1 = Double.valueOf(args[0]);
        double val2 = Double.valueOf(args[2]);

        String operator = args[1].trim();

        double result = 0;

        if( operator.equals("+") ){
            result = val1 + val2;
        } else if( operator.equals("*") || operator.equals("x") ){
            result = val1 * val2;
        } else if ( operator.equals("-") ){
            result = val1 - val2;
        } else if ( operator.equals("^") ){
            result = Math.pow(val1, val2);
        } else if ( operator.equals("/") ) {
            result = val1 / val2;
        } else {
            System.out.println("Pas d'opérateur trouvé.");
        }

        System.out.println(args[0] + args[1] + args[2] + " = " + result);
    }
}
```

Ainsi, nous pouvons interragir avec `Calcul` de la manière suivante à travers un terminal :

![image-20181017131907661](../../../../OneDrive/Cours/Développement/assets/image-20181017131907661.png)



## 7 : Fusion de deux tableaux triés :

```java
public static void main(String[] args){
    int[] arr1 = {1, 42, 55, 90, 3992};
    int[] arr2 = {3, 57, 2, 33, 78};

    int[] arr3 = arrayMergeAndSort(arr1, arr2);

    for(int i = 0; i < arr3.length; i++){
        System.out.print("  " + arr3[i] + " ");
    }
}

public static int[] arrayMergeAndSort(int[] arr1, int[] arr2){

    int[] arr3 = new int[ arr1.length + arr2.length ];

    for (int i = 0; i < arr1.length ; i++){
        arr3[i] = arr1[i];
    }

    for (int i = 0; i < arr2.length; i++){
        arr3[i + arr1.length] = arr2[i];
    }

    Arrays.sort( arr3 );
    return arr3;
}
```

Ce qui affiche :

```
  1   2   3   33   42   55   57   78   90   3992 
```

