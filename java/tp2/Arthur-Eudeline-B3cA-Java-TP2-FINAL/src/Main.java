
import java.lang.Object;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author arthur-eudeline
 */
public class Main {

    /**
     * @param args the command line arguments
     */
//    Exercice 1
//    public static void main(String[] args) {
//
//        float somme = 0;
//        int nbNombre = 0;
//        Scanner entree = new Scanner(System.in);
//        System.out.print("Donner les nombres dont on veux calculer la moyenne (terminer par -1):\n");
//        while(true){
//            float nombre = entree.nextFloat();
//            if(nombre != -1)
//            {
//                entree.nextLine();
//                somme = somme + nombre;
//                nbNombre++;
//            }
//            else{
//                break;
//            }
//        }
//        System.out.println("Moyenne calculée : "+somme/nbNombre);
//        entree.close();
//
//    }
//    /Exercice 1

//    Exercice 2
//    public static void main(String[] args) {
//
//        Scanner entree = new Scanner(System.in);
//        System.out.println("Combien de nombres à calculer : ");
//
//        int n = Math.round(entree.nextFloat());
//        double somme = 0;
//        double random;
//        double ecartType = 0;
//
//        for( int i = 1; i <= n ; i++ ){
//
//            random = Math.random();
//
//            somme = somme + random;
//            ecartType = ecartType + Math.pow(random, 2);
//
//        }
//
//        double moyenne = somme / n;
//        System.out.println("\nLa moyenne calculée est de : " + moyenne);
//
//        ecartType = Math.sqrt( (ecartType / n) - Math.pow(moyenne, 2) );
//        System.out.println("\nL'écart type est de : " + ecartType);
//
//    }
//    /Exercice 2

//    Exercice 3
//    public static void main(String[] args) throws InterruptedException {
//
//        double ecart;
//        long mesure1;
//        long mesure2;
//
//        mesure1 = System.currentTimeMillis();
//        System.out.println( "Mesure n°1 mls : " + mesure1 );
//        Date date1 = new Date(mesure1);
//        System.out.println( "Mesure n°1 date : " + date1 );
//
//        TimeUnit.MINUTES.sleep(0);
//
//        mesure2 = System.currentTimeMillis();
//        System.out.println( "Mesure n°2 mls : " + mesure2 );
//        Date date2 = new Date(mesure2);
//        System.out.println( "Mesure n°2 date : " + date2 );
//
//        ecart = mesure2 - mesure1;
//        System.out.println( "\nÉcart : " + ecart );
//
//        double nbAnnee = mesure2 / ( 3.154E10 );
//        System.out.println("\nNombre approximatif d'années écoulées : " + nbAnnee);
//
//        Calendar calendarObj = Calendar.getInstance();
//
//        String[] nomsMois = {"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"};
//        String Cmois = nomsMois[calendarObj.get(Calendar.MONTH)];
//
//        String[] nomsJours = {"Samedi", "Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi"};
//        String CnomJour = nomsJours[calendarObj.get(Calendar.DAY_OF_WEEK)];
//
//        int Cjour = calendarObj.get(Calendar.DAY_OF_MONTH);
//
//        int Cannee = calendarObj.get(Calendar.YEAR);
//
//        System.out.println("\nNous sommes le "+ CnomJour + " " + Cjour + " " + Cmois + " " + Cannee);
//
//        Date d = new Date();
//        SimpleDateFormat f = new SimpleDateFormat();
//        String dateFormatee = f.format(d);
//
//        System.out.println(dateFormatee);
//    }
//    /Exercice 3

//    Exercice 4
//    public static void main(String[] args) throws InterruptedException {
//
//        // 4.1
//        int nombre = 42;
//        String texte = String.valueOf(nombre);
//        System.out.println("Mon nombre est :" + texte);
//
//        // 4.2
//        String texte2 = "42";
//        int nombre2 = Integer.valueOf(texte2);
//        System.out.println("Mon entier est : "+ nombre2);
//
//        // 4.3
//        String texte3 = "0.12345e4";
//        Float nombre3 = Float.parseFloat(texte3);
//
//        System.out.println("Mon réel est : "+ nombre3);
//
//        // 4.4
//        String nomVille = " Le Puy-en-Velay ";
//        System.out.println("\nnomVille sans espace autour et en min : [" + nomVille.trim().toLowerCase() +"]");
//
//        // 4.5
//        String s1 = "String n°1";
//        String s2 = "String n°2";
//
//        System.out.print("Les deux chaînes commencent par le même caractère : ");
//        if(s1.charAt(0) == s2.charAt(0)){
//            System.out.println("TRUE");
//        } else {
//            System.out.println("FALSE");
//        }
//
//        // 4.6
//        String s21 = "abcd";
//        String s22 = "ABCD";
//
//        System.out.println("\ns21 = "+ s21);
//        System.out.println("s22 = "+ s22);
//
//        System.out.println("\n(s21 == s22) = "+ String.valueOf(s21 == s22).toUpperCase() );
//        System.out.println("(s21.equals(s22)) = "+ String.valueOf(s21.equals(s22)).toUpperCase() );
//        System.out.println("(s21.compareToIgnoreCase(s22)) = "+ String.valueOf(s21.compareToIgnoreCase(s22)).toUpperCase() );
//
//        // 4.7
//        String s31 = "Lorem ipsum dolor sit amet.";
//        String s32 = "Lorem";
//
//        System.out.println("\n31 = "+ s31);
//        System.out.println("s32 = "+ s32);
//
//        System.out.println("\ns31 commence par s32 = " + String.valueOf( s31.startsWith(s32) ).toUpperCase() );
//        System.out.println("s31 finit par s32 = " + String.valueOf( s31.endsWith(s32) ).toUpperCase() );
//        System.out.println("s31 contient s32 = " + String.valueOf( s31.contains(s32) ).toUpperCase() );
//
//        // 4.8
//        String s41 = "Lorem ipsum dolor sit amet.";
//        String s42 = "Lorem";
//
//        if(s41.contains(s42)){
//           s41 = s41.replace(s42, "");
//        }
//        System.out.println("s41 = " + s41);
//    }
//    /Exercice 4

//    Exercice 5
//    public static long factorielle1(int n){
//
//        long result = 1;
//
//        for(int i = 2; i <= n; i++){
//            result = result * i;
//        }
//
//        return result;
//    }
//
//    static BigInteger factorielle2(int n){
//
//        BigInteger result = BigInteger.valueOf(1);
//
//        for(int i = 2; i <= n; i++){
//            result = result.multiply( BigInteger.valueOf(i) );
//        }
//
//        return result;
//    }
//
//    public static void main(String[] args){
//
//        // 5.1
//        int i = 1;
//        while(factorielle1(i) / factorielle1(i - 1) == i ){
//            i++;
//        }
//
//        System.out.println("La factorielle est vérifiée jusqu'à : " + (i-1));
//
//        // 5.2
//        int i2 = 1;
//        while( factorielle2(i2).divide(factorielle2(i2 - 1)).equals( BigInteger.valueOf(i2) ) ){
//            i++;
//        }
//    }
//    /Exercice 5

//    Exercice 6
//    Voir la classe Calcul.java
//    /Exercice 6

//    Exercice 7
    public static void main(String[] args){
        int[] arr1 = {1, 42, 55, 90, 3992};
        int[] arr2 = {3, 57, 2, 33, 78};

        int[] arr3 = arrayMergeAndSort(arr1, arr2);

        for(int i = 0; i < arr3.length; i++){
            System.out.print("  " + arr3[i] + " ");
        }
    }

    public static int[] arrayMergeAndSort(int[] arr1, int[] arr2){

        int[] arr3 = new int[ arr1.length + arr2.length ];

        for (int i = 0; i < arr1.length ; i++){
            arr3[i] = arr1[i];
        }

        for (int i = 0; i < arr2.length; i++){
            arr3[i + arr1.length] = arr2[i];
        }

        Arrays.sort( arr3 );
        return arr3;
    }
//    /Exercice 7

    private static Object Calendar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
