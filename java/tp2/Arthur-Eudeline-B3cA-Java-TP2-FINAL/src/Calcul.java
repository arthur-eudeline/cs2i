public class Calcul {
    public static void main(String[] args){
        double val1 = Double.valueOf(args[0]);
        double val2 = Double.valueOf(args[2]);

        String operator = args[1].trim();

        double result = 0;

        if( operator.equals("+") ){
            result = val1 + val2;
        } else if( operator.equals("*") || operator.equals("x") ){
            result = val1 * val2;
        } else if ( operator.equals("-") ){
            result = val1 - val2;
        } else if ( operator.equals("^") ){
            result = Math.pow(val1, val2);
        } else if ( operator.equals("/") ) {
            result = val1 / val2;
        } else {
            System.out.println("Pas d'opérateur trouvé.");
        }

        System.out.println(args[0] + args[1] + args[2] + " = " + result);
    }
}
