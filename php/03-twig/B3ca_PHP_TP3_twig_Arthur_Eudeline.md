### Arthur Eudeline - B3ca

# TP PHP 03 : Introduction à Symfony



## 1.1 : Installation de Twig

![image-20181015155124185](assets/image-20181015155124185.png)



- Installer Composer en ajoutant le fichier `composer.phar` au PATH pour pouvoir l'utiliser plus simplement en ligne de commande. Sinon, pointer simplement vers le `composer.phar`.
- Se positionner à la racine du dossier du projet et exécuter la commande `composer require "twig/twig:^2.0"` qui va installer Twig.
- Inclure Twig dans les scripts PHP en passant par un `require_once ('vendor/autoload.php');`



## 1.2 : Les commandes de bases de Twig

### Les templates Twig

![image-20181015163455958](assets/image-20181015163455958.png)

Les templates Twig peuvent être affichés directement mais aussi être récupérés afin d'être traités en PHP par la suite :

#### index.php :

```php
<?php
require_once ('vendor/autoload.php');
// Dossier contenant les templates
$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader, array('cache' => false));

$content = $twig->render(
	"index.twig",
	array(
		'var1' => "Ma variable 1",
		'var2' => "MA VARIABLE 2"
	)
);
echo $content;

echo "<h1>var_dump du template twig stocké en variable :</h1>";
echo "<pre>";
var_dump($content);
echo "</pre>";
```

#### index.twig :

```html
<hr />
<h1>Début du template TWIG</h1>

<ul>
	<li>{{ var1 }}</li>
	<li>{{ var2 }}</li>
</ul>

<h1>Fin du template TWIG</h1>
<hr />
```



### Afficher des variables

- ####Affichage de variable simple

  ```twig
  {{ maVariable }}
  ```

- #### Affichage de contenu de tableau ou de propriété d'un objet

  ```twig
  {{ monObjet.prop }} {{ monTableau['index'] }}
  ```

- #### Concaténation de valeurs

  Avec Twig, la concaténation s'éffectue en utilisante le tilde `~` entre deux valeurs :

  ```twig
  {{ monObjet.prenom ~ ' ' ~ monObjet.nom }}
  ```

- #### Les filtres Twig

  Les filtres permettent d'altérer l'affichage de variables dynamiquement dans la vie. Ils font en réalité appel à des fonctions qui retournent un texte formaté selon les intructions effectées au sein de la fonction :

  ```twig
  {{ maDateISO }}
  {{ maDateISO | date('d/m/Y') }}
  ```



Ainsi, si on met tout cela en pratique on obtient cela :

- **index.php**

  ```php
  $monObjet = new stdClass();
  $monObjet->prenom = "John";
  $monObjet->nom = "Nom";
  
  echo $twig->render(
    "affichage-variables.twig",
    array(
      'maVariable'  => 'Lorem ipsum dolor sit amet',
  
      'monObjet' => $monObjet,
      'monTableau' => array( "contenu1", "contenu2" ),
  
      'maDateISO' => date('Y-m-d')
    )
  );
  ```

- **templates/affichage-variables.twig**

  ```twig
  <h1>Affichage des variables</h1>
  <ul>
    <li><b>Ma variable simple :</b> {{ maVariable }}</li>
  
    <li><b>Affichage d'une proriété d'un objet :</b> {{ monObjet.prenom }}</li>
  
    <li><b>Affichage d'un index d'un tableau :</b> {{ monTableau[0] }}</li>
  
    <li><b>Concaténation de deux valeurs :</b> {{ monObjet.prenom ~ " " ~ monObjet.nom }}</li>
  
    <li>Conversion du format d'une date avec un filtre : {{ maDateISO }} --> {{ maDateISO | date('d/m/Y') }}</li>
  </ul>
  ```

  Ce qui affiche

![image-20181022141038780](assets/image-20181022141038780.png)



### Structures de contrôle et expressions

- **index.php**

  ```php
  echo $twig->render(
    "boucles-conditions.twig",
    array(
      'tailleDemandee' => 'XXL',
      'taillesAuthorisees' => array("XS", "S", "M", "L", "XL", "XXL")
    )
  );
  ```

- **templates/boucles-conditions.twig**

  ```twig
  <h1>Boucles et conditions :</h1>
  
  <h2>Condition</h2>
  <!-- au lieu d'utiliser === pour la comparaison stricte il faut utiliser "is same as('{VALUE}')" -->
  {% if tailleDemandee is defined and tailleDemandee is same as("XXL") %}
  <p>Désolé, il ne me reste plus de t-shirt en XXL</p>
  {% else %}
  <p>Voici le t-shirt demdandé, en taille {{ tailleDemandee }} comme convenu.</p>
  {% endif %}
  
  <h2>Boucles</h2>
  <p>Tailles :</p>
  <ul>
  {% for taille in taillesAuthorisees %}
    <li>
      {{ taille }}
    </li>
  {% endfor %}
  </ul>
  ```

Ce qui affiche :

![image-20181022144303227](assets/image-20181022144303227.png)

