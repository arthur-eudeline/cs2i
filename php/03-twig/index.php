<?php
require_once ('vendor/autoload.php');
// Dossier contenant les templates
$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader, array('cache' => false));



$content = $twig->render(
  "index.twig",
  array(
    'var1' => "Ma variable 1",
    'var2' => "MA VARIABLE 2",
  )
);
echo $content;

echo "<h3>var_dump du template twig stocké en variable :</h3>";
echo "<pre>";
var_dump($content);
echo "</pre>";

// ------------------- Affichage des variables -------------------
$monObjet = new stdClass();
$monObjet->prenom = "John";
$monObjet->nom = "Nom";

echo $twig->render(
  "affichage-variables.twig",
  array(
    'maVariable'  => 'Lorem ipsum dolor sit amet',

    'monObjet' => $monObjet,
    'monTableau' => array( "contenu1", "contenu2" ),

    'maDateISO' => date('Y-m-d')
  )
);
// ------------------- /Affichage des variables -------------------

// ------------------- Boucles et conditions -------------------
echo $twig->render(
  "boucles-conditions.twig",
  array(
    'tailleDemandee' => 'XXL',
    'taillesAuthorisees' => array("XS", "S", "M", "L", "XL", "XXL")
  )
);
// ------------------- /Boucles et conditions -------------------
