<?php

require_once(__DIR__."/../lib/database.lib.php");

class Realisateur {
	# Déclaration des variables d'après la BDD
	private $id;
	private $nom;
	private $prenom;
	# Constructeur
	# On peut instancier un objet existant en passant son ID(new Realisateur(4))
	# Ou en créer un nouveau (sans passer de paramètre)
	public function __construct($id = '') {
		if ($id != '') {
			$this->id = $id;

			if($this->exist()){
				$this->load();
			}

		}
	}
	# Chargement depuis la BDD
	public function load() {
		if (isset($this->id)) {
			$db = Database::getInstance();
			$sql = 'SELECT id,
			nom,
			prenom
			FROM Realisateur
			WHERE id = "'.$this->id.'"';
			if ($result = $db->fetch($sql)) {

				$this->id = $result[0]["id"];
				$this->nom = $result[0]["nom"];
				$this->prenom = $result[0]["prenom"];

				return true;
			}
			return false;
		}
	}
	# Méthode surchargée pour l'affichage de l'objet: echo $obj;
	public function __toString() {
		return $this->prenom.' '.$this->nom;
	}

	public function set_nom($nom){
		$this->nom = $nom;
	}

	public function set_prenom($prenom){
		$this->prenom = $prenom;
	}

	public function get_id(){
		return $this->id;
	}

	public function get_nom(){
		return $this->nom;
	}

	public function get_prenom(){
		return $this->prenom;
	}

	public function exist(){
		$db = Database::getInstance();
		$sql = 'SELECT * FROM Realisateur WHERE id = "'.$this->id.'"';
		$results = $db->fetch($sql);

		if(count($results) > 0){
			return true;
		} else {
			return false;
		}
	}

	public static function get_all(){
		$db = Database::getInstance();
		$sql = 'SELECT * FROM Realisateur WHERE 1';

		$results = $db->fetch($sql);
		$realisateurs = array();

		if( count($results) > 0 ){
			foreach($results as $result){
				$new_realisateur = new Realisateur($result["id"]);

				if($new_realisateur->exist()){
					$new_realisateur->load();
					array_push($realisateurs, $new_realisateur);
				}

			}
		}

		return $realisateurs;
	}

	public static function add_new($data){
		$db = Database::getInstance();
		$requete = $db->get_db()->prepare(
			'INSERT INTO Realisateur
			(nom, prenom)
			VALUES (:nom, :prenom)'
		);

		$requete->execute($data);

		return $db->lastInsertId();
	}

}

?>
