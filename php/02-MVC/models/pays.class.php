<?php

require_once(__DIR__."/../lib/database.lib.php");

class Pays {

	/**
	 * ID en base du pays
	 *
	 * @var int
	 */
	private $id;

	/**
	 * Le nom du pays
	 *
	 * @var string
	 */
	private $nom;

	public function __construct($id = ''){
		if( !empty($id) ){
			$this->id = $id;

			if($this->exist()){
				$this->load();
			}
		}
	}

	public function load(){
		if( !empty($this->id) ){

			$db = Database::getInstance();
			$sql = 'SELECT id,
                           nom
                    FROM Pays
                    WHERE id = "'.$this->id.'"';
            if ($result = $db->fetch($sql)) {
				$this->set_nom($result[0]["nom"]);

				return true;
			}

			return false;
		}
	}

	public function set_nom($nom){
		$this->nom = $nom;
	}

	public function get_nom(){
		return $this->nom;
	}

	public function get_id(){
		return $this->id;
	}

	public function __toString(){
		return $this->nom;
	}

	public function exist(){
		$db = Database::getInstance();
		$sql = 'SELECT * FROM Pays WHERE id = "'.$this->id.'"';
		$results = $db->fetch($sql);

		if(count($results) > 0){
			return true;
		} else {
			return false;
		}
	}

	public static function get_all(){
		$db = Database::getInstance();
		$sql = 'SELECT * FROM Film WHERE 1';

		$results = $db->fetch($sql);
		$pays = array();

		if( count($results) > 0 ){
			foreach($results as $result){
				$new_pays = new Pays($result["id"]);
				if($new_pays->exist()){
					$new_pays->load();
					array_push($pays, $new_pays);
				}
			}
		}

		return $pays;
	}

	public static function add_new($data){
		$db = Database::getInstance();
		$requete = $db->get_db()->prepare(
			'INSERT INTO Pays
			(nom)
			VALUES (:nom)'
		);

		$requete->execute($data);

		return $db->lastInsertId();
	}

}

?>
