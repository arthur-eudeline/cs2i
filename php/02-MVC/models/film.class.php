<?php

require_once(__DIR__."/../lib/database.lib.php");
require_once(__DIR__."/../models/realisateur.class.php");
require_once(__DIR__."/../models/pays.class.php");

class Film {

	/**
	* ID en base du film
	*
	* @var int
	*/
	private $id;

	/**
	* Le titre du film
	*
	* @var string
	*/
	private $titre;

	/**
	* ID du réalisateur du film
	*
	* @var int
	*/
	private $realisateur_id;

	/**
	* Objet réalisateur
	*
	* @var OBJECT
	*/
	private $realisateur;

	/**
	* ID du pays du film
	*
	* @var int
	*/
	private $pays_id;

	/**
	* Objet pays du film
	*
	* @var OBJECT
	*/
	private $pays;

	/**
	* Année du film
	*
	* @var int
	*/
	private $annee;

	/**
	* Synopsis du film
	*
	* @var string
	*/
	private $synopsis;

	public function __construct($id = ''){
		if( !empty($id) ){
			$this->id = $id;

			if($this->exist()){
				$this->load();
			}
		}
	}

	public function load(){
		if( !empty($this->id) ){

			$db = Database::getInstance();
			$sql = 'SELECT id,
			titre,
			realisateur_id,
			pays_id,
			annee,
			synopsis
			FROM Film
			WHERE id = "'.$this->id.'"';
			if ($result = $db->fetch($sql)) {
				$this->set_titre($result[0]["titre"]);
				$this->set_realisateur_id($result[0]["realisateur_id"]);
				$this->set_realisateur(new Realisateur($this->realisateur_id));
				$this->set_pays_id($result[0]["pays_id"]);
				$this->set_pays(new Pays($result[0]["pays_id"]));
				$this->set_annee($result[0]["annee"]);
				$this->set_synopsis($result[0]["synopsis"]);

				return true;
			}

			return false;
		}
	}

	public static function get_all(){
		$db = Database::getInstance();
		$sql = 'SELECT * FROM Film WHERE 1';

		$results = $db->fetch($sql);
		$films = array();

		if( count($results) > 0 ){
			foreach($results as $result){
				$new_film = new Film($result["id"]);
				if($new_film->exist()){
					$new_film->load();
					array_push($films, $new_film);
				}

			}
		}

		return $films;
	}

	public function set_titre($titre){
		$this->titre = $titre;
	}

	public function set_realisateur_id($realisateur_id){
		$this->realisateur_id = $realisateur_id;
	}

	public function set_realisateur($realisateur){
		$this->realisateur = $realisateur;
	}

	public function set_pays_id($pays_id){
		$this->pays_id = $pays_id;
	}

	public function set_pays($pays){
		$this->pays = $pays;
	}

	public function set_annee($annee){
		$this->annee = $annee;
	}

	public function set_synopsis($synopsis){
		$this->synopsis = $synopsis;
	}

	public function get_titre(){
		return $this->titre;
	}

	public function get_realisateur_id(){
		return $this->realisateur_id;
	}

	public function get_realisateur(){
		return $this->realisateur;
	}

	public function get_pays_id(){
		return $this->pays_id;
	}

	public function get_pays(){
		return $this->pays;
	}

	public function get_annee(){
		return $this->annee;
	}

	public function get_synopsis(){
		return $this->synopsis;
	}

	public function dump(){
		echo '<pre>';
		var_dump($this);
		echo'</pre>';
	}

	public function __toString(){
		ob_start();
		?>

		<hr />
		<h2><?php echo $this->titre; ?></h2>
		<p>
			Film réalisé par <b><?php echo $this->realisateur; ?></b> et sortit en <?php echo $this->annee; ?>.
		</p>
		<p>
			<em><?php echo $this->synopsis; ?></em>
		</p>
		<hr />

		<?php
		$output = ob_get_clean();
		return $output;
	}

	public function exist(){
		$db = Database::getInstance();
		$sql = 'SELECT * FROM Film WHERE id = "'.$this->id.'"';
		$results = $db->fetch($sql);

		if(count($results) > 0){
			return true;
		} else {
			return false;
		}
	}

	public static function add_new($data){
		$db = Database::getInstance();
		$requete = $db->get_db()->prepare(
			'INSERT INTO Film
			(titre, realisateur_id, pays_id, annee, synopsis)
			VALUES (:titre, :realisateur_id, :pays_id, :annee, :synopsis)'
		);

		$requete->execute($data);

		return $db->lastInsertId();
	}

	public static function search($search_params){
		$db = Database::getInstance();

		$sql = 'SELECT * FROM Film';

		$sql_search = '';

		if(isset($search_params["titre"]) && !empty($search_params["titre"])){
			$sql_search .= (empty($sql_search)) ? ' WHERE' : ' AND';
			$sql_search .= ' titre LIKE "%'. $search_params["titre"] .'%"';
		}

		if(isset($search_params["realisateurs"]) && !empty($search_params["realisateurs"])){
			$sql_search .= (empty($sql_search)) ? ' WHERE' : ' AND';
			$sql_search .= ' realisateur_id IN ('. implode(",", $search_params["realisateurs"]) .')';
		}

		if(isset($search_params["pays"]) && !empty($search_params["pays"])){
			$sql_search .= (empty($sql_search)) ? ' WHERE' : ' AND';
			$sql_search .= ' pays_id IN ('. implode(",", $search_params["pays"]) .')';
		}

		$sql = $sql.$sql_search;

		$results = $db->fetch($sql);
		$films = array();

		if( count($results) > 0 ){
			foreach($results as $result){
				$new_film = new Film($result["id"]);
				if($new_film->exist()){
					$new_film->load();
					array_push($films, $new_film);
				}

			}
		}

		return $films;
	}
}

?>
