<?php
require_once(__DIR__."/top.php");


if(isset($_GET["action"]) && $_GET["action"] === "add-film" && isset($_POST["film-data"]) && !empty($_POST["film-data"])){
	if($_POST["realisateur_new"] === 'true'){
		$new_realisateur_id = Realisateur::add_new($_POST["real-data"]);
		$_POST["film-data"]["realisateur_id"] = $new_realisateur_id;
	}

	if($_POST["pays_new"] === 'true'){
		$new_pays_id = Pays::add_new($_POST["pays-data"]);
		$_POST["film-data"]["pays_id"] = $new_pays_id;
	}

	Film::add_new($_POST["film-data"]);
	header("Location: ". URL_PATH ); // redirection à Home pour éviter de pouvoir resoumettre le formulaire
}

?>
