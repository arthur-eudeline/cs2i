<?php

/*
 * Database
 */
define('DB_HOST', 'localhost:8889');		// Le serveur MySQL de votre BDD : à modifier
define('DB_NAME', 'cs2i_php_mvc_tp1');		// Votre base de données : à modifier
define('DB_USER', 'root');		// Votre login : à modifier
define('DB_PWD', 'root');		// Votre mot de passe : à modifier

/*
 * Paths
 */
define('URL_PATH', 'http://localhost:8888/cs2i/php-cabret/tp-mvc/');				// Path de l'application : à modifier
define('STATIC_PATH', URL_PATH . 'static/');		// Path des médias
?>
