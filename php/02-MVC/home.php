<?php

require_once(__DIR__."/top.php");

if(isset($_GET["search-data"]) && !empty($_GET["search-data"])){
	$films = Film::search($_GET["search-data"]);
} else {
	$films = Film::get_all();
}
$active_view = "home";

require_once(__DIR__."/templates/header.template.php");

$realisateurs_array = Realisateur::get_all();
$pays_array = Pays::get_all();
require_once(__DIR__."/templates/search.template.php");

require_once(__DIR__."/templates/list-films.template.php");

require_once(__DIR__."/templates/footer.template.php");

?>
