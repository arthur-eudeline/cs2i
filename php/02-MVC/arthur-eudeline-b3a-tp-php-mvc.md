### Arthur EUDELINE - B3A

# PHP : MVC 

## 1.2.2 : complétion de la méthode `load()`

Dans un premier temps, il faut charger la classe **database** qui est utilisée dans la classe réalisateur :

```php
require_once(__DIR__."/../lib/database.lib.php");
```

Puis nous pouvons implémenter la méthode `load()` :

```php
public function load() {
  if (isset($this->id)) {
     $db = Database::getInstance();
     $sql = 'SELECT id,
                    nom,
                    prenom
             FROM Realisateur
             WHERE id = "'.$this->id.'"';
     if ($result = $db->fetch($sql)) {

        $this->id = $result[0]["id"];
        $this->nom = $result[0]["nom"];
        $this->prenom = $result[0]["prenom"];

        return true;
     }
     return false;
  }
   
```

La création des classes : 

```php
require_once(__DIR__."/../lib/database.lib.php");

class Pays {

	/**
	 * ID en base du pays
	 *
	 * @var int
	 */
	private $id;

	/**
	 * Le nom du pays
	 *
	 * @var string
	 */
	private $nom;

	public function __construct($id = ''){
		if( !empty($id) ){
			$this->id = $id;
			$this->load();
		}
	}

	public function load(){
		if( !empty($this->id) ){

			$db = Database::getInstance();
			$sql = 'SELECT id,
                           nom
                    FROM Pays
                    WHERE id = "'.$this->id.'"';
            if ($result = $db->fetch($sql)) {
				$this->set_nom($result[0]["nom"]);

				return true;
			}

			return false;
		}
	}

	public function set_nom($nom){
		$this->nom = $nom;
	}

	public function get_nom(){
		return $this->nom;
	}
    
    public function __toString(){
		return $this->nom;
	}
}
```



## 1.3 : Test des classes

```php
$film = new Film(1);
echo $film;
echo $film->get_annee() . "<br />";

$realisateur = new Realisateur(1);
echo $realisateur . "<br />";

echo $film->get_pays() . "<br />";
echo $film->get_realisateur() . "<br />";
```

Ce qui affiche :

![Capture d'écran 2018-10-08 à 16.28.00](./assets/output1.png)



### Méthode `get_all()` de la classe `Film`

```php
public static function get_all(){
    $db = Database::getInstance();
    $sql = 'SELECT * FROM Film WHERE 1';

    $results = $db->fetch($sql);
    $films = array();

    if( count($results) > 0 ){
        foreach($results as $result){
            $new_film = new Film($result["id"]);
            $new_film->load();

            array_push($films, $new_film);
        }
    }

    return $films;
}
```



## 1.4.1 et 1.4.2 : Les templates et la stylisation CSS

Pour inclure les templates à notre architecture, nous devons dans un premier temps remanier le fichier `index.php` comme ceci :

```php
require_once(__DIR__."/top.php");

$films = Film::get_all();

require_once(__DIR__."/templates/header.template.php");

require_once(__DIR__."/templates/list-films.template.php");

require_once(__DIR__."/templates/footer.template.php");
```

En suite, nous pouvons nous focaliser sur le template de la liste des films `./templates/list-films.template.php` qui se base sur l'utilisation de du thème material design de BootStrap 3 :

```php+HTML
<div class="container">
	<div class="row">

		<?php

		if(isset($films) && !empty($films) && is_array($films)):
			echo '<div class="card-deck">';

			foreach($films as $film):
				?>

				<div class="card">

					<div class="card-body">
						<h2 class="card-title"><?php echo $film->get_titre(); ?></h2>

						<div class="film-infos card-subtitle mb-2 text-muted">
							<p>
								<span class="date">
									Sortit en <?php echo $film->get_annee(); ?>
								</span>

								<span class="realisateur">Réalisé par <b><?php echo $film->get_realisateur(); ?></b></span>
							</p>
						</div>

						<div class="film-synopsis card-text">
							<p>
								<?php echo $film->get_synopsis(); ?>
							</p>
						</div>
					</div>

				</div>

				<?php
			endforeach;

			echo '</div>';
		endif;

		?>
	</div>
</div>
```

Ce qui donne :

![output2](./assets/output2.png)



## 1.4.3 : Création de la méthode `exist()` :

Si on cherche à l'implémenter sur la classe film, il suffit de vérifier d'abord si la requête sql renvoie un tableau vide ou non. De cette manière on peu savoir si la requête pour un id donné renvoi un film existant ou non :

```php
public function exist(){
    $db = Database::getInstance();
    $sql = 'SELECT * FROM Film WHERE id = "'.$this->id.'"';
    $results = $db->fetch($sql);

    if(count($results) > 0){
        return true;
    } else {
        return false;
    }
}
```



## 1.4.4 : Page d'ajout de films 

![localhost_8888_cs2i_php-cabret_tp-mvc_add-film.php(iPhone 6_7_8) (1)](./assets/localhost_8888_cs2i_php-cabret_tp-mvc_add-film.php(iPhone 6_7_8) (1).png)

L'ajout du film passe ensuite par un traitement dans le fichier `form-action.php` appelé dans l'attribut `action` de la balise `<form>` :

```php
if(isset($_GET["action"]) && $_GET["action"] === "add-film" && isset($_POST["film-data"]) && !empty($_POST["film-data"])){
	if($_POST["realisateur_new"] === 'true'){
		$new_realisateur_id = Realisateur::add_new($_POST["real-data"]);
		$_POST["film-data"]["realisateur_id"] = $new_realisateur_id;
	}

	if($_POST["pays_new"] === 'true'){
		$new_pays_id = Pays::add_new($_POST["pays-data"]);
		$_POST["film-data"]["pays_id"] = $new_pays_id;
	}

	Film::add_new($_POST["film-data"]);
	header("Location: ". URL_PATH ); // redirection à Home pour éviter de pouvoir resoumettre le formulaire
}
```

L'ajout en base pase par une méthode statique `add_new($data)` présent sur chaque classe, comme le montre l'exemple de la classe film :

```php
public static function add_new($data){
    $db = Database::getInstance();
    //get_db() return l'instance PDO
    $requete = $db->get_db()->prepare(
        'INSERT INTO Film
        (titre, realisateur_id, pays_id, annee, synopsis)
        VALUES (:titre, :realisateur_id, :pays_id, :annee, :synopsis)'
    );

    $requete->execute($data);

    return $db->lastInsertId();
}
```

## 1.4.5 et 1.4.6 : formulaire de recherche de films :

![Capture d'écran 2018-10-09 à 22.04.14](./assets/output03.png)

La recherche est basée sur la méthode statique `Film::search($data)` qui permet d'opérer une recherche en fonction des paramètres contenus dans `$_GET` :

```php
public static function search($search_params){
    $db = Database::getInstance();

    $sql = 'SELECT * FROM Film';

    $sql_search = '';

    if(isset($search_params["titre"]) && !empty($search_params["titre"])){
        $sql_search .= (empty($sql_search)) ? ' WHERE' : ' AND';
        $sql_search .= ' titre LIKE "%'. $search_params["titre"] .'%"';
    }

    if(isset($search_params["realisateurs"]) && !empty($search_params["realisateurs"])){
        $sql_search .= (empty($sql_search)) ? ' WHERE' : ' AND';
        $sql_search .= ' realisateur_id IN ('. implode(",", $search_params["realisateurs"]) .')';
    }

    if(isset($search_params["pays"]) && !empty($search_params["pays"])){
        $sql_search .= (empty($sql_search)) ? ' WHERE' : ' AND';
        $sql_search .= ' pays_id IN ('. implode(",", $search_params["pays"]) .')';
    }

    $sql = $sql.$sql_search;

    $results = $db->fetch($sql);
    $films = array();

    if( count($results) > 0 ){
        foreach($results as $result){
            $new_film = new Film($result["id"]);
            if($new_film->exist()){
                $new_film->load();
                array_push($films, $new_film);
            }

        }
    }

    return $films;
}
```

