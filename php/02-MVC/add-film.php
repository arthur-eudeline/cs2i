<?php

require_once(__DIR__."/top.php");
$active_view = "add-film";

$realisateurs_array = Realisateur::get_all();
$pays_array = Pays::get_all();

require_once(__DIR__."/templates/header.template.php");

require_once(__DIR__."/templates/add-film-form.template.php");

require_once(__DIR__."/templates/footer.template.php");

?>
