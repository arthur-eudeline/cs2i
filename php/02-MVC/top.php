<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('memory_limit', '5G'); // or you could use 1G
error_reporting(E_ALL);

/* Fichiers de configuration */
require_once('conf/settings.php');		// Configuration de l'acc�s � la base de donn�es

/* Librairies */
require_once('lib/database.lib.php');	// Classe de la base de donn�es

/* Classes */
require_once('models/realisateur.class.php');
require_once('models/film.class.php');
require_once('models/pays.class.php');
?>
