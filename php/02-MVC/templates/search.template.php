<div class="container">
	<div class="row">

		<div class="card" style="width: 100%; margin:15px 0">
			<div class="card-body">
				<h2 class="card-title">Rechercher un film</h2>

				<form action="<?php echo URL_PATH ?>home.php">
					<div class="row" style="margin: 0 -30px">

						<div class="form-group col">
							<div class="dropdown">

								<button class="btn dropdown-toggle" type="button" id="ex1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons" style="vertical-align: middle; margin-top: -2px">account_circle</i> Filtrer par réalisateur
								</button>
								<div class="dropdown-menu dropdown-menu-left" aria-labelledby="ex1">
									<?php foreach($realisateurs_array as $realisateur): ?>
										<div class="dropdown-item">

											<div class="checkbox">
												<label>
													<input type="checkbox" value="<?php echo $realisateur->get_id() ?>" name="search-data[realisateurs][]" <?php echo (isset($_GET["search-data"]["realisateurs"]) && in_array(strval($realisateur->get_id()), $_GET["search-data"]["realisateurs"]) ) ? 'checked' : '' ;?>>
													<?php echo $realisateur; ?>
												</label>
											</div>

										</div>
									<?php endforeach; ?>
								</div>

							</div>
						</div>

						<div class="form-group col">
							<div class="dropdown">

								<button class="btn dropdown-toggle" type="button" id="ex1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="material-icons" style="vertical-align: middle; margin-top: -2px">language</i> Filtrer par pays
								</button>
								<div class="dropdown-menu dropdown-menu-left" aria-labelledby="ex1">
									<?php foreach($pays_array as $pays): ?>
										<div class="dropdown-item">

											<div class="checkbox">
												<label>
													<input type="checkbox" value="<?php echo $pays->get_id() ?>" name="search-data[pays][]" <?php echo (isset($_GET["search-data"]["pays"]) && in_array(strval($pays->get_id()), $_GET["search-data"]["pays"]) ) ? 'checked' : '' ;?>> 
													<?php echo $pays; ?>
												</label>
											</div>

										</div>
									<?php endforeach; ?>
								</div>

							</div>
						</div>

					</div>

					<div class="form-group">
						<label class="bmd-label-floating full-width" for="search-titre">
							Rechercher par titre
						</label>
						<input id="search-titre" type="text" class="form-control" name="search-data[titre]" value="<?php echo (isset($_GET["search-data"]["titre"])) ? $_GET["search-data"]["titre"] : ""; ?>">
					</div>

					<div class="text-right">
						<?php if(isset($_GET["search-data"])){ ?>
							<a href="<?php echo URL_PATH ?>index.php" class="btn btn-danger">Effacer la recherche</a>
						<?php } ?>
						<button type="reset" class="btn btn-secondary">Réinitiliser la recherche</button>
						<button type="submit" class="btn btn-primary">Rechercher</button>
					</div>
				</form>

			</div>
		</div>

	</div>
</div>
