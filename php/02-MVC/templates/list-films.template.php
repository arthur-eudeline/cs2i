<div class="container" style="padding-bottom: 50px;">
	<div class="row">

		<div class="card-deck">

			<?php

			if(isset($films) && !empty($films) && is_array($films)):
				foreach($films as $film):
					?>

					<div class="card">

						<div class="card-body">
							<h2 class="card-title"><?php echo $film->get_titre(); ?></h2>

							<div class="film-infos card-subtitle mb-2 text-muted">
								<p>
									<span class="date">
										Sortit en <?php echo $film->get_annee(); ?>
									</span>

									<span class="realisateur">Réalisé par <b><?php echo $film->get_realisateur(); ?></b></span>

									<span class="pays">| <?php echo $film->get_pays(); ?></span>
								</p>
							</div>

							<div class="film-synopsis card-text">
								<p>
									<?php echo $film->get_synopsis(); ?>
								</p>
							</div>
						</div>

					</div>

					<?php
				endforeach;

			endif;

			?>

		</div>
	</div>
</div>
