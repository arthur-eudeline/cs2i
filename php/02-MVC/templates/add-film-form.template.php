<div class="container">
	<div class="row">

		<h1 class="col-12" style="margin-bottom: 80px;">Ajout d'un film</h1>

		<form class="col-12" method="post" action="<?php echo URL_PATH; ?>form-action.php?action=add-film">
			<!-- Titre film -->
			<div class="card" style="margin-bottom: 30px;">
				<div class="card-body">

					<div class="form-group">
						<label class="bmd-label-floating full-width" for="film-titre">
							Titre du film
						</label>
						<input id="film-titre" type="text" class="form-control" name="film-data[titre]">
					</div>

				</div>
			</div>

			<!-- Réalisateur -->
			<div class="card" style="margin-bottom: 30px;">
				<div class="card-body">
					<div class="row">

						<!-- Séléctionner un réalisateur pre-existant -->
						<div class="col">
							<div class="radio">
								<label>
									<input type="radio" name="realisateur_new" value="false" checked>
									Utiliser un réalisateur pré-existant
								</label>
							</div>

							<select class="custom-select" name="film-data[realisateur_id]">
								<option selected disabled>Réalisateur</option>

								<?php foreach($realisateurs_array as $realisateur): ?>
									<option value="<?php echo $realisateur->get_id() ?>"><?php echo $realisateur; ?></option>
								<?php endforeach; ?>
							</select>
						</div>

						<!-- Créer un nouveau réalisateur -->
						<div class="col">
							<div class="radio">
								<label>
									<input type="radio" name="realisateur_new" value="true">
									Ajouter un nouveau réalisateur
								</label>
							</div>

							<div class="form-group">
								<label class="bmd-label-floating full-width" for="real-prenom">
									Prénom
								</label>
								<input id="real-prenom" type="text" class="form-control" name="real-data[prenom]">
							</div>

							<div class="form-group">
								<label class="bmd-label-floating full-width" for="real-nom">
									Nom
								</label>
								<input id="real-nom" type="text" class="form-control" name="real-data[nom]">
							</div>
						</div>

					</div>
				</div>
			</div>

			<!-- Pays -->
			<div class="card" style="margin-bottom: 30px;">
				<div class="card-body">
					<div class="row" style="padding:15px 0">

						<!-- Séléctionner un pays pre-existant -->
						<div class="col">
							<div class="radio">
								<label>
									<input type="radio" name="pays_new" value="false" checked>
									Utiliser un pays pré-existant
								</label>
							</div>

							<select class="custom-select" name="film-data[pays_id]">
								<option selected disabled>Pays</option>

								<?php foreach($pays_array as $pays): ?>
									<option value="<?php echo $pays->get_id() ?>"><?php echo $pays; ?></option>
								<?php endforeach; ?>
							</select>
						</div>

						<!-- Créer un nouveau pays -->
						<div class="col">
							<div class="radio">
								<label>
									<input type="radio" name="pays_new" value="true">
									Ajouter un nouveau pays
								</label>
							</div>

							<div class="form-group">
								<label class="bmd-label-floating full-width" for="pays-nom">
									Nom
								</label>
								<input id="pays-nom" type="text" class="form-control" name="pays-data[nom]">
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Année de parution -->
			<div class="card" style="margin-bottom: 30px;">
				<div class="card-body">

					<div class="form-group">
						<label for="film-annee" class="bmd-label-floating full-width">
							Année de parution
						</label>
						<input id="film-annee" type="number" class="form-control" name="film-data[annee]">
					</div>

				</div>
			</div>

			<!-- Synopsis -->
			<div class="card" style="margin-bottom: 30px;">
				<div class="card-body">

					<div class="form-group">
						<label class="bmd-label-floating full-width" for="film-syno">
							Synopsis
						</label>
						<textarea id="film-syno" class="form-control" name="film-data[synopsis]"></textarea>
					</div>

				</div>
			</div>

			<div class="row text-right">
				<div class="col">
					<button type="reset" class="btn btn-default">Vider les champs</button>

					<button type="submit" class="btn btn-primary btn-raised">Ajouter le film</button>
				</div>
			</div>
		</form>

	</div>
</div>
