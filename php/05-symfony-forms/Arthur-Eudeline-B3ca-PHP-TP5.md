### Arthur Eudeline - B3ca

# PHP - Symfony : TP05



## 3.1 : Aprendre à créer des fomulaires avec l'outil Symfony Form :



### Création d'un formulaire simple :

Si on reprend les fichiers de l'exemple donné sur la documentation Symfony avec les fichiers [src/entity/task.php](https://symfony.com/doc/current/forms.html#creating-a-simple-form), [src/Controller/DefaultController.php](https://symfony.com/doc/current/forms.html#building-the-form) et le template [templates/default/new.html.twig](https://symfony.com/doc/current/forms.html#rendering-the-form) il nous suffit tout simplement d'attribuer une route au `DefaultController` précédemment créé pour afficher le formulaire :

```php
<?php
	// src/controller/DefaultController.php
  
  // [...]
  
  // Importation du système d'annotation pour déclarer la route
  use Symfony\Component\Routing\Annotation\Route;
  
	class DefaultController extends AbstractController
	{
      /**
       * Déclaration de la route
       *
       * @Route("/task/new")
       */
      public function new(Request $request)
      {
       	// [...] 
      }
?>
```

Ainsi, si on se rend sur l'URL http://localhost:8000/task/new, on obtient bien le rendu du formulaire :

![image-20181119142948816](assets/image-20181119142948816.png)



### [Gérer la soumission du formulaire](https://symfony.com/doc/current/forms.html#handling-form-submissions)

Précédemment, notre **formulaire était rendu quoi qu'il arrive**. Cependant, on remarque que le contrôle récupère un objet `Request $request`. 

```php
<?php 
  
  	public function new(Request $request)
    {
        // creates a task and gives it some dummy data for this example
        $task = new Task();
        $task->setTask('Write a blog post');
        $task->setDueDate(new \DateTime('tomorrow'));

        $form = $this->createFormBuilder($task)
            ->add('task', TextType::class)
            ->add('dueDate', DateType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Task'))
            ->getForm();

        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }
  
?>
```

```php
<?php
  
    public function new(Request $request)
    {
  			$success = false;			
  
        // creates a task and gives it some dummy data for this example
        $task = new Task();
        $task->setTask('Write a blog post');
        $task->setDueDate(new \DateTime('tomorrow'));

        $form = $this->createFormBuilder($task)
            ->add('task', TextType::class)
            ->add('dueDate', DateType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Task'))
            ->getForm();

  			// Modification : 
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();

          	// On attribue une variable succès pour afficher un message
            $sucess = true;
        }

        return $this->render('default/new.html.twig', array(
            'form' 		=> $form->createView(),
          	'sucess'	=> $sucess
        ));
  
    }
  
 ?>
```

Dans notre `templates/new.html.twig`, on peut alors conditionner l'affichage d'un message :

```html
{% if success == true %}
<h1>Formulaire correctement soumis</h1>
{% endif %}

{{ form_start(form) }}
{{ form_widget(form) }}
{{ form_end(form) }}

```

ce qui affiche à la soumission :

![image-20181119150553183](assets/image-20181119150553183.png)



### [Validation des formulaires](https://symfony.com/doc/current/forms.html#form-validation)

Après avoir inclu `symfony/validator` via Composer, il nous est possible de tester la contenu de certains champs lors de la soumission d'un formulaire :

```php
<?php
  // src/entity/task.php
  namespace App\Entity;

  // Importation de Symfony Validator
  use Symfony\Component\Validator\Constraints as Assert;

  class Task
  {

    /**
    * @Assert\NotBlank
    */
    protected $task;

    /**
    * @Assert\NotBlank
    * @Assert\Type("\DateTime")
    */ 
    protected $dueDate;
    
    // [...]
    
  }
?>
```

Puis dans notre template Twig :

```twig
{{ form_start(form, {'attr': {'novalidate': 'novalidate'}}) }}
{{ form_widget(form) }}
{{ form_end(form) }}
```

Afin de le tester, retournons sur la page de soumission du formulaire : http://loalhost:8000/task/new. Via l'inspecteur, on vide la `valeur=""` d'une des `<option>` de la valeur du `<select>` , `option` que nous appellons ici "*valeur vide*" :

```html
<!-- modifications apportées depuis l'inspecteur du navigateur -->
<select id="form_dueDate_month" name="form[dueDate][month]">
  <option value="">Valeur vide</option>
  <!-- [...] -->
</select>
```

![image-20181119152817774](assets/image-20181119152817774.png)

À présent, si on essaie de soumettre le formulaire, on obtient l'érreur suivante :

![image-20181119152958311](assets/image-20181119152958311.png)



### Type de champs intégrés nativement avec Symfony

Pour ajouter un champ de type `range` appelé `priority` un champ de type `color` appelé `color`, il faut tout d'abord ajouter les champs à notre entité `Task` :

```php
<?php
// src/entity/task.php
  
namespace App\Entity;

// Importation de Symfony Validator
use Symfony\Component\Validator\Constraints as Assert;

class Task
{

  /**
  * @Assert\NotBlank
  */
  protected $task;

  /**
  * @Assert\NotBlank
  * @Assert\Type("\DateTime")
  */
  protected $dueDate;

  // Type public pour éviter d'avoir à faire les getter et les setter et aller plus vite
  public $color;
  public $priority;
  
  // [...]
}
?>
```

Puis, dans notre contrôlleur, il faut étoffer le formulaire déjà existant avec les nouveaux champs :

```php
<?php
  // src/controller/DefaultController.php
  
  // [...]
  
  // Importation des nouveaux types de champs dont on a besoin
  use Symfony\Component\Form\Extension\Core\Type\RangeType;
	use Symfony\Component\Form\Extension\Core\Type\ColorType;

class DefaultController extends AbstractController
{
  /**
  * @Route("/task/new")
  */
  public function new(Request $request)
  {

		$form = $this->createFormBuilder($task)
    ->add('task', TextType::class)
    ->add('dueDate', DateType::class)
      
    // Ajout des champs personnalisés
    ->add('color', ColorType::class)
    ->add('priority', RangeType::class, array(
        'attr' => array(
            'min' => 1,
            'max' => 4
        )
    ))
      
    ->add('save', SubmitType::class, array('label' => 'Create Task'))
    ->getForm();
    
    // [...]
    
  }
}
```

![image-20181119154657547](assets/image-20181119154657547.png)



##4.1: Apprendre comment Symfony facilite la gestion de base de données avec l’ORM Doctrine

### Création de la base de données :

dans notre fichier `.env` on doit commencer par modifier les lignes suivantes :

```text
### [...]

###> doctrine/doctrine-bundle ###
# Format described at http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html#connecting-using-a-url
# For an SQLite database, use: "sqlite:///%kernel.project_dir%/var/data.db"
# Configure your db driver and server_version in config/packages/doctrine.yaml
DATABASE_URL=__DB_TYPE__://__DB_USERNAME__:__DB_PASSWORD__@__DB_HOST__:__DB_HOST_PORT__/__DB_NAME__
###< doctrine/doctrine-bundle ###

### [...]
```

Après avoir remplacé les champs `__FIELD_NAME__` dans le code ci-dessus, on peu éxcuter la commande `php bin/console doctrine:database:create`, qui va automatiquement créer la base de données :

![image-20181119160846851](assets/image-20181119160846851.png)



### [Créer une classe entité](https://symfony.com/doc/current/doctrine.html#creating-an-entity-class)

Création d'une entité `monEntite` avec les champs :

- `string(255) nomEntite required`
- `string(500) descEntite`

![image-20181119161348030](assets/image-20181119161348030.png)

Ainsi, la commande génère un fichier `src/Entity/MonEntite.php` :

```php
<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MonEntiteRepository")
 */
class MonEntite
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomEntite;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $descEntite;
  
  	// [...]
}
?>
```



Cependant, aucun changement n'est encore opéré en base de données. Pour ce faire, il faut effectuer la migration :

```text
php bin/console make:migration
```

Cette commande créer un fichier PHP qui contient les instructions de création des mofications à apporter à notre base de données qui n'est toujours pas modifiée :

```php
$this->addSql('CREATE TABLE mon_entite (id INT AUTO_INCREMENT NOT NULL, nom_entite VARCHAR(255) NOT NULL, desc_entite VARCHAR(500) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
```

Pour appliquer les modifications on doit éxecuter la dernière commande : 

```text
php bin/console doctrine:migrations:migrate
```

![image-20181119162516667](assets/image-20181119162516667.png)

Cette fois, les modifications sont bien prises en comptes et si on consulte notre base, qui juste à présent était vide, a maintenant une table :

![image-20181119162649189](assets/image-20181119162649189.png)



### [Sauvegarder les objets en base de données](https://symfony.com/doc/current/doctrine.html#persisting-objects-to-the-database) :

```
php bin/console make:controller monEntiteController
```

Cette commande génère à elle seule un controlleur basique et un template automatiquement associé. Dans notre contrôlleur, on peu ajouter une nouvelle vue qui affichera un formulaire d'ajout d'entité : 

```php
 /**
  * @Route("/mon-entite/new")
  */
  public function new(Request $request){
    $success = false;
    $entityManager = $this->getDoctrine()->getManager();
    $monEntite = new monEntite();

    $form = $this->createFormBuilder( $monEntite )
    ->add('nomEntite', TextType::class)
    ->add('descEntite', TextType::class)
    ->add('save', SubmitType::class, array('label' => 'Create Task'))
    ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
			// Enregistrement
      $monEntite = $form->getData();
      $entityManager->persist($monEntite);
      $entityManager->flush();

      $success = true;
    }

    return $this->render('mon_entite/new.html.twig', [
      "form"    => $form->createView(),
      "sucess"  => $success
    ]);
  }
```

et dans notre template :

```twig
{% if success == true %}
	<h1>Entité créée</h1>
{% else %}
	</h1>Création d'entité</h1>
  {{ form_start(form) }}
  {{ form_widget(form) }}
  {{ form_end(form) }}
{% endif %}
```

![image-20181119183304033](assets/image-20181119183304033.png)



### Extraire des données depuis la base de données

Imaginons que sur l'URL http://localhost:8000/mon-entite/ nous voulions lister toutes les entités, il suffit de modifier le contrôleur comme ceci :

```php
	/**
  * @Route("/mon-entite", name="mon_entite")
  */
  public function index()
  {
    // Affichage de toutes les entités
    $repository = $this->getDoctrine()->getRepository(MonEntite::class);
    $entities = $repository->findAll();

    return $this->render('mon_entite/index.html.twig', [
      'controller_name' => 'MonEntiteController',
      'entities'        => $entities
    ]);
  }
```

Puis notre template affiche le tout via une boucle `for` :

```twig
<h1>Liste d'entités</h1>
{% for entity in entities %}
  <div>
    <h2>{{ entity.nomEntite }}</h2>
    <p>
      {{ entity.descEntite }}
    </p>
  </div>
{% endfor %}
```

Ce qui au final affiche ceci :

![image-20181119184235616](assets/image-20181119184235616.png)



### Manipulation des données :

#### Modification

```php
// On récupèrel le gestionnaire d'entités	
$entityManager = $this->getDoctrine()->getManager();

// Récupère une instance de MonEntite en fonction de son ID
$entite = $entityManager->getRepository(MonEntite::class)->find(1);

// On défini un nouveau nom via le setter défini dans la classe MonEntite grâce à doctrine
$entite->setNomEntite('Mon nouveau nom');

// On enregistre les changements en base de données
$entityManager->flush();
```

![image-20181119184810791](assets/image-20181119184810791.png)

#### Suppression

```php
// On récupèrel le gestionnaire d'entités	
$entityManager = $this->getDoctrine()->getManager();

// Récupère une instance de MonEntite en fonction de son ID
$entite = $entityManager->getRepository(MonEntite::class)->find(1);

// On supprime l'entité en donnant directement son instance
$entityManager->remove($entite);

// On enregistre les modifications en base de données
$entityManager->flush();
```



