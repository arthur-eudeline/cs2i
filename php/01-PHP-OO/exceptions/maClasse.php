<?php

class maClass {

	public function trigg_execption(int $val){

		try {
			if($val >= 0 && $val <= 100){
				echo "<h1 style='color: green'>Valide</h1>";
			} else {
				throw new Exception("La valeur saisie doit être comprise entre 0 et 100.");
			}
		} catch (Exception $e){
			echo "<h1 style='color: red'>Exception :</h1>";
			echo "<p>". $e->getMessage() ."</p>";
		} finally {
			echo "<p>{block finally}</p>";
		}
	}

}

?>
