<?php

class customException extends Exception {
  public function errorMessage() {
    $errorMsg = '<p>Erreur à la ligne '.$this->getLine().' dans <em>'.
    $this->getFile().'</em> :<br /> <b>'.$this->getMessage().
    '</b> n’est pas une adresse E-Mail valide.</p>';
    return $errorMsg;
	}
}

$email = "email@mail";
try {
  if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
    throw new customException($email);
  }
}
catch (customException $e) {
  echo $e->errorMessage();
}

?>
