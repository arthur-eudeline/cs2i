<?php

class Chien {

	public function __construct($name, $nb_puces){
		if(is_null($name) || !isset($name)){
			throw new InvalidArgumentException('Le chien doit obligatoirement avoir un nom.');
		}

		if(!is_int($nb_puces)){
			throw new InvalidArgumentException('Le nombre de puces saisi n\'est pas de type nombre.');
		}

		if($nb_puces < 1){
			throw new InvalidArgumentException('Le nombre de puces doit être positif.');
		}

		$this->nb_puces = $nb_puces;
		$this->name = $name;
	}

}

?>
