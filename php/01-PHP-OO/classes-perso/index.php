<?php
require_once('class/entreprise/Entreprise.php');
require_once('class/entreprise/EntrepriseSansProfit.php');
require_once('class/entreprise/EntrepriseSecrete.php');

$entreprises = array(
	"renault"	=> new Entreprise('Renault', 'Construire des véhicules', 25000, 275000000),
	"peugeot"	=> new Entreprise('Peugeot', 'Construire des véhicules', 32000, 32100000),
	"cia"		=> new EntrepriseSecrete('CIA', 'Récolter des renseignements pour le gouvernement américain', 100000, 2000000),
	"croix-rouge" => new EntrepriseSansProfit('Croix Rouge', 'Apporter une aide humanitaire', 25000)
);
?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title>Erreurs d'exceptions sur classes personalisées</title>

		<style>
			* { font-family: sans-serif; }

			table{
				width: 100%;
				border-collapse: collapse;
			}

			td, th{
				padding: 15px;
				border : solid 1px #333;
				text-align: left;
			}

			tbody th{
				width: 250px;
			}

			h1{
				margin-top: 45px;
			}
		</style>
	</head>
	<body>
		<?php
		foreach($entreprises as $entreprise):
		?>

			<h1><?= $entreprise->get_nom(); ?></h1>

			<table>
				<thead>
					<tr>
						<th>Label</th>
						<th>Contenu</th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<th>Capital</th>
						<td>
							<?php
							try{
								echo $entreprise->get_capital();
							} catch(Exception $e){
								echo $e->getMessage();
							}
							?>
						</td>
					</tr>
					<tr>
						<th>Mission</th>
						<td>
							<?php
							try{
								echo $entreprise->get_mission();
							} catch(Exception $e){
								echo $e->getMessage();
							}
							?>
						</td>
					</tr>
					<tr>
						<th>Nombre employé</th>
						<td>
							<?php
							try{
								echo $entreprise->get_nb_employes();
							} catch(Exception $e){
								// Si on veux l'afficher
								echo $e->getMessage();

								// Si on ne veux pas l'affichier
								// echo "Ø";
							}
							?>
						</td>
					</tr>
				</tbody>
			</table>

		<?php endforeach; ?>

	</body>
</html>
