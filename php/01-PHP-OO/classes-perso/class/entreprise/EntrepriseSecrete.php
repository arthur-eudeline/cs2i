<?php

class EntrepriseSecrete extends Entreprise {

	public function get_capital(){
		throw new Exception('Cette entreprise est secrète. Cette information est classifiée.');
	}

	public function get_mission(){
		throw new Exception('Cette entreprise est secrète. Cette information est classifiée.');
	}

	public function get_nb_employes(){
		throw new Exception('Cette entreprise est secrète. Cette information est classifiée.');
	}

}

?>
