<?php

class EntrepriseSansProfit extends Entreprise {

	public function get_capital(){
		throw new Exception('Cette entreprise ne fait pas de profits, elle n\'a donc pas de capital.');
	}

}

?>
