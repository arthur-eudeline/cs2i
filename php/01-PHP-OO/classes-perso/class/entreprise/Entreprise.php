<?php

class Entreprise {

	public function __construct($nom, $mission = "Make Monney", $nb_employes = 0, $capital = 0){
		$this->nom = $nom;
		$this->mission = $mission;
		$this->nb_employes = $nb_employes;
		$this->capital = $capital;
	}

	protected $nb_employes;
	public function get_nb_employes(){
		return $this->nb_employes;
	}

	protected $capital;
	public function get_capital(){
		return $this->capital;
	}

	protected $nom;
	public function get_nom(){
		return $this->nom;
	}

	protected $mission;
	public function get_mission(){
		return $this->mission;
	}

}
?>
