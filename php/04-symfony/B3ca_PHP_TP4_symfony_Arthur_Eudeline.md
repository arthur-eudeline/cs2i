### Arthur Eudeline - B3ca

###TP PHP 04 : Installation et utilisation de base du framework PHP Symfony.



#2.1 : Installation du framework symfony

### F ) Expliquer quelle URL utiliser dans votre navigateur pour accéder à la même page d’accueil de e) en utilisant WAMP.

Étant donné que le dossier **www** consitue la racine de **http://localhost**, le projet Symfony se trouvera à l'adresse : **http://localhost[:port]/Symfony/** dans le contexte de l'énnoncé.



#2.2 : Apprendre à utiliser Symfony

### [2.2.1 : Création de pages](https://symfony.com/doc/current/page_creation.html)

#### Création du controller

Pour commencer, il faut créer le controller qui sera associé à la vue `http://localhost/lucky/number`. Pour cela, il faut créer un fichier `src\Controller\LuckyController.php` :

```php
<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LuckyController extends AbstractController
{
  /**
  * @Route("/lucky/number")
  */
  public function number()
  {
    $number = random_int(0, 100);

    return $this->render(
      'lucky/number.html.twig',
      array(
        "number"  => $number
      )
    );
  }
}

?>
```

#### Création de la vue

Il faut ensuite créer un second fichier qui aura cette fois le rôle de template et qui sera situé dans `src\templates\lucky\number.html.twig`. Pour savoir où placer son fichier, il suffit de considérer que le dossier `src\templates` constitue la racine de notre `http://localhost`. Il faut donc reproduire l'arborescence présente dans l'URL souhaitée avec les dossiers, par exemple pour l'url `http://localhost/a-propos/notre-equipe`, nous devrions placer le template ici `src\templates\a-propos\notre-equipe.html.twig`.

Contenu de `number.html.twig` :

```twig
<h1>Your lucky number is {{ number }}</h1>
```

Ce qui affiche :

![image-20181023214035372](assets/image-20181023214035372.png)

#### Liaison de la vue et du controlleur

Afin que le routeur comprennent quelle vue et contrôlleur associer à l'URL `http://localhost/lucky/number`, il faut éditer le fichier `config\routes.yaml` et y ajouter la ligne suivante :

```yaml
app_lucky_number:
  path: /lucky/number
  controller: App\Controller\LuckyController::number
```

- l'argument `path` va définir l'URL qui se situera après la racine, dans notre cas `http://localhost`
- `controller` indique quant à lui la classe à utiliser comme controlleur en passant par son namespace

Le même résultat peut également être obtenu sans avoir à passer par le fichier `config\routes.yaml` grâce aux annotations de routes Symfony. Une syntaxe particulière au niveau des commentaires PHP permet ainsi de directement déclarer une route dans le contrôlleur :

```php
...
use Symfony\Component\Routing\Annotation\Route;
...
class LuckyController extends AbstractController
{
  /**
  * @Route("/lucky/number")
  */
  public function number(){ ... }
...
```



####The bin/console Command

Exécution de la commande `php bin/console debug:router`, qui permet de lister l'ensemble des routes disponibles au sein de notre application Symfony et d'avoir des informations utiles telles que les méthodes supportées ou encore le schéma de l'URL

![image-20181112141340887](assets/image-20181112141340887.png)



#### The Web Debug Toolbar: Debugging Dream :

Avec Symfony flex, il suffit de lancer la commande :

```bash
composer require --dev symfony/profiler-pack
```

Pour l'activer ou la désactiver, il faut ensuite se rendre dans le dossier `config/packages/[__ENV_NAME__]/web_profiler.yaml` depuis la racine du projet, ou `[__ENV_NAME__]` est le nom de l'environnement de développement dans lequel on se site (par défaut "dev").

Dans le fichier on pourra trouver une architecture semblable :

```yaml
web_profiler:
	# toolbar: true # --> toolbar activée
    toolbar: false # --> toolbar désactivée
    intercept_redirects: false

framework:
    profiler: { only_exceptions: false }

```

![image-20181112145819493](assets/image-20181112145819493.png)



#### Rendering a Template :

Cette question a déjà été traitée au début de la 2.2.1 :

- **src/Controller/LuckyController.php**

```php
<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LuckyController extends AbstractController
{
  /**
  * @Route("/lucky/number")
  */
  public function number()
  {
    $number = random_int(0, 100);

    return $this->render(
      'lucky/number.html.twig',
      array(
        "number"  => $number
      )
    );
  }
}

?>
```

- **templates/lucky/number.html.twig**

```html
{# templates/lucky/number.html.twig #}

<h1>Your lucky number is {{ number }}</h1>
```

Ce qui affiche :

![image-20181112150903467](assets/image-20181112150903467.png)



#### Checking out the Project Structure :

 	##### `config/`

C'est le dossier qui va contenir tous les fichiers relatif à la **configuration** de notre projet Symfony



##### `src/`

Contient l'ensemble de notre code **PHP**



##### `templates/`

Contient l'ensemble des templates **Twig**, donc la partie vue du MVC.



##### `bin/`

Contient des **utilitaires** comme c'est par exemple le cas de la console Symfony



##### `var/`

Dossier de destination de fichiers automatiques et temporaires tels que les fichiers de cache.



##### `public/`

Le document racine du projet et accessible publiquement par les utilisateurs



###[2.2.2 : Routing](https://symfony.com/doc/current/routing.html)

Pour intéragir avec le router de Symfony, on peu choisir de passer par plusieurs syntaxes. La plus plébliscitée est celle dite par *annotations*. Celle-ci repose sur une syntaxe particulière de commentaires placés directement dans notre code PHP :

```php
	/**
     * Matches /blog exactly
     *
     * @Route("/blog", name="blog_list")
     */
    public function list()
    {
        // ...
    }
```

On peu également retrouver la configuration des routes via un fichier `config/routes.yaml` :

```yml
# config/routes.yaml
blog_list:
    path:     /blog
    controller: App\Controller\BlogController::list

blog_show:
    path:     /blog/{slug}
    controller: App\Controller\BlogController::show
```

Ou encore via du XML `config/routes.xml` :

```xml
<!-- config/routes.xml -->
<?xml version="1.0" encoding="UTF-8" ?>
<routes xmlns="http://symfony.com/schema/routing"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://symfony.com/schema/routing
        http://symfony.com/schema/routing/routing-1.0.xsd">

    <route id="blog_list" controller="App\Controller\BlogController::list" path="/blog" >
        <!-- settings -->
    </route>

    <route id="blog_show" controller="App\Controller\BlogController::show" path="/blog/{slug}">
        <!-- settings -->
    </route>
</routes>
```

Ou enfin, pour les chevronnés, via du PHP Orienté Objet :

```php
// config/routes.php
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use App\Controller\BlogController;

$routes = new RouteCollection();
$routes->add('blog_list', new Route('/blog', array(
    '_controller' => [BlogController::class, 'list']
)));
$routes->add('blog_show', new Route('/blog/{slug}', array(
    '_controller' => [BlogController::class, 'show']
)));

return $routes;
```



#### Creating Routes

Le code suivant permettra de créer une route conditionnelle :

```php
// src/Controller/BlogController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
     * Matches /blog exactly
     *
     * @Route("/blog", name="blog_list")
     */
    public function list()
    {
        return $this->render(
        	'blog/list.html.twig',
        	array()
      	);
    }

    /**
     * Matches /blog/*
     *
     * @Route("/blog/{slug}", name="blog_show")
     */
    public function show($slug)
    {
        return $this->render(
        	'blog/list.html.twig',
        	array(
          		"post_title"  => $slug
        	)
      	);
    }
}
```

Ainsi, si on va sur une url se terminant exactement par `[...]/blog` alors on appelera la fonction `list()`. Sinon si on a une url qui se poursuit après `[...]/blog/{post_slug}` on appelera cette fois la fonction `show($post_slug)` qui sera alors en mesure de disposer de la variable `$post_slug` contenue dans l'URL.

Ainsi si on va sur http://localhost:8000/blog on obtient :

![image-20181112163242666](assets/image-20181112163242666.png)

Mais si on va sur http://localhost:8000/blog/ on obtiendra cette fois si 

![image-20181112163459184](assets/image-20181112163459184.png)

Avec le titre de l'article qui est en mesure de changer en fonction de l'URL :

```html
{# Appelé par BlogController lorsque l'on appelle un article simple #}
<h1>Article {{ post_title }}</h1>

<p>Lorem ipsum [...]</p>
```



#### Localized Routing (i18n) : construire un site multilingues

Le système de routing par annotation de symfony embarque une solution afin d'intertionaliser les URLs des routes en fonction des langues. On peu ainsi définir plusieurs appelations d'une vue en fonction de différents languages afin que la même vue soit appelée. Ainsi si on écrit un contrôlleur :

```php
<?php
// SRC/AboutUsController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AboutUsController extends AbstractController
{
    /**
     * @Route({
     *     "fr": "/a-propos-de-nous",
     *     "en": "/about-us"
     * }, name="about_us")
     */
    public function about()
    {
        return $this->render(
          'about-us.html.twig',
          array()
        );
    }
}
?>
```

Nous serons capable d'appeler le fichier `about-us.html.twig` à partir de l'url http://localhost:8000/about-us mais aussi à partir de l'url http://localhost:8000/a-propos-de-nous :

![image-20181112164812780](assets/image-20181112164812780.png)

![image-20181112164839707](assets/image-20181112164839707.png)

On peu également venir éditer le fichier `config/routes/annotations.yaml` afin d'ajouter un préfix aux routes :

```yaml
controllers:
    resource: ../../src/Controller/
    type: annotation

    prefix:
      en: ''    # Laisser vide dans la mesure où l'anglais est la langue par défaut
      fr: '/fr'
```

Ainsi, si on affiche nos routes, on remarque que de nouvelles URL sont maintenant accessibles :

![image-20181112165437332](assets/image-20181112165437332.png)

##### Récupérer la locale demandée par l'utilisateur

Pour récupérer la langue demandée par l'utilisateur, on peu, depuis le contrôlleur, appeler le service de requête HTTP : 

```php
<?php
// Controller
// [...]
use Symfony\Component\HttpFoundation\Request;

class AboutUsController extends AbstractController
{
    // [...]
    // Passer en paramètre l'objet Request $request
    public function about(Request $request)
    {
        return $this->render(
          'about-us.html.twig',
          array(
              
              // Récupération de la langue via requête HTTP
              "locale" => $request->getLocale()
              
          )
        );
    }
}
    
?>
```



#### Adding {wildcard} Requirements

Les paramètres passés dans l'URL et analysés par le système de routing de Symfony peuvent également être restreints à certains types de données afin de ne pas être détectés dans une situation où il ne le faudrait pas, comme par exemple les pages de blog qui donneraient des url en blog/2, blog/3, donc si on ne contraint pas ce paramètre, il est aussi detecté lorsque l'on tape : blog/mon-article. Pour résoudre ce problème, on peu donc indiquer que le paramètre de page doit obligatoirement être de type numérique :

```php
class BlogController extends AbstractController
{
    /**
     * Matches /blog exactly
     *
     * @Route("/blog/{page}", name="blog_list", requirements={"page"="\d+"})
     */
    public function list()
    {
      return $this->render(
        'blog/list.html.twig',
        array()
      );
    }
  	
  	// [...]
}
```



####[Giving {placeholder} a default value](https://symfony.com/doc/current/routing.html#giving-placeholders-a-default-value)

Avec le code précédemment ajouté, nous faisons maintenant face à un problème. En effet, http://localhost:8000/blog/1 va fonctionner, mais http://localhost:8000/blog va renvoyé une erreur `route not found`. Pour pallier à cela, nous aimerions que par défaut, si on ne saisit pas de numéro de page, celui-ci soit initialisé à 1 :

```php
class BlogController extends AbstractController
{
    /**
     * Matches /blog exactly
     *
     * @Route("/blog/{page}", name="blog_list", requirements={"page"="\d+"})
     */
    public function list($page = 1)
  	
  	// [...]
}
```



#### [Listing all of your routes](https://symfony.com/doc/current/routing.html#listing-all-of-your-routes)

Utilisation de la commande `php bin/console debug:router`

![image-20181120110059949](assets/image-20181120110059949.png)



#### [Advanced routing example](https://symfony.com/doc/current/routing.html#advanced-routing-example)

```php
// src/Controller/ArticleController.php

// ...
class ArticleController extends AbstractController
{
    /**
     * @Route(
     *     "/articles/{_locale}/{year}/{slug}.{_format}",
     *     defaults={"_format": "html"},
     *     requirements={
     *         "_locale": "en|fr",
     *         "_format": "html|rss",
     *         "year": "\d+"
     *     }
     * )
     */
    public function show($_locale, $year, $slug)
    {
    }
}
```

Si on reprend ce bout de code donné en exemple, on peu voir que l'on peu aisément construire une structure d'URL poussée :

- `defaults={"_format": "html"}` indique la valeur par défaut de la variable `$_format` qui est initialisé à `html`
- `requirements={}` indique les contraintes que l'on souhaite appliquer à nos variables sous forme d'expression régulières



#### [Controller Naming Pattern](https://symfony.com/doc/current/routing.html#controller-naming-pattern)

On peut également faire référence à nos controlleurs via l'utilisation du mot clé `use` sans indiquer le chemin absolu de la class, mais plutôt `app\Controller\__NOM_CLASSE_CONTROLLER__`



#### [Generating URLs](https://symfony.com/doc/current/routing.html#generating-urls)

Le système de routing de Symfony permet de générer des URLs même si celles-ci ne renverraient pas quelque chose. Par exemple, si on imagine une url `/blog/mon-super-article` mais qu'aucun article avec un slug "*mon-super-article*" n'existe en base de données, on peu par exemple effectuer certaines actions, comme appeler un template spécifique en détectant l'envoi de cette URL



###[2.2.3 Controllers](https://symfony.com/doc/current/controller.html)

#### [A simple controller](https://symfony.com/doc/current/controller.html#a-simple-controller)

exemple de contrôlleur simple déjà réalisé au début du TP : 

```php
<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
     * Matches /blog exactly
     *
     * @Route("/blog/{page}", name="blog_list", requirements={"page"="\d+"})
     */
    public function list($page = 1)
    {
      return $this->render(
        'blog/list.html.twig',
        array()
      );
    }
  
  	// [...]
  
}
?>
```



#### [Mapping a URL to a Controller](https://symfony.com/doc/current/controller.html#mapping-a-url-to-a-controller)

Si on se base sur la méthodes des annotations on peu associer une route à controller comme ceci :

```php
class BlogController extends AbstractController
{
    /**
     * Association de route pour http://localhost:8000/blog
     * @Route("/blog")
     */
    public function list(){}
  
  	// [...]
  
}
```



#### [The base Controller Classes and Services](https://symfony.com/doc/current/controller.html#the-base-controller-classes-services)

On peut faire hériter nos controller de la classe mère `AbstractController` afin de récupérer tout un tas de méthodes utiles au bon fonctionnement de Symfony, notamment la méthode `AbstractController.render()` qui est utile pour afficher un template :

```php
// Import de la classe mère AbstractController
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// Blog controlleur hérite d'AbstractController, c'est donc une façon d'y injecter des comportements de Symfony
class BlogController extends AbstractController
{
   	// [...] 
}
```



#### [Redirecting](https://symfony.com/doc/current/controller.html#redirecting)

Symfony offre la possibilité de rediriger les utilisateurs via la méthode `AbstractConroller.redirectToRoute()` :

```php
// Inclusion de la méthode de redirection.
use Symfony\Component\HttpFoundation\RedirectResponse;

// ...

public function maMethode()
{
  	// Structure de la fonction redirection 
    return $this->redirectToRoute('__ROUTE_SLUG__', $args['__NOM_ARG__' => '__ARG_VAL__'], __HTTP_CODE_RESPONSE__);

    // Redirige vers une route tout en lui passant les paramètres récupérés par notre vue
    return $this->redirectToRoute('blog', $request->query->all());

    // Redirection externe
    return $this->redirect('https://google.com/');
}
```



#### [Rendering Template](https://symfony.com/doc/current/controller.html#rendering-templates)

Pour rendre un template, on peu choisir de rendre un template Twig, ce qui est bien plus utile, ou d'afficher directement du contenu HTML :

```php
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog/{page}", name="blog_list", requirements={"page"="\d+"})
     */
    public function list($page = 1)
    {
      // Rendu du template app/templates/bloglist.html.twig
      return $this->render(
        'blog/list.html.twig',
        array()
      );
      
      // Si on voulait rendre du HTML :
      return new Response(
      	'<html><body>Lucky number: '.$number.'</body></html>'
      );
    }
  
  	// ...
}
```



#### [Generating Controllers](https://symfony.com/doc/current/controller.html#generating-controllers)

Au lieu de créer manuellement des controlleurs manuellement, nous pouvons également passer par la console de Symfony qui nous permet de créer des controlleurs par le biais de la commande :

```
php bin/console make:controller __NOM_DU_CONTROLLEUR__
```



#### [Managing Errors and 404 Pages](https://symfony.com/doc/current/controller.html#managing-errors-and-404-pages)

Pour générer des erreurs 404, Symfony permet de générer des erreurs de type `createNotFoundException` qui génèrent des erreurs HTTP 404.

```php
<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

// Import des erreurs 404
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog/{page}", name="blog_list", requirements={"page"="\d+"})
     */
    public function list($page = 1)
    {
      $posts = array();
      
      if( empty($posts) ){
        // on génère l'erreur 404
        throw $this->createNotFoundException('Aucun article trouvé');
      }
      
      return $this->render(
        'blog/list.html.twig',
        array()
      );
    }

}

?>
```



#### [The Request object as a Controller Argument](https://symfony.com/doc/current/controller.html#the-request-object-as-a-controller-argument)

Dans notre contrôlleur, il est possible de récupérer un objet `Response` dans notre contrôleur et qui contient une multidue d'information sur la requête effectuée par l'utilisateur. Ainsi, si on place un `var_dump()` pour afficher le contenu de notre objet `Response`, on remarque qu'il contient notamment des informations sur :

- les paramètres de la requête
- des informations sur le serveur (protocole, version de PHP, nom de domaine, ...)
- les headers, cookies, et session de la requête
- ...

```php
<?php 
// ...
  
use Symfony\Component\HttpFoundation\Request;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog/{page}", name="blog_list", requirements={"page"="\d+"})
     */
    public function list(Request $request, $page = 1)
    {
      echo "<pre>";
      var_dump($request);
      echo "</pre>";
      
      return $this->render(
        'blog/list.html.twig',
        array()
      );
    }
}

?>
```



#### [Managing the Session](https://symfony.com/doc/current/controller.html#managing-the-session)

Symfony simplifie grandement l'interaction avec les `$_SESSION` de PHP :

```php
// ...

use Symfony\Component\HttpFoundation\Session\SessionInterface;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog/{page}", name="blog_list", requirements={"page"="\d+"})
     */
    public function list(Request $request, SessionInterface $session, $page = 1)
    {
      // On définit le userName avec la valeur Arthur Eudeline
      $session->set('userName', 'Arthur Eudeline');

      // On récupère le userName depuis la session, s'il n'est pas définit on lui indique de prendre la valeur John Doe
      $userName = $session->get('userName', 'John Doe');
    }
}
```



#### [Flash Messages](https://symfony.com/doc/current/controller.html#flash-messages)

Les `Flash Messages` sont des messages stockés en `$_SESSION` par Symfony et qui sont supprimés dès leur premier affichage. Cela fait d'eux l'outil idéal pour afficher des **messages** ou des **notifications** à l'utilisateur. Exemple d'utilisation :

```php
use Symfony\Component\HttpFoundation\Request;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog/{page}", name="blog_list", requirements={"page"="\d+"})
     */
    public function list(Request $request, SessionInterface $session, $page = 1)
    {
      $this->addFlash(
        'flash_notice', // index du message 
        'Test de message !' // contenu du message
      );
      
      return $this->render(
        'blog/list.html.twig',
        array()
      );
    }
}
```

Et dans notre template Twig :

```twig
{% for message in app.flashes('flash_notice') %}
<div class="flash-notice">
  {{ message }}
</div>
{% endfor %}
```

Ce qui affiche :

![image-20181120153052385](assets/image-20181120153052385.png)



#### [The Request and Response Object](https://symfony.com/doc/current/controller.html#the-request-and-response-object)

Pour accèder aux informations contenues dans l'objet `Request`, on peu utiliser ces méthodes :

```php
public function list()
{
  	// Savoir si la requête est faite en Ajax ou non
    $request->isXmlHttpRequest();

    // Récupère les variables contenues dans $_POST
    $request->server->get('HTTP_HOST');

    // Récupère $_FILES['foo]
    $request->files->get('foo');

    // Récupère $_COOKIES['PHPSESSID]
    $request->cookies->get('PHPSESSID');

    // Récupère une des informations contenues dans le header de la requête
    $request->headers->get('host');
    $request->headers->get('content-type');
}
```

Avec l'objet `Response` on peu cette fois retourner une réponse au lieu de retourner simplement l'affichage d'un template Twig :

```php
// On retourne du CSS au lieu de retourner de l'HTML
$response = new Response('<style> ... </style>');
$response->headers->set('Content-Type', 'text/css');
```



#### [Returning JSON Response](https://symfony.com/doc/current/controller.html#returning-json-response)

```php
return $this->json($data["data_key" => "data_value"], $HTTP_status = 200, $headers = array(), $context = array());
```



####[Streaming File Responses](https://symfony.com/doc/current/controller.html#streaming-file-responses)

```php
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

public function download()
{
    // On charge un fichier avec son chemin relatif
    $file = new File('/path/to/some_file.pdf');

  	// Force le téléchargement du fichier sans changer son nom
    return $this->file($file);

    // Change le nom du fichier
    return $this->file($file, 'mon-fichier.pdf');

    // Affiche le contenu dans le navigateur plutôt que de forcer le téléchargement
    return $this->file($file, 'mon-fichier.pdf', ResponseHeaderBag::DISPOSITION_INLINE);
}
```





### [2.2.4 : les templates](https://symfony.com/doc/current/templating.html)

#### [Template Inheritance and Layouts](https://symfony.com/doc/current/templating.html#template-inheritance-and-layouts)

On peut définir des "bouts" de template qui seront affichés à certains endroits. Pour cela on définit un template parent, dont les enfant hériteront. C'est dans le template enfant que l'ont comblera les "trous" laissés dans le template parent par le code contenu dans le template enfant :

- Template parent `base.html.twig`

  ```html
  <!DOCTYPE html>
  <html lang="fr" dir="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <title>{% block pageTitle %}Titre par défaut{% endblock %}</title>
    <meta name="description" content=""/>
  
    <link rel="stylesheet" type="text/css" href="#" />
  
    {% block header %}{% endblock %}
  </head>
  
  <body>
    <p>---- Titre de la page utilisé à la fois dans la balise head et dans le h1 ----</p>
    {# on ne peu pas rappeller % block %, il ne peut être défini qu'une seule fois #}
    <h1>{{ block('pageTitle') }}</h1>
    <p>---- /Titre de la page utilisé à la fois dans la balise head et dans le h1 ----</p>
  
  
    <p>---- block body ----</p>
    {% block body %}{% endblock %}
    <p>---- /block body ----</p>
  
    <p>---- footer ----</p>
    {% block footer %}Défaut footer{% endblock %}
    <p>---- /footer ----</p>
  </body>
  </html>
  ```



  ```html
  {% extends 'base.html.twig' %}
  
  {% block pageTitle %}
    Liste des articles
  {% endblock %}
  
  {% block body %}
    {% for message in app.flashes('flash_notice') %}
    <div class="flash-notice">
      {{ message }}
    </div>
    {% endfor %}
  
    <ul>
      <li>Article 1</li>
      <li>Article 2</li>
      <li>Article 3</li>
    </ul>
  {% endblock %}
  
  ```



  ![image-20181120161415347](assets/image-20181120161415347.png)

Il est intéressant de voir que l'ont peut également récupérer le contenu par défaut du block situé dans le template parent :

```html
{% block footer %}
<p>Test</p>
{# inclu le contenu par défaut du template parent #}
{{ parent() }}
{% endblock %}
```



#### [Including other Templates](https://symfony.com/doc/current/templating.html#including-other-templates)

On peu inclure d'autres templates directement depuis Twig avec `{{ include( '__PATH_TO_TEMPLATE__', { '__DATA_KEY__' : __DATA_VALUE__ }) }}` :

```twig
{{ include('blog/article-excerpt.html.twig', { 'article': article }) }}
```



#### [Linking to Pages](https://symfony.com/doc/current/templating.html#linking-to-pages)

On peu facilement récupérer un lien vers une route en spécifiant son slug dans Twig :

```php
/**
 * @Route("/", name="welcome")
 */
public function index()
{
  // ...
}
```

```html
<a href="{{ path('welcome') }}">Home</a>
```



#### [Linking to Assets](https://symfony.com/doc/current/templating.html#linking-to-assets)

Pour faire des liens vers des images par exemple, on peut utiliser la fonction `asset()` de Symfony, qu'il faut commencer par installer :

```
composer require symfony/asset
```

Puis l'utilisation est assez simple. Les assets doivent par défaut être stockées dans `public/` :

```html
<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>

  <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />

</head>
 
<body>
  <!-- chemin absolu -->
	<img src="{{ absolute_url(asset('images/logo.png')) }}" alt="Symfony!" />
</body>
</html>
```

Ainsi, `style.css` devra être stocké dans `/public/css/style.css`.



###2.2.6 Questions :

#### Création d’une nouvelle application :

```
composer create-project symfony/website-skeleton my-project
```

#### Création d'une nouvelle page Web :

La commande suivante permet la création de base d'un controlleur lié à un template Twig lui aussi généré automatiquement lors de cette commande :

```
php bin/console make:controller __NOM_DU_CONTROLLEUR__
```

####Création d’un routage :

- **Qu’est ce que le routage ?** 

  Le routage désigne le fait de <u>lier une url (ou route) à un contrôleur</u> qui se chargera d'appeler divers services, mettre en forme les données et de les injecter dans la vue qui affichera le tout à l'utilisateur.

- **De préférence, comment est configuré le routage ?**

  Symfony offre plusieurs possibilité pour mettre en places les routes d'une application. On peu par exemple éditer le fichier `config/routes.yaml` ou bien `config/routes.xml` si on préfère le XML. Cependant, le moyen plébliscité par Symfony est la <u>déclaration de routes par annotation</u> :

  ```php
  // src/Controller/BlogController.php
  namespace App\Controller;
  
  use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
  
  // Nécessaire pour utiliser la déclaration de route par annotation
  use Symfony\Component\Routing\Annotation\Route;
  
  class BlogController extends AbstractController
  {
      /**
  		 * Appelle la fonction list() lorsque l'on se rend sur l'url http://localhost:8000/blog
       * @Route("/blog", name="blog_list")
       */
      public function list()
      {
          return $this->render(
          	'blog/list.html.twig',
          	array()
        	);
      }
    
    	// ...
  }
  ```

#### Création d’un contrôleur :

```
php bin/console make:controller __NOM_DU_CONTROLLEUR__
```

Ce qui génère un code similaire à celui-ci :

```php
// src/Controller/BlogController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

// Nécessaire pour utiliser la déclaration de route par annotation
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
		 * Appelle la fonction list() lorsque l'on se rend sur l'url http://localhost:8000/blog
     * @Route("/blog", name="blog_list")
     */
    public function list()
    {
        return $this->render(
        	'blog/list.html.twig',
        	array()
      	);
    }
  
  	// ...
}
```

#### Test de bon fonctionnement :

Pour voir si notre page web fonctionne, on peut déjà se rendre à l'URL et vérifier qu'aucune erreur ne soit générée. On peu également utiliser la commande `debug:router` afin de s'assurer que notre URL est bien enregistrée et donc accessible :

```
php bin/console debug:router
```

