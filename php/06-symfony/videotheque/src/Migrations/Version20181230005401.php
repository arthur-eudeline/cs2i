<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181230005401 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE film CHANGE realisateur_id realisateur_id INT DEFAULT NULL, CHANGE pays_id pays_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE film ADD CONSTRAINT FK_8244BE22F1D8422E FOREIGN KEY (realisateur_id) REFERENCES realisateur (id)');
        $this->addSql('ALTER TABLE film ADD CONSTRAINT FK_8244BE22A6E44244 FOREIGN KEY (pays_id) REFERENCES pays (id)');
        $this->addSql('CREATE INDEX IDX_8244BE22F1D8422E ON film (realisateur_id)');
        $this->addSql('CREATE INDEX IDX_8244BE22A6E44244 ON film (pays_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE film DROP FOREIGN KEY FK_8244BE22F1D8422E');
        $this->addSql('ALTER TABLE film DROP FOREIGN KEY FK_8244BE22A6E44244');
        $this->addSql('DROP INDEX IDX_8244BE22F1D8422E ON film');
        $this->addSql('DROP INDEX IDX_8244BE22A6E44244 ON film');
        $this->addSql('ALTER TABLE film CHANGE realisateur_id realisateur_id INT NOT NULL, CHANGE pays_id pays_id INT NOT NULL');
    }
}
