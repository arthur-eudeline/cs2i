<?php

namespace App\Repository;

use App\Entity\Film;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Film|null find($id, $lockMode = null, $lockVersion = null)
 * @method Film|null findOneBy(array $criteria, array $orderBy = null)
 * @method Film[]    findAll()
 * @method Film[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilmRepository extends ServiceEntityRepository
{
  public function __construct (RegistryInterface $registry)
  {
    parent::__construct($registry, Film::class);
  }
  
  // /**
  //  * @return Film[] Returns an array of Film objects
  //  */
  /*
  public function findByExampleField($value)
  {
      return $this->createQueryBuilder('f')
          ->andWhere('f.exampleField = :val')
          ->setParameter('val', $value)
          ->orderBy('f.id', 'ASC')
          ->setMaxResults(10)
          ->getQuery()
          ->getResult()
      ;
  }
  */
  
  /*
  public function findOneBySomeField($value): ?Film
  {
      return $this->createQueryBuilder('f')
          ->andWhere('f.exampleField = :val')
          ->setParameter('val', $value)
          ->getQuery()
          ->getOneOrNullResult()
      ;
  }
  */
  
  public function search (Array $formData)
  {
    $query = $this->createQueryBuilder('film');
    
    if (isset($formData['search']) && !empty($formData['search'])) {
      $query->andWhere('film.titre LIKE :titre');
      $query->setParameter('titre', '%' . $formData['search'] . '%');
    }
    
    if (isset($formData['filter_realisateur']) && !empty($formData['filter_realisateur'])) {
      $query->andWhere('film.realisateur = :realisateur');
      $query->setParameter('realisateur', $formData['filter_realisateur']);
    }
    
    if (isset($formData['filter_pays']) && !empty($formData['filter_pays'])) {
      $query->andWhere('film.pays = :pays');
      $query->setParameter('pays', $formData['filter_pays']);
    }
    
    if (isset($formData['filter_annee']) && !empty($formData['filter_annee'])) {
      $query->andWhere('film.annee = :annee');
      $query->setParameter('annee', $formData['filter_annee']);
    }
    
    return $query->getQuery()->getResult();
  }
}
