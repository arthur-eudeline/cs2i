<?php

namespace App\Controller;

use App\Entity\Film;
use App\Form\FilmType;
use App\Repository\FilmRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\Pays;
use App\Entity\Realisateur;

/**
 * @Route("/")
 */
class FilmController extends AbstractController
{
  /**
   * @Route("/", name="film_index", methods={"GET"})
   */
  public function index (FilmRepository $filmRepository, Request $request): Response
  {
    $searchForm = $this->getSearchForm();
    $searchForm->handleRequest($request);
    
    if ($searchForm->isSubmitted() && $searchForm->isValid()) {
      $renderContent = $filmRepository->search($request->query->get('form'));
    } else {
      $renderContent = $filmRepository->findAll();
    }
    
    
    return $this->render('film/index.html.twig', array(
      'films' => $renderContent,
      'form' => $searchForm->createView()
    ));
  }
  
  /**
   * @Route("/film/new", name="film_new", methods={"GET","POST"})
   */
  public function new (Request $request): Response
  {
    $film = new Film();
    $form = $this->getForm($film);
    $form->handleRequest($request);
    
    if ($form->isSubmitted() && $form->isValid()) {
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($film);
      $entityManager->flush();
      
      return $this->redirectToRoute('film_index');
    }
    
    return $this->render('film/new.html.twig', [
      'film' => $film,
      'form' => $form->createView(),
    ]);
  }
  
  /**
   * @Route("film/{id}", name="film_show", methods={"GET"})
   */
  public function show (Film $film): Response
  {
    return $this->render('film/show.html.twig', ['film' => $film]);
  }
  
  /**
   * @Route("film/{id}/edit", name="film_edit", methods={"GET","POST"})
   */
  public function edit (Request $request, Film $film): Response
  {
//    $form = $this->createForm(FilmType::class, $film);
    $form = $this->getForm($film);
    $form->handleRequest($request);
    
    if ($form->isSubmitted() && $form->isValid()) {
      $this->getDoctrine()->getManager()->flush();
      
      return $this->redirectToRoute('film_index', ['id' => $film->getId()]);
    }
    
    return $this->render('film/edit.html.twig', [
      'film' => $film,
      'form' => $form->createView(),
    ]);
  }
  
  /**
   * @Route("film/{id}", name="film_delete", methods={"DELETE"})
   */
  public function delete (Request $request, Film $film): Response
  {
    if ($this->isCsrfTokenValid('delete' . $film->getId(), $request->request->get('_token'))) {
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->remove($film);
      $entityManager->flush();
    }
    
    return $this->redirectToRoute('film_index');
  }
  
  private function getForm (Film $film)
  {
    $class = array("class" => 'form-control');
    $label_class = array("class"  => "bmd-label-floating full-width");
    
    $form = $this->createFormBuilder($film)
      ->add('titre', TextType::class, array(
        'label' => 'Titre du film',
        'label_attr' => $label_class,
        'attr' => $class
      ))
      ->add('realisateur', EntityType::class, array(
        'label' => "Réalisateur",
        'label_attr' => $label_class,
        'class' => Realisateur::class,
        'choice_label' => 'prenomNom',
        'attr' => $class
      ))
      ->add('pays', EntityType::class, array(
        'class' => Pays::class,
        'choice_label' => 'nom',
        'label' => "Pays",
        'label_attr' => $label_class,
        'attr' => $class
      ))
      ->add('annee', TextType::class, array(
        'label' => 'Année de parution',
        'label_attr' => $label_class,
        'attr' => $class
      ))
      ->add('synopsis', TextareaType::class, array(
        'label' => 'Synopsis',
        'label_attr' => $label_class,
        'attr' => array_merge($class, array(
          'style' => 'min-height: 100px'
        ))
      ))
      
      ->getForm();
    return $form;
  }
  
  private function getSearchForm(){
    $class = array("class" => 'form-control');
    $label_class = array("class"  => "bmd-label-floating full-width");
    
    return $this->createFormBuilder()
      ->setMethod('GET')
      ->add('search', TextType::class, array(
        'label' => 'Titre du film',
        'label_attr' => $label_class,
        'attr' => $class,
        'required'  => false
      ))
      ->add('filter_realisateur', EntityType::class, array(
        'label' => "Réalisateur",
        'label_attr' => $label_class,
        'class' => Realisateur::class,
        'choice_label' => 'prenomNom',
        'attr' => $class,
        'required'  => false
      ))
  
      ->add('filter_pays', EntityType::class, array(
        'class' => Pays::class,
        'choice_label' => 'nom',
        'label' => "Pays",
        'label_attr' => $label_class,
        'attr' => $class,
        'required'  => false
      ))
  
      ->add('filter_annee', TextType::class, array(
        'label' => 'Année de parution',
        'label_attr' => $label_class,
        'attr' => $class,
        'required'  => false
      ))
      
      ->getForm();
  }
}
