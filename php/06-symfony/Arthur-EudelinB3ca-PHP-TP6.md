### Arthur Eudeline B3ca

# PHP - Symfony : TP05

## 5 : CRUD

Dans un premier temps nous allons créer une entite `task2` avec un champ `titre (String)` et `due_date (Datetime)` : 

![image-20181210154726058](assets/image-20181210154726058-4453246.png)

Ensuite, il faut générer notre fichier SQL pour préparer les modifications en base de données :

```bash
php bin/console make:migration
```

Une fois ce fichier de migration généré, on peu effectuer les modifications en base avec la commande :

```bash
php bin/console doctrine:migrations:migrate
```

Enfin, nous pouvons générer notre CRUD qui va se charger de générer pour nous les templates, formulaires d'ajout/mise à jour/suppression et contrôleurs de notre entité via la commande :

```bash
php bin/console make:crud Task2
```

De cette manière, on récupère aisément un formulaire d'ajout basique : 

![image-20181210162751484](assets/image-20181210162751484-4455671.png)

![image-20181210162826805](assets/image-20181210162826805-4455706.png)



## 6.1

Création de l'entité `task2` avec un champ `titre` et `due_date` :

![image-20181210154726058](assets/image-20181210154726058-4453246.png)



Durant le TP5, j'avais déjà réalisé un exemple de création d'objet via un formulaire :

```php
/**
  * @Route("/mon-entite/new")
  */
  public function new(Request $request){
    $success = false;
    $entityManager = $this->getDoctrine()->getManager();
    $monEntite = new monEntite();

    // Création du formulaire
    $form = $this->createFormBuilder( $monEntite )
    ->add('nomEntite', TextType::class)
    ->add('descEntite', TextType::class)
    ->add('save', SubmitType::class, array('label' => 'Create Task'))
    ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {

      // Récupération des données du formulaire
      $monEntite = $form->getData();

      // Ecriture
      $entityManager->persist($monEntite);
      // Sauvegarde en base
      $entityManager->flush();

      $success = true;
    }

    return $this->render('mon_entite/new.html.twig', [
      "form"    => $form->createView(),
      "success"  => $success
    ]);
  }
```

Ainsi, le formulaire affiche :

![image-20181210160207335](assets/image-20181210160207335-4454127.png)

Et ajoute bel et bien notre entité à la base de données (*ici en ID 5*) :

![image-20181210160304600](assets/image-20181210160304600-4454184.png)



## 6.2 : Faire fonctionner un exemple de site complet avec Symfony 

Pour éviter d'être bloqué par le contrôle de sécurité de Symfony qui bloque l'installation du projet, on peu contourner cela en installant la version de dev :

```bash
composer create-project symfony/symfony-demo -s dev
```

Le site de Symfony demo est un blog basique. Il est donc découpé en deux parties : 

- la partie publique sert la partie du blog avec une vue de listing des articles, une de lecture et une page de connxion. Le tout est multilangue. 
- la partie admin, prefixée dans l'url par `/admin/` qui est reservée aux redacteurs et gestionnaires du site.

Pour cette application, nous allons retrouver trois types d'entité :

- Les commentaires laissés par les utilisateurs : `Comment`
- Les articles de blog : `Post`
- Les étiquettes de classification des articles : `Tag`
- Les utilisateurs du blog : `User`



### Les posts

Au niveau de l'affichage, pour le listing des articles on va retrouver une méthode `BlogController->index()` qui va se charger de récupérer tous les articles en faisant appel aux méthodes stoquées dans les `Repository` de l'application. Les `Repository` comme leur nom l'indique, sont des classes regroupant des méthodes utiles pour chaque type d'entité. Par exemple, pour notre exemple du listing des derniers articles, on va faire appel à la méthode `PostRepository->findLatest()` qui récupère simplement les derniers posts via une requête SQL. Elle inclue également une méthode de pagination des résultats.  

Au niveau des vues :

- `index` : listing des derniers articles et des résultats de recherche avec pagination 
- `postShow`  : affichage d'un article en particulier 
- `commentNew` : création d'un nouveau commentaire 
- `search` : recherche d'articles



### Le back office

La partie sécurité de l'administration est gérée grâce notamment aux `symfony/security-bundle` et `SensioFrameworkExtraBundle` qui permet notamment de sécuriser simplement certaines vues grâce à des annotations du type :

```
/**
 * Vérifie que l'utilisateur courant a bien le rôle "ROLE_ADMIN"
 * @IsGranted("ROLE_ADMIN")
 */
```

On peut généraliser la portée de cette vérification à toute une classe (une classe `admin` qui engloberai tous les controllers de l'admin par exemple), ou à un controller particulier.

Certaines vérifications peuvent également être effectuées directement dans les vues : 

```twig
{% if is_granted('ROLE_ADMIN') %}
    <a href="...">Delete</a>
{% endif %}
```

> Lien vers la [documentation de la sécurité dans Symfony](https://symfony.com/doc/current/security.html)



## 6.3 : exemple de la vidéothèque sous symfony

### Création du projet

- Création du projet :

```
composer create-project symfony/website-skeleton videotheque
```

- Création des entités en fonction des tables présentes dans la base de données de l'ancienne vidéothèque et mise à jour de la base de données. 

  > Pour ce qui est des pays et des réalisateurs, plutôt que de créer un champs `réalisateur_id` manuellement, nous pouvons indiquer un champ de type `relation` pointant vers l'entité désirée.

```
# x3
php bin/console make:entity

php bin/console make:migration

php bin/console doctrine:migrations:migrate
```

- Composition des entités 

| Film                                                | Realisateur         | Pays            |
| --------------------------------------------------- | ------------------- | --------------- |
| <u>id int</u>                                       | <u>Id int</u>       | <u>id int</u>   |
| titre varchar(255)                                  | nom varchar(255)    | nom String(255) |
| *realisateur* relation -> `Realisateur` (ManyToOne) | prenom varchar(255) |                 |
| *pays* relation -> `Pays` (ManyToOne)               |                     |                 |
| annee int                                           |                     |                 |
| synopsis longtext                                   |                     |                 |

- Génération des contrôlleurs et de certaines vues :

```
php bin/console make:crud Film
php bin/console make:crud Realisateur
php bin/console make:crud Pays
```

### Création des templates

Voici un exemple du style de mofications apportées. Il suffit juste de prendre les fichiers HTML de la vidéothèque du TP MVC, et de les convertires en TWIG :

```twig
{% extends 'base.html.twig' %}

{% block title %}Liste des films{% endblock %}

{% block body %}
  <div class="container" style="padding-bottom: 50px;">
    <div class="row">
      <h1>Liste des films</h1>
    </div>
  </div>

  <div class="container" style="padding-bottom: 50px;">
    <div class="row">
      <div class="card-deck" style="width:100%">
        {% for film in films %}
          <div class="card">

            <div class="card-body">
              <h2 class="card-title">{{ film.titre }}</h2>

              <div class="film-infos card-subtitle mb-2 text-muted">
                <p>
									<span class="date">
										Sortit en {{ film.annee }}
									</span>

                  <span class="realisateur">Réalisé par <b>{{ film.realisateur }}</b></span>

                  <span class="pays">| {{ film.pays }}</span>
                </p>
              </div>

              <div class="film-synopsis card-text">
                <p>
                  {{ film.synopsis }}
                </p>
              </div>
            </div>

          </div>
        {% else %}
          <div class="card">
            <div class="card-body">
              <p>
                Aucun film trouvé
              </p>
            </div>
          </div>
        {% endfor %}
      </div>
    </div>
  </div>

{% endblock %}
```

### Personnalisation des formulaires

Purement facultative, cette tâche me permet de modifier l'aspect des formulaires et de mieux comprendre le fonctionnement des formulaires dans Symfony, au lieu de simplement passé par la génération d'un formulaire simple qu'aurais donné l'utilisation de `$this->getForm($film);`.

#### Le controlleur

```php
class FilmController extends AbstractController
{
 
 ...

  public function new (Request $request): Response
  {
    $film = new Film();
		// $form = $this->createForm(FilmType::class, $film); // --> génére tout le formulaire
    $form = $this->getForm($film); // --> méthode personalisée
    $form->handleRequest($request);
    
    if ($form->isSubmitted() && $form->isValid()) {
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($film);
      $entityManager->flush();
      
      return $this->redirectToRoute('film_index');
    }
    
    return $this->render('film/new.html.twig', [
      'film' => $film,
      'form' => $form->createView(),
    ]);
  } 

  ...
  
 	private function getForm (Film $film)
  {
    $class = array("class" => 'form-control');
    $label_class = array("class"  => "bmd-label-floating full-width");
    
    $form = $this->createFormBuilder($film)
      ->add('titre', TextType::class, array(
        'label' => 'Titre du film',
        'label_attr' => $label_class,
        'attr' => $class
      ))
      ->add('realisateur', EntityType::class, array(
        'label' => "Réalisateur",
        'label_attr' => $label_class,
        'class' => Realisateur::class,
        'choice_label' => 'prenomNom', // --> création méthode personnalisée Realisateur.getPrenomNom() dans l'entité
        'attr' => $class
      ))
      ->add('pays', EntityType::class, array(
        'class' => Pays::class,
        'choice_label' => 'nom',
        'label' => "Pays",
        'label_attr' => $label_class,
        'attr' => $class
      ))
      ->add('annee', TextType::class, array(
        'label' => 'Année de parution',
        'label_attr' => $label_class,
        'attr' => $class
      ))
      ->add('synopsis', TextareaType::class, array(
        'label' => 'Synopsis',
        'label_attr' => $label_class,
        'attr' => $class
      ))
      ->getForm();
    
    return $form;
  }
}
```



#### Le fichier Twig :

```twig
{# Ajoute la classe 'col-12' #}
{{ form_start(form, {'attr' : {'class' : 'col-12'}}) }}
 <!-- Titre film -->
  <div class="card" style="margin-bottom: 30px;">
    <div class="card-body">

      <div class="form-group">
      	{# Génère le contenu HTML liée au titre (label + input) #}
        {{ form_row(form.titre) }}
      </div>

    </div>
  </div>

  <!-- Réalisateur -->
  <div class="card" style="margin-bottom: 30px;">
    <div class="card-body">

      <div class="form-group">
        {{ form_row(form.realisateur) }}
      </div>

      <p class="text-right">
        <a href="{{ path('realisateur_new') }}" class="btn btn-default" target="_blank">Créer un nouvel utilisateur</a>
      </p>

    </div>
  </div>

  <!-- Pays -->
  <div class="card" style="margin-bottom: 30px;">
    <div class="card-body">

      <div class="form-group">
        {{  form_row(form.pays) }}
      </div>

      <p class="text-right">
        <a href="{{ path('pays_new') }}" class="btn btn-default" target="_blank">Créer un nouveau pays</a>
      </p>

    </div>
  </div>

  <!-- Année de parution -->
  <div class="card" style="margin-bottom: 30px;">
    <div class="card-body">

      <div class="form-group">
        {{ form_row(form.annee) }}
      </div>

    </div>
  </div>

  <!-- Synopsis -->
  <div class="card" style="margin-bottom: 30px;">
    <div class="card-body">

      <div class="form-group">
        {{ form_row(form.synopsis) }}
      </div>

    </div>
  </div>

  <div class="row text-right">
    <div class="col">
      <button type="reset" class="btn btn-default">Vider les champs</button>

      <button class="btn btn-primary btn-raised">{{ button_label|default('Enregistrer') }}</button>
    </div>
  </div>

{{ form_end(form) }}
```



### Création de la recherche de film

Dans la vue de listing des films, nommée `film_index`, on rajoute un formulaire de recherche :

#### Création du formulaire dans le `FilmControlleur`

```php
private function getSearchForm(){
    $class = array("class" => 'form-control');
    $label_class = array("class"  => "bmd-label-floating full-width");
    
    return $this->createFormBuilder()
      ->setMethod('GET')
      ->add('search', TextType::class, array(
        'label' => 'Titre du film',
        'label_attr' => $label_class,
        'attr' => $class,
        'required'  => false
      ))
      ->add('filter_realisateur', EntityType::class, array(
        'label' => "Réalisateur",
        'label_attr' => $label_class,
        'class' => Realisateur::class,
        'choice_label' => 'prenomNom',
        'attr' => $class,
        'required'  => false
      ))
  
      ->add('filter_pays', EntityType::class, array(
        'class' => Pays::class,
        'choice_label' => 'nom',
        'label' => "Pays",
        'label_attr' => $label_class,
        'attr' => $class,
        'required'  => false
      ))
  
      ->add('filter_annee', TextType::class, array(
        'label' => 'Année de parution',
        'label_attr' => $label_class,
        'attr' => $class,
        'required'  => false
      ))
      
      ->getForm();
  }
```

#### Affichage du formulaire

```twig
<div class="card">
  {{ form_start(form) }}
  <div class="card-body">
    <h2 class="card-title">Rechercher un film</h2>

    <div class="form-group">
      {{ form_row(form.search) }}
    </div>
    <div class="form-group">
      {{ form_row(form.filter_realisateur) }}
    </div>
    <div class="form-group">
      {{ form_row(form.filter_pays) }}
    </div>
    <div class="form-group">
      {{ form_row(form.filter_annee) }}
    </div>
    <div class="form-group text-right">
      <a href="{{ path('film_index') }}" class="btn btn-default">Réinitialiser la recherche</a>
      <button type="submit" class="btn btn-primary">Chercher</button>
    </div>
  </div>
  {{ form_end(form) }}
</div>
```

#### Modification du controlleur lié à la vue de listing des films

On détecte si le formulaire de recherche a été soumis ou non, le cas échéant, on altère les résultats :

```php
public function index (FilmRepository $filmRepository, Request $request): Response
{
  $searchForm = $this->getSearchForm();
  $searchForm->handleRequest($request);

  if ($searchForm->isSubmitted() && $searchForm->isValid()){
    // Appel de la méthode personnalisée search qui récupère les valeurs des champs
    $renderContent = $filmRepository->search( $request->query->get('form') );
  }
  else {
    $renderContent = $filmRepository->findAll();
  }
  
  
  return $this->render('film/index.html.twig', array(
    'films' => $renderContent,
    'form'  => $searchForm->createView()
  ));
}
```

#### Construction de la requête de recherche 

Dans le repository des films, on va créer une nouvelle méthode `search` qui va effectuer une requête en base de données :

```php
public function search (Array $formData)
{
  $query = $this->createQueryBuilder('film');
  
  if (isset($formData['search']) && !empty($formData['search'])) {
    $query->andWhere('film.titre LIKE :titre');
    $query->setParameter('titre', '%' . $formData['search'] . '%');
  }
  
  if (isset($formData['filter_realisateur']) && !empty($formData['filter_realisateur'])) {
    $query->andWhere('film.realisateur = :realisateur');
    $query->setParameter('realisateur', $formData['filter_realisateur']);
  }
  
  if (isset($formData['filter_pays']) && !empty($formData['filter_pays'])) {
    $query->andWhere('film.pays = :pays');
    $query->setParameter('pays', $formData['filter_pays']);
  }
  
  if (isset($formData['filter_annee']) && !empty($formData['filter_annee'])) {
    $query->andWhere('film.annee = :annee');
    $query->setParameter('annee', $formData['filter_annee']);
  }
  
  return $query->getQuery()->getResult();
}
```

