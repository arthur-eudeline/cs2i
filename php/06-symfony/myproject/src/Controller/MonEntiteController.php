<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\MonEntite;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class MonEntiteController extends AbstractController
{
  /**
  * @Route("/mon-entite", name="mon_entite")
  */
  public function index()
  {
    // Affichage de toutes les entités
    $repository = $this->getDoctrine()->getRepository(MonEntite::class);
    $entities = $repository->findAll();

    return $this->render('mon_entite/index.html.twig', [
      'controller_name' => 'MonEntiteController',
      'entities'        => $entities
    ]);
  }

  /**
  * @Route("/mon-entite/new")
  */
  public function new(Request $request){
    $success = false;
    $entityManager = $this->getDoctrine()->getManager();
    $monEntite = new monEntite();

    $form = $this->createFormBuilder( $monEntite )
    ->add('nomEntite', TextType::class)
    ->add('descEntite', TextType::class)
    ->add('save', SubmitType::class, array('label' => 'Create Task'))
    ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {

      $monEntite = $form->getData();

      $entityManager->persist($monEntite);
      $entityManager->flush();

      $success = true;
    }

    return $this->render('mon_entite/new.html.twig', [
      "form"    => $form->createView(),
      "success"  => $success
    ]);
  }

}
