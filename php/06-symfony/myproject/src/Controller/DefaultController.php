<?php

namespace App\Controller;

use App\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;

class DefaultController extends AbstractController
{
  /**
  * [new description]
  *
  * @Route("/task/new")
  */
  public function new(Request $request)
  {
    $success = false;

    // creates a task and gives it some dummy data for this example
    $task = new Task();
    $task->setTask('Write a blog post');
    $task->setDueDate(new \DateTime('tomorrow'));

    $form = $this->createFormBuilder($task)
    ->add('task', TextType::class)
    ->add('dueDate', DateType::class)
    ->add('color', ColorType::class)
    ->add('priority', RangeType::class, array(
        'attr' => array(
            'min' => 1,
            'max' => 4
        )
    ))
    ->add('save', SubmitType::class, array('label' => 'Create Task'))
    ->getForm();

    // Modification :
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $task = $form->getData();

      // On attribue une variable succès pour afficher un message
      $success = true;
    }

    return $this->render('default/new.html.twig', array(
      'form' 		=> $form->createView(),
      'success'	=> $success
    ));

  }
}

?>
