<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class AboutUsController extends AbstractController
{
    /**
     * @Route({
     *     "fr": "/a-propos-de-nous",
     *     "en": "/about-us"
     * }, name="about_us")
     */
    public function about(Request $request)
    {
        return $this->render(
          'about-us.html.twig',
          array(
            "locale" => $request->getLocale()
          )
        );
    }
}
?>
