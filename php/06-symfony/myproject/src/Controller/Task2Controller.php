<?php

namespace App\Controller;

use App\Entity\Task2;
use App\Form\Task2Type;
use App\Repository\Task2Repository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/task2")
 */
class Task2Controller extends AbstractController
{
    /**
     * @Route("/", name="task2_index", methods="GET")
     */
    public function index(Task2Repository $task2Repository): Response
    {
        return $this->render('task2/index.html.twig', ['task2s' => $task2Repository->findAll()]);
    }

    /**
     * @Route("/new", name="task2_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $task2 = new Task2();
        $form = $this->createForm(Task2Type::class, $task2);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($task2);
            $em->flush();

            return $this->redirectToRoute('task2_index');
        }

        return $this->render('task2/new.html.twig', [
            'task2' => $task2,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="task2_show", methods="GET")
     */
    public function show(Task2 $task2): Response
    {
        return $this->render('task2/show.html.twig', ['task2' => $task2]);
    }

    /**
     * @Route("/{id}/edit", name="task2_edit", methods="GET|POST")
     */
    public function edit(Request $request, Task2 $task2): Response
    {
        $form = $this->createForm(Task2Type::class, $task2);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('task2_index', ['id' => $task2->getId()]);
        }

        return $this->render('task2/edit.html.twig', [
            'task2' => $task2,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="task2_delete", methods="DELETE")
     */
    public function delete(Request $request, Task2 $task2): Response
    {
        if ($this->isCsrfTokenValid('delete'.$task2->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($task2);
            $em->flush();
        }

        return $this->redirectToRoute('task2_index');
    }
}
