<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MonEntiteRepository")
 */
class MonEntite
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomEntite;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $descEntite;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomEntite(): ?string
    {
        return $this->nomEntite;
    }

    public function setNomEntite(string $nomEntite): self
    {
        $this->nomEntite = $nomEntite;

        return $this;
    }

    public function getDescEntite(): ?string
    {
        return $this->descEntite;
    }

    public function setDescEntite(?string $descEntite): self
    {
        $this->descEntite = $descEntite;

        return $this;
    }
}
