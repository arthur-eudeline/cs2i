<?php

namespace App\Entity;

// Importation de Symfony Validator
use Symfony\Component\Validator\Constraints as Assert;

class Task
{

  /**
  * @Assert\NotBlank
  */
  protected $task;

  /**
  * @Assert\NotBlank
  * @Assert\Type("\DateTime")
  */
  protected $dueDate;

  // Type public pour éviter d'avoir à faire les getter et les setter et aller plus vite
  public $color;
  public $priority;

  public function getTask()
  {
    return $this->task;
  }

  public function setTask($task)
  {
    $this->task = $task;
  }

  public function getDueDate()
  {
    return $this->dueDate;
  }

  public function setDueDate(\DateTime $dueDate = null)
  {
    $this->dueDate = $dueDate;
  }
}

?>
